-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 28, 2019 at 05:08 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `craftcms`
--

-- --------------------------------------------------------

--
-- Table structure for table `assetindexdata`
--

CREATE TABLE `assetindexdata` (
  `id` int(11) NOT NULL,
  `sessionId` varchar(36) NOT NULL DEFAULT '',
  `volumeId` int(11) NOT NULL,
  `uri` text,
  `size` bigint(20) UNSIGNED DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `recordId` int(11) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT '0',
  `completed` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE `assets` (
  `id` int(11) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `kind` varchar(50) NOT NULL DEFAULT 'unknown',
  `width` int(11) UNSIGNED DEFAULT NULL,
  `height` int(11) UNSIGNED DEFAULT NULL,
  `size` bigint(20) UNSIGNED DEFAULT NULL,
  `focalPoint` varchar(13) DEFAULT NULL,
  `deletedWithVolume` tinyint(1) DEFAULT NULL,
  `keptFile` tinyint(1) DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assets`
--

INSERT INTO `assets` (`id`, `volumeId`, `folderId`, `filename`, `kind`, `width`, `height`, `size`, `focalPoint`, `deletedWithVolume`, `keptFile`, `dateModified`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(3, 1, 1, 'hp_grafik.png', 'image', 2082, 1374, 950373, NULL, NULL, NULL, '2019-02-13 14:32:25', '2019-02-13 14:32:26', '2019-02-13 14:32:33', '7148ff46-d1e4-45b3-840d-0908a352f1b8'),
(4, 1, 1, 'HP_grafik-mobile.png', 'image', 510, 440, 27372, NULL, NULL, NULL, '2019-02-13 14:32:47', '2019-02-13 14:32:47', '2019-02-13 14:32:50', 'e5cfb6f6-df6e-4751-b412-203bbe23561a'),
(28, 2, 7, 'HP_grafik-mobile.png', 'image', 510, 440, 27393, NULL, NULL, NULL, '2019-02-27 11:30:08', '2019-02-27 11:30:08', '2019-02-27 11:30:08', 'e585aa7a-69e9-41fe-83f9-719b449aae04'),
(29, 2, 7, 'hp_grafik.png', 'image', 2082, 1374, 950394, NULL, NULL, NULL, '2019-02-27 11:31:08', '2019-02-27 11:31:11', '2019-02-27 11:31:11', 'd14a151a-d8a6-4145-a518-c0e99a0b9a7b');

-- --------------------------------------------------------

--
-- Table structure for table `assettransformindex`
--

CREATE TABLE `assettransformindex` (
  `id` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `location` varchar(255) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) NOT NULL DEFAULT '0',
  `inProgress` tinyint(1) NOT NULL DEFAULT '0',
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `assettransforms`
--

CREATE TABLE `assettransforms` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `mode` enum('stretch','fit','crop') NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') NOT NULL DEFAULT 'center-center',
  `width` int(11) UNSIGNED DEFAULT NULL,
  `height` int(11) UNSIGNED DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `quality` int(11) DEFAULT NULL,
  `interlace` enum('none','line','plane','partition') NOT NULL DEFAULT 'none',
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `deletedWithGroup` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categorygroups`
--

CREATE TABLE `categorygroups` (
  `id` int(11) NOT NULL,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categorygroups_sites`
--

CREATE TABLE `categorygroups_sites` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_description` text,
  `field_registrationSectionTitle` text,
  `field_registrationSectionDescription` text,
  `field_registrationLink` varchar(255) DEFAULT NULL,
  `field_registrationCodeDescription` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `elementId`, `siteId`, `title`, `dateCreated`, `dateUpdated`, `uid`, `field_description`, `field_registrationSectionTitle`, `field_registrationSectionDescription`, `field_registrationLink`, `field_registrationCodeDescription`) VALUES
(1, 1, 1, NULL, '2019-02-12 06:57:58', '2019-02-14 06:15:21', '28d8fab7-68e7-43ea-82d8-2b0ff126fced', NULL, NULL, NULL, NULL, NULL),
(2, 2, 1, 'Homepage', '2019-02-13 14:14:34', '2019-02-27 14:57:50', '59295f8e-a70f-496f-b6af-8249af6edfe8', '<div class="content-block d-none d-sm-block">\r\n <h2 class="content-heading">Vorstellungen</h2>\r\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\r\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\r\n <div class="schedule-block">\r\n <div class="schedule-row">\r\n <div class="schedule-col schedule-head">\r\n Mittwoch\r\n </div>\r\n <div class="schedule-col schedule-head">\r\n Donnerstag\r\n </div>\r\n <div class="schedule-col schedule-head">\r\n Freitag\r\n </div>\r\n <div class="schedule-col schedule-head">\r\n Samstag\r\n </div>\r\n <div class="schedule-col schedule-head">\r\n Sonntag\r\n </div>\r\n </div>\r\n <div class="schedule-row">\r\n <div class="schedule-col schedule-data">\r\n Teil Eins\r\n <label>14.30 Uhr</label>\r\n <label class="icon-amp iabc">&</label>\r\n </div>\r\n <div class="schedule-col schedule-data schedule-blank">\r\n \r\n <label> </label>\r\n </div>\r\n <div class="schedule-col schedule-data schedule-blank">\r\n \r\n <label> </label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Eins\r\n <label>14.30 Uhr</label>\r\n <label class="icon-amp iabc">&</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Eins\r\n <label>14.30 Uhr</label>\r\n <label class="icon-amp iabc">&</label>\r\n </div>\r\n </div>\r\n <div class="schedule-row">\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n <label class="icon-amp iarc">&</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n </div>\r\n </div>\r\n </div>\r\n </div>\r\n<div class="content-block nomr">\r\n            <h2 class="content-heading  d-none d-sm-block">Tickets Kaufen</h2>\r\n            <p class="d-none d-sm-block">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\r\n              direkt zur Ticketauswahl.\r\n            </p>\r\n            <div class="row">\r\n              <div class="col-xs-12 col-lg-12 col-md-12 no-mo-pd">\r\n                <div class="banner-block color1-bg modal-block">\r\n                  <div class="ribbon ribbon-top-left"><span>Empfohlen</span></div>\r\n                  <h4 class="h430"><figure><img src="public/app/images/icon-1.png" data-image="0yn5tbbn9n7l"></figure>Kombi</h4>\r\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\r\n                  <a href="javascript:void(0);" class="modify-btn ajax-data" data-toggle="modal" data-target="#myModal" data-id="HPKOMBI" data-local-json="public/sample_json/HPKOMBI.json">Kombi\r\n                    Tickets Kaufen</a>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>', NULL, NULL, NULL, NULL),
(3, 3, 1, 'Hp Grafik', '2019-02-13 14:32:17', '2019-02-13 14:32:33', '27f93690-7bfd-4d16-990b-24155df8ac93', NULL, NULL, NULL, NULL, NULL),
(4, 4, 1, 'Hp Grafik Mobile', '2019-02-13 14:32:46', '2019-02-13 14:32:50', '69315e7c-1de8-47ac-815f-375ab2ed5524', NULL, NULL, NULL, NULL, NULL),
(5, 5, 1, 'Impressum', '2019-02-13 14:37:11', '2019-02-18 07:06:59', '36515755-ea32-4346-865c-062a5e1dd592', '<p><strong>Eigentumer und Betreiber der Webseite ist: </strong></p><p>Aka Promotions Ltd </p><p>115 Shaftesbury Avenue,</p><p> Cambridge Circus, London, WC2H 8AF</p><p> </p><p>E-Mail: webteam@akauk.com </p><p>Telefon: +44(0)20 7836 4747 </p><p> </p><p><strong>Produzent fur Harry Potter und das verwunschene Kind in Deutschland ist: </strong></p><p>Mehr--BB Enterainment GmbH </p><p>Erkrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p><strong>Geschaftsfuhrer: </strong></p><p>Maik Klokow, Ralf Kokemuller, Gunter Irmler </p><p>Amtsgericht Dusseldorf - HRB 57980</p><p> </p><p>webmaster@mehr-entertainment.de </p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p><p><strong>Ticketing </strong></p><p>Deutsche Eintrittskarten TKS GmbH - www.eintrittskarten.de </p><p>Banksstr. 28 </p><p>D - 20097 Hamburg</p><p> </p><p>Buro Dusseldorf: </p><p>Erlrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p>Geschaftsfuhrer: Kai Ricke </p><p>Amtsgericht Hamburg - HRB 121731 </p><p>Ust-IdNr: DE 812 702 791</p><p> </p><p>webmaster@mehr-entertainment.de</p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p>', NULL, NULL, NULL, NULL),
(6, 6, 1, 'Datenschutzerklarung', '2019-02-13 14:37:35', '2019-02-18 07:06:20', 'ecdc0793-ba48-4b4e-a205-b573b9d7f025', '<h2> </h2><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><h2>1 AKA</h2><p>1.1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><h2>2 USE OF THE SITE</h2><p>2.1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.2 Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p><p>(a) Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>(b) Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>(c) Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>(d) Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.3 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.4 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.5 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.6 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, NULL, NULL, NULL),
(7, 7, 1, 'Agbs', '2019-02-13 14:37:55', '2019-02-18 07:06:20', '04c0326f-dadd-482c-a4bd-236f3a0b6159', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc accumsan ornare euismod. Vestibulum tincidunt massa at augue faucibus finibus. Duis iaculis sapien quis orci commodo, id commodo tortor volutpat. Etiam luctus est sapien, vel tempor diam cursus et. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque ornare a ipsum at posuere. Maecenas sollicitudin leo id ipsum pellentesque tincidunt. Sed velit quam, gravida at ornare sed, gravida a nisl. Curabitur gravida fringilla erat a venenatis. Nam posuere sit amet lorem ut blandit. Praesent ligula lectus, imperdiet eu leo id, aliquet commodo nisl. Ut venenatis sem commodo auctor gravida. Nulla facilisi. Integer congue at dui nec venenatis. Suspendisse facilisis enim nec porttitor euismod.</p><p>Aliquam ornare, ante pretium feugiat elementum, dui quam suscipit ipsum, a sagittis est libero id diam. Nunc viverra, ante vitae maximus placerat, risus dolor luctus elit, ac aliquet purus erat vel urna. Praesent varius urna purus, sit amet semper turpis scelerisque ac. Morbi aliquet nibh mauris, vel lobortis quam iaculis ut. Aliquam rutrum odio ut urna consectetur fringilla. Suspendisse sit amet urna vitae augue sodales pharetra. Etiam vel felis elit. Etiam at finibus velit, condimentum fringilla nibh. Morbi dignissim sollicitudin diam, ut elementum nisi auctor vitae.</p><p>Etiam egestas dignissim ultricies. Pellentesque vel arcu sed lectus feugiat iaculis et ac tortor. In sit amet maximus arcu, et vestibulum neque. Quisque sollicitudin elit neque, lacinia convallis leo lacinia quis. Sed semper elit orci. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur at sagittis nibh, a egestas ipsum. Ut tellus diam, dapibus nec purus a, porttitor iaculis diam. Sed sagittis mauris eu lacinia ultricies. Pellentesque volutpat in odio pellentesque facilisis.</p><p>Aenean pretium massa ac nisl blandit, aliquet imperdiet mi mollis. Integer erat lacus, finibus et diam sed, vestibulum lobortis quam. Sed porttitor, magna in malesuada accumsan, turpis ipsum placerat lectus, nec interdum tortor leo ut dui. Nunc commodo lorem ut fermentum scelerisque. Aenean id leo malesuada, dapibus diam id, convallis libero. Fusce auctor tortor id risus ultrices tempus. Integer pretium gravida leo, nec iaculis velit commodo ut. Suspendisse posuere ipsum non magna volutpat, sit amet luctus massa scelerisque. Donec dictum tincidunt massa ut gravida.</p><p>Integer fermentum felis sapien, nec rhoncus mi consequat in. Mauris eu dictum risus. Nulla tincidunt a felis nec rhoncus. Aliquam placerat nisi augue, sit amet tincidunt tortor dictum eu. Integer hendrerit rutrum dui, eget pellentesque risus. Duis et molestie risus. Cras lacinia vel dui quis imperdiet. Integer interdum blandit ligula ac congue. Donec semper euismod rhoncus.</p>', NULL, NULL, NULL, NULL),
(8, 8, 1, 'Cookies', '2019-02-13 14:38:09', '2019-02-18 07:06:20', 'cddeda29-29c7-4a26-87fd-e76f8aee36f4', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc accumsan ornare euismod. Vestibulum tincidunt massa at augue faucibus finibus. Duis iaculis sapien quis orci commodo, id commodo tortor volutpat. Etiam luctus est sapien, vel tempor diam cursus et. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque ornare a ipsum at posuere. Maecenas sollicitudin leo id ipsum pellentesque tincidunt. Sed velit quam, gravida at ornare sed, gravida a nisl. Curabitur gravida fringilla erat a venenatis. Nam posuere sit amet lorem ut blandit. Praesent ligula lectus, imperdiet eu leo id, aliquet commodo nisl. Ut venenatis sem commodo auctor gravida. Nulla facilisi. Integer congue at dui nec venenatis. Suspendisse facilisis enim nec porttitor euismod.</p><p>Aliquam ornare, ante pretium feugiat elementum, dui quam suscipit ipsum, a sagittis est libero id diam. Nunc viverra, ante vitae maximus placerat, risus dolor luctus elit, ac aliquet purus erat vel urna. Praesent varius urna purus, sit amet semper turpis scelerisque ac. Morbi aliquet nibh mauris, vel lobortis quam iaculis ut. Aliquam rutrum odio ut urna consectetur fringilla. Suspendisse sit amet urna vitae augue sodales pharetra. Etiam vel felis elit. Etiam at finibus velit, condimentum fringilla nibh. Morbi dignissim sollicitudin diam, ut elementum nisi auctor vitae.</p><p>Etiam egestas dignissim ultricies. Pellentesque vel arcu sed lectus feugiat iaculis et ac tortor. In sit amet maximus arcu, et vestibulum neque. Quisque sollicitudin elit neque, lacinia convallis leo lacinia quis. Sed semper elit orci. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur at sagittis nibh, a egestas ipsum. Ut tellus diam, dapibus nec purus a, porttitor iaculis diam. Sed sagittis mauris eu lacinia ultricies. Pellentesque volutpat in odio pellentesque facilisis.</p><p>Aenean pretium massa ac nisl blandit, aliquet imperdiet mi mollis. Integer erat lacus, finibus et diam sed, vestibulum lobortis quam. Sed porttitor, magna in malesuada accumsan, turpis ipsum placerat lectus, nec interdum tortor leo ut dui. Nunc commodo lorem ut fermentum scelerisque. Aenean id leo malesuada, dapibus diam id, convallis libero. Fusce auctor tortor id risus ultrices tempus. Integer pretium gravida leo, nec iaculis velit commodo ut. Suspendisse posuere ipsum non magna volutpat, sit amet luctus massa scelerisque. Donec dictum tincidunt massa ut gravida.</p><p>Integer fermentum felis sapien, nec rhoncus mi consequat in. Mauris eu dictum risus. Nulla tincidunt a felis nec rhoncus. Aliquam placerat nisi augue, sit amet tincidunt tortor dictum eu. Integer hendrerit rutrum dui, eget pellentesque risus. Duis et molestie risus. Cras lacinia vel dui quis imperdiet. Integer interdum blandit ligula ac congue. Donec semper euismod rhoncus.</p>', NULL, NULL, NULL, NULL),
(9, 9, 1, 'Footer Copyright Text', '2019-02-14 10:18:18', '2019-02-18 08:17:48', 'b1c4c77d-57a2-4b0a-804f-71fd10dece35', '<p>Harry Potter Publishing and Theatrical Rights © J.k. Rowling</p>', NULL, NULL, NULL, NULL),
(10, 10, 1, 'Website Managed by text', '2019-02-14 10:29:20', '2019-02-18 08:17:48', 'b4dedece-429f-4d6a-a751-9a66243c8913', '<p>Webseite wird gemanaged von <a href="http://www.google.com">AKA</a></p>', NULL, NULL, NULL, NULL),
(11, 11, 1, 'Trademarks text', '2019-02-14 10:35:54', '2019-02-18 08:17:48', 'c590724a-ceb4-4f81-92b9-f2fa81d5c182', '<p>Harry Portter Characters, Name and related indicia are Trademarks of and © Warner Bros. Ent. Alle Rechte vorberhalten.</p>', NULL, NULL, NULL, NULL),
(12, 12, 1, 'Verfügbare Tickets', '2019-02-14 11:18:50', '2019-02-18 08:17:48', '18e5d267-30e5-4b7c-aad2-ab3af9b51f95', '<p>Es glbt noch letzte Elnzelplatze fur Vorstellungen Irn Januar</p>', NULL, NULL, NULL, NULL),
(13, 13, 1, 'Saalplan', '2019-02-14 11:51:41', '2019-02-20 11:30:42', 'd922edac-1010-4137-bf0c-9e2ad15b5c3d', '<p>Mehr!Theater am Grobmarkt, Hamburg</p>\r\n<img src="public/app/images/img1.png" class="iframe-img">', NULL, NULL, NULL, NULL),
(14, 3, 2, 'Hp Grafik', '2019-02-15 06:56:51', '2019-02-15 06:56:51', '57a69da7-f029-47bb-8573-2311c67edd2c', NULL, NULL, NULL, NULL, NULL),
(15, 4, 2, 'Hp Grafik Mobile', '2019-02-15 06:56:51', '2019-02-15 06:56:51', '89a48863-e915-48c3-9c9e-966762034566', NULL, NULL, NULL, NULL, NULL),
(16, 14, 2, 'HomePage English', '2019-02-18 06:37:19', '2019-02-20 12:16:29', '065f8096-e3dc-4d09-abfc-61b8c34c633f', '<div class="content-block d-none d-sm-block">\r\n <h2 class="content-heading">Vorstellungen</h2>\r\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\r\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\r\n <div class="schedule-block">\r\n <div class="schedule-row">\r\n <div class="schedule-col schedule-head">\r\n Mittwoch\r\n </div>\r\n <div class="schedule-col schedule-head">\r\n Donnerstag\r\n </div>\r\n <div class="schedule-col schedule-head">\r\n Freitag\r\n </div>\r\n <div class="schedule-col schedule-head">\r\n Samstag\r\n </div>\r\n <div class="schedule-col schedule-head">\r\n Sonntag\r\n </div>\r\n </div>\r\n <div class="schedule-row">\r\n <div class="schedule-col schedule-data">\r\n Teil Eins\r\n <label>14.30 Uhr</label>\r\n <label class="icon-amp iabc">&</label>\r\n </div>\r\n <div class="schedule-col schedule-data schedule-blank">\r\n \r\n <label> </label>\r\n </div>\r\n <div class="schedule-col schedule-data schedule-blank">\r\n \r\n <label> </label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Eins\r\n <label>14.30 Uhr</label>\r\n <label class="icon-amp iabc">&</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Eins\r\n <label>14.30 Uhr</label>\r\n <label class="icon-amp iabc">&</label>\r\n </div>\r\n </div>\r\n <div class="schedule-row">\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n <label class="icon-amp iarc">&</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n </div>\r\n </div>\r\n </div>\r\n </div>\r\n<div class="content-block nomr">\r\n            <h2 class="content-heading  d-none d-sm-block">Tickets Kaufen</h2>\r\n            <p class="d-none d-sm-block">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\r\n              direkt zur Ticketauswahl.\r\n            </p>\r\n            <div class="row">\r\n              <div class="col-xs-12 col-lg-12 col-md-12 no-mo-pd">\r\n                <div class="banner-block color1-bg modal-block" data-toggle="modal" data-target="#myModal">\r\n                  <div class="ribbon ribbon-top-left"><span>Empfohlen</span></div>\r\n                  <h4 class="h430"><figure><img src="/public/app/images/icon-1.png" data-image="iqd7x1sxiz3z"></figure>Kombi</h4>\r\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\r\n                  <a href="javascript:void()" class="modify-btn">Kombi Tickets Kaufen</a>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class="row">\r\n              <div class="col-xs-12 col-lg-6 col-md-6 col-6 no-mo-pd">\r\n                <div class="banner-block color2-bg">\r\n                  <h4><figure><img src="/public/app/images/icon-2.png" data-image="jg0fwanmf9r4"><span id="selection-marker-start" class="redactor-selection-marker">﻿</span><span id="selection-marker-end" class="redactor-selection-marker">﻿</span></figure>Individual</h4>\r\n                  <p>Teil Eins und Teil Zwei unabhängig voneinander schauen </p>\r\n                  <a href="javascript:void()" class="modify-btn">Individual Tickets Kaufen</a>\r\n                </div>\r\n              </div>\r\n              <div class="col-xs-12 col-lg-6 col-md-6 col-6 no-mo-pd">\r\n                <div class="banner-block color3-bg">\r\n                  <h4 class="h458"><figure><img src="/public/app/images/icon-3.png" data-image="fxa0pz83a16q"></figure>Single</h4>\r\n                  <p class="mo-mb-39 tab-mb-60">Nur Teil Eins oder<br>nur Teil Zwei schauen</p>\r\n                  <a href="javascript:void()" class="modify-btn">Single Tickets Kaufen</a>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>', NULL, NULL, NULL, NULL),
(26, 15, 2, 'Impressum English', '2019-02-18 07:07:45', '2019-02-18 07:07:45', '2dc7da09-2c96-479d-8ff3-e8d0b527742b', '<p><strong>Eigentumer und Betreiber der Webseite ist: </strong></p><p>Aka Promotions Ltd </p><p>115 Shaftesbury Avenue,</p><p> Cambridge Circus, London, WC2H 8AF</p><p> </p><p>E-Mail: webteam@akauk.com </p><p>Telefon: +44(0)20 7836 4747 </p><p> </p><p><strong>Produzent fur Harry Potter und das verwunschene Kind in Deutschland ist: </strong></p><p>Mehr--BB Enterainment GmbH </p><p>Erkrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p><strong>Geschaftsfuhrer: </strong></p><p>Maik Klokow, Ralf Kokemuller, Gunter Irmler </p><p>Amtsgericht Dusseldorf - HRB 57980</p><p> </p><p>webmaster@mehr-entertainment.de </p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p><p><strong>Ticketing </strong></p><p>Deutsche Eintrittskarten TKS GmbH - www.eintrittskarten.de </p><p>Banksstr. 28 </p><p>D - 20097 Hamburg</p><p> </p><p>Buro Dusseldorf: </p><p>Erlrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p>Geschaftsfuhrer: Kai Ricke </p><p>Amtsgericht Hamburg - HRB 121731 </p><p>Ust-IdNr: DE 812 702 791</p><p> </p><p>webmaster@mehr-entertainment.de</p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p>', NULL, NULL, NULL, NULL),
(27, 16, 2, 'Seating plan', '2019-02-18 08:18:44', '2019-02-20 12:16:58', 'd11a2a79-29dc-4afb-b67a-53aefa6c3e9b', '<p>More! Theater am Grobmarkt, Hambur</p>\r\n<figure><img src="/public/app/images/img1.png" class="iframe-img" data-image="l6webfxrwj2l"></figure>', NULL, NULL, NULL, NULL),
(28, 17, 2, 'Available tickets', '2019-02-18 08:19:31', '2019-02-18 08:19:31', '2c894e1a-d97b-41eb-8ec9-76873becdace', '<p>It still gleams last Elnzelplatze for performances in January</p>', NULL, NULL, NULL, NULL),
(29, 18, 2, 'Trademarks text English', '2019-02-18 08:20:11', '2019-02-18 08:20:11', '598dad55-8317-447e-a719-1f5eacc0f0ae', '<p>Harry Portter Characters, Name and related indicia are Trademarks of and © Warner Bros. Ent. Alle Rechte vorberhalten.</p>', NULL, NULL, NULL, NULL),
(30, 19, 2, 'Website Managed by text English', '2019-02-18 08:21:26', '2019-02-18 08:21:26', 'c76b8249-cd65-4c26-a1e4-0769248c70e2', '<p>Website is managed by <a href="http://www/google.com">AKA</a></p>', NULL, NULL, NULL, NULL),
(31, 20, 2, 'Footer Copyright Text English', '2019-02-18 08:22:12', '2019-02-18 08:22:12', 'a67d8d29-0b6e-4bb9-83d0-4fac8a9cff76', '<p>Harry Potter Publishing and Theatrical Rights © J.k. Rowling</p>', NULL, NULL, NULL, NULL),
(32, 21, 2, 'Data protection', '2019-02-18 08:23:03', '2019-02-18 08:23:03', 'f4eab899-5107-40e7-8d40-671cbde384a0', '<h2> </h2><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><h2>1 AKA</h2><p>1.1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><h2>2 USE OF THE SITE</h2><p>2.1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.2 Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p><p>(a) Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>(b) Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>(c) Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>(d) Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.3 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.4 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.5 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.6 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, NULL, NULL, NULL),
(33, 22, 2, 'Agbs English', '2019-02-18 08:23:58', '2019-02-18 08:23:58', 'f5b9661a-8d02-4f4d-b062-87c7337a7795', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc accumsan ornare euismod. Vestibulum tincidunt massa at augue faucibus finibus. Duis iaculis sapien quis orci commodo, id commodo tortor volutpat. Etiam luctus est sapien, vel tempor diam cursus et. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque ornare a ipsum at posuere. Maecenas sollicitudin leo id ipsum pellentesque tincidunt. Sed velit quam, gravida at ornare sed, gravida a nisl. Curabitur gravida fringilla erat a venenatis. Nam posuere sit amet lorem ut blandit. Praesent ligula lectus, imperdiet eu leo id, aliquet commodo nisl. Ut venenatis sem commodo auctor gravida. Nulla facilisi. Integer congue at dui nec venenatis. Suspendisse facilisis enim nec porttitor euismod.</p><p>Aliquam ornare, ante pretium feugiat elementum, dui quam suscipit ipsum, a sagittis est libero id diam. Nunc viverra, ante vitae maximus placerat, risus dolor luctus elit, ac aliquet purus erat vel urna. Praesent varius urna purus, sit amet semper turpis scelerisque ac. Morbi aliquet nibh mauris, vel lobortis quam iaculis ut. Aliquam rutrum odio ut urna consectetur fringilla. Suspendisse sit amet urna vitae augue sodales pharetra. Etiam vel felis elit. Etiam at finibus velit, condimentum fringilla nibh. Morbi dignissim sollicitudin diam, ut elementum nisi auctor vitae.</p><p>Etiam egestas dignissim ultricies. Pellentesque vel arcu sed lectus feugiat iaculis et ac tortor. In sit amet maximus arcu, et vestibulum neque. Quisque sollicitudin elit neque, lacinia convallis leo lacinia quis. Sed semper elit orci. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur at sagittis nibh, a egestas ipsum. Ut tellus diam, dapibus nec purus a, porttitor iaculis diam. Sed sagittis mauris eu lacinia ultricies. Pellentesque volutpat in odio pellentesque facilisis.</p><p>Aenean pretium massa ac nisl blandit, aliquet imperdiet mi mollis. Integer erat lacus, finibus et diam sed, vestibulum lobortis quam. Sed porttitor, magna in malesuada accumsan, turpis ipsum placerat lectus, nec interdum tortor leo ut dui. Nunc commodo lorem ut fermentum scelerisque. Aenean id leo malesuada, dapibus diam id, convallis libero. Fusce auctor tortor id risus ultrices tempus. Integer pretium gravida leo, nec iaculis velit commodo ut. Suspendisse posuere ipsum non magna volutpat, sit amet luctus massa scelerisque. Donec dictum tincidunt massa ut gravida.</p><p>Integer fermentum felis sapien, nec rhoncus mi consequat in. Mauris eu dictum risus. Nulla tincidunt a felis nec rhoncus. Aliquam placerat nisi augue, sit amet tincidunt tortor dictum eu. Integer hendrerit rutrum dui, eget pellentesque risus. Duis et molestie risus. Cras lacinia vel dui quis imperdiet. Integer interdum blandit ligula ac congue. Donec semper euismod rhoncus.</p>', NULL, NULL, NULL, NULL),
(34, 23, 2, 'Cookies English', '2019-02-18 08:24:19', '2019-02-18 08:24:19', '0e5941e9-a4d0-4457-901d-e63ac34cd486', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc accumsan ornare euismod. Vestibulum tincidunt massa at augue faucibus finibus. Duis iaculis sapien quis orci commodo, id commodo tortor volutpat. Etiam luctus est sapien, vel tempor diam cursus et. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque ornare a ipsum at posuere. Maecenas sollicitudin leo id ipsum pellentesque tincidunt. Sed velit quam, gravida at ornare sed, gravida a nisl. Curabitur gravida fringilla erat a venenatis. Nam posuere sit amet lorem ut blandit. Praesent ligula lectus, imperdiet eu leo id, aliquet commodo nisl. Ut venenatis sem commodo auctor gravida. Nulla facilisi. Integer congue at dui nec venenatis. Suspendisse facilisis enim nec porttitor euismod.</p><p>Aliquam ornare, ante pretium feugiat elementum, dui quam suscipit ipsum, a sagittis est libero id diam. Nunc viverra, ante vitae maximus placerat, risus dolor luctus elit, ac aliquet purus erat vel urna. Praesent varius urna purus, sit amet semper turpis scelerisque ac. Morbi aliquet nibh mauris, vel lobortis quam iaculis ut. Aliquam rutrum odio ut urna consectetur fringilla. Suspendisse sit amet urna vitae augue sodales pharetra. Etiam vel felis elit. Etiam at finibus velit, condimentum fringilla nibh. Morbi dignissim sollicitudin diam, ut elementum nisi auctor vitae.</p><p>Etiam egestas dignissim ultricies. Pellentesque vel arcu sed lectus feugiat iaculis et ac tortor. In sit amet maximus arcu, et vestibulum neque. Quisque sollicitudin elit neque, lacinia convallis leo lacinia quis. Sed semper elit orci. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur at sagittis nibh, a egestas ipsum. Ut tellus diam, dapibus nec purus a, porttitor iaculis diam. Sed sagittis mauris eu lacinia ultricies. Pellentesque volutpat in odio pellentesque facilisis.</p><p>Aenean pretium massa ac nisl blandit, aliquet imperdiet mi mollis. Integer erat lacus, finibus et diam sed, vestibulum lobortis quam. Sed porttitor, magna in malesuada accumsan, turpis ipsum placerat lectus, nec interdum tortor leo ut dui. Nunc commodo lorem ut fermentum scelerisque. Aenean id leo malesuada, dapibus diam id, convallis libero. Fusce auctor tortor id risus ultrices tempus. Integer pretium gravida leo, nec iaculis velit commodo ut. Suspendisse posuere ipsum non magna volutpat, sit amet luctus massa scelerisque. Donec dictum tincidunt massa ut gravida.</p><p>Integer fermentum felis sapien, nec rhoncus mi consequat in. Mauris eu dictum risus. Nulla tincidunt a felis nec rhoncus. Aliquam placerat nisi augue, sit amet tincidunt tortor dictum eu. Integer hendrerit rutrum dui, eget pellentesque risus. Duis et molestie risus. Cras lacinia vel dui quis imperdiet. Integer interdum blandit ligula ac congue. Donec semper euismod rhoncus.</p>', NULL, NULL, NULL, NULL),
(35, 24, 2, 'Footer Logo Block English', '2019-02-20 12:22:05', '2019-02-20 12:22:28', '6931aa9f-2303-497f-add8-157702d0b648', '<figure><img src="/public/app/images/footer-logo-hp.png" class="logo-hp" data-image="76st2dbitket"></figure>\r\n<figure><img src="/public/app/images/footer-logo-wizard-world.png" class="logo-wizard" data-image="jsx9zbr1jx3k"></figure>\r\n<figure><img src="/public/app/images/footer-logo-Mehr.png" class="logo-mehr" data-image="33ome26msee7"></figure>', NULL, NULL, NULL, NULL),
(36, 25, 1, 'Footer Logo Block', '2019-02-20 12:26:23', '2019-02-20 12:26:23', '82cbfde4-3f89-4896-a5a6-0323b0b53676', '<figure><img src="/public/app/images/footer-logo-hp.png" class="logo-hp" data-image="76st2dbitket"></figure>\r\n<figure><img src="/public/app/images/footer-logo-wizard-world.png" class="logo-wizard" data-image="jsx9zbr1jx3k"></figure>\r\n<figure><img src="/public/app/images/footer-logo-Mehr.png" class="logo-mehr" data-image="33ome26msee7"></figure>', NULL, NULL, NULL, NULL),
(37, 26, 1, 'process', '2019-02-25 13:41:04', '2019-02-25 13:49:53', 'dbcea56b-f477-4182-be2e-a7d1b17cd6cb', NULL, NULL, NULL, NULL, NULL),
(38, 26, 2, 'process', '2019-02-25 13:41:04', '2019-02-25 13:49:53', 'b1e50da2-3562-4e1a-bc6f-e2b94d7da330', NULL, NULL, NULL, NULL, NULL),
(39, 27, 1, 'Presale', '2019-02-26 06:41:43', '2019-02-27 15:00:27', 'bab7fca3-9216-499c-8a0a-774aefe647d6', '<div class="content-block d-none d-sm-block">\r\n <h2 class="content-heading">Vorstellungen</h2>\r\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\r\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\r\n <div class="schedule-block">\r\n <div class="schedule-row">\r\n <div class="schedule-col schedule-head">\r\n Mittwoch\r\n </div>\r\n <div class="schedule-col schedule-head">\r\n Donnerstag\r\n </div>\r\n <div class="schedule-col schedule-head">\r\n Freitag\r\n </div>\r\n <div class="schedule-col schedule-head">\r\n Samstag\r\n </div>\r\n <div class="schedule-col schedule-head">\r\n Sonntag\r\n </div>\r\n </div>\r\n <div class="schedule-row">\r\n <div class="schedule-col schedule-data">\r\n Teil Eins\r\n <label>14.30 Uhr</label>\r\n <label class="icon-amp iabc">&</label>\r\n </div>\r\n <div class="schedule-col schedule-data schedule-blank">\r\n \r\n <label> </label>\r\n </div>\r\n <div class="schedule-col schedule-data schedule-blank">\r\n \r\n <label> </label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Eins\r\n <label>14.30 Uhr</label>\r\n <label class="icon-amp iabc">&</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Eins\r\n <label>14.30 Uhr</label>\r\n <label class="icon-amp iabc">&</label>\r\n </div>\r\n </div>\r\n <div class="schedule-row">\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n <label class="icon-amp iarc">&</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n </div>\r\n </div>\r\n </div>\r\n </div>\r\n<div class="content-block nomr close" style="display:none;">\r\n            <h2 class="content-heading  d-none d-sm-block">Tickets Kaufen</h2>\r\n            <p class="d-none d-sm-block">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\r\n              direkt zur Ticketauswahl.\r\n            </p>\r\n            <div class="row">\r\n              <div class="col-xs-12 col-lg-12 col-md-12 no-mo-pd">\r\n                <div class="banner-block color1-bg modal-block">\r\n                  <div class="ribbon ribbon-top-left">Empfohlen</div>\r\n                  <h4 class="h430"><figure><img src="/public/app/images/icon-1.png" data-image="0yn5tbbn9n7l"></figure>Kombi</h4>\r\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\r\n                  <a href="javascript:void(0);" class="modify-btn ajax-data" data-toggle="modal" data-target="#myModal" data-id="HPKOMBI" data-local-json="/public/sample_json/HPKOMBI.json">Kombi\r\n                    Tickets Kaufen</a>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>', 'Noch 10 Tage bis zum Tickets Presale', '<p> Atte Fans, die sich bis zum 15. Marz unter <a href="http://www.XYZ.de">www.XYZ.de</a> fur den Newsletter registrieren, haben vier Tage vor dem offiziellen Vorverkaufsstart am 25. Marz die Meglichkeit, hier Tickets zu bestehen. </p>', 'https://www.google.com', '<p>Wenn Du Dich bei <a href="http://www.XYZ.de">www.XYZ.de</a> registriert hast, hast Du einen Code per Email bekommen. Den kannst Du hier eintragen und exklusiv vor offiziellem Verkaufsstart Tickets Kir Harry Potter und das verwunschene Kind kaufen.</p>');
INSERT INTO `content` (`id`, `elementId`, `siteId`, `title`, `dateCreated`, `dateUpdated`, `uid`, `field_description`, `field_registrationSectionTitle`, `field_registrationSectionDescription`, `field_registrationLink`, `field_registrationCodeDescription`) VALUES
(40, 27, 2, 'Presale', '2019-02-26 06:41:44', '2019-02-27 15:00:27', 'e232be7c-8c78-4d18-82e9-a294f0cf5074', '<div class="content-block d-none d-sm-block">\r\n <h2 class="content-heading">Vorstellungen</h2>\r\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\r\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\r\n <div class="schedule-block">\r\n <div class="schedule-row">\r\n <div class="schedule-col schedule-head">\r\n Mittwoch\r\n </div>\r\n <div class="schedule-col schedule-head">\r\n Donnerstag\r\n </div>\r\n <div class="schedule-col schedule-head">\r\n Freitag\r\n </div>\r\n <div class="schedule-col schedule-head">\r\n Samstag\r\n </div>\r\n <div class="schedule-col schedule-head">\r\n Sonntag\r\n </div>\r\n </div>\r\n <div class="schedule-row">\r\n <div class="schedule-col schedule-data">\r\n Teil Eins\r\n <label>14.30 Uhr</label>\r\n <label class="icon-amp iabc">&</label>\r\n </div>\r\n <div class="schedule-col schedule-data schedule-blank">\r\n \r\n <label> </label>\r\n </div>\r\n <div class="schedule-col schedule-data schedule-blank">\r\n \r\n <label> </label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Eins\r\n <label>14.30 Uhr</label>\r\n <label class="icon-amp iabc">&</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Eins\r\n <label>14.30 Uhr</label>\r\n <label class="icon-amp iabc">&</label>\r\n </div>\r\n </div>\r\n <div class="schedule-row">\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n <label class="icon-amp iarc">&</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n </div>\r\n <div class="schedule-col schedule-data">\r\n Teil Zwei\r\n <label>19.30 Uhr</label>\r\n </div>\r\n </div>\r\n </div>\r\n </div>\r\n<div class="content-block nomr close" style="display:none;">\r\n            <h2 class="content-heading  d-none d-sm-block">Tickets Kaufen</h2>\r\n            <p class="d-none d-sm-block">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\r\n              direkt zur Ticketauswahl.\r\n            </p>\r\n            <div class="row">\r\n              <div class="col-xs-12 col-lg-12 col-md-12 no-mo-pd">\r\n                <div class="banner-block color1-bg modal-block">\r\n                  <div class="ribbon ribbon-top-left">Empfohlen</div>\r\n                  <h4 class="h430"><figure><img src="/public/app/images/icon-1.png" data-image="0yn5tbbn9n7l"></figure>Kombi</h4>\r\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\r\n                  <a href="javascript:void(0);" class="modify-btn ajax-data" data-toggle="modal" data-target="#myModal" data-id="HPKOMBI" data-local-json="/public/sample_json/HPKOMBI.json">Kombi\r\n                    Tickets Kaufen</a>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>', 'Noch 10 Tage bis zum Tickets Presale', '<p> Atte Fans, die sich bis zum 15. Marz unter <a href="http://www.XYZ.de">www.XYZ.de</a> fur den Newsletter registrieren, haben vier Tage vor dem offiziellen Vorverkaufsstart am 25. Marz die Meglichkeit, hier Tickets zu bestehen. </p>', 'https://www.google.com', '<p>Wenn Du Dich bei <a href="http://www.XYZ.de">www.XYZ.de</a> registriert hast, hast Du einen Code per Email bekommen. Den kannst Du hier eintragen und exklusiv vor offiziellem Verkaufsstart Tickets Kir Harry Potter und das verwunschene Kind kaufen.</p>'),
(41, 28, 1, 'HP_grafik-mobile', '2019-02-27 11:30:07', '2019-02-27 11:30:07', '7562d962-d423-4543-b055-2caf7115d945', NULL, NULL, NULL, NULL, NULL),
(42, 28, 2, 'HP_grafik-mobile', '2019-02-27 11:30:08', '2019-02-27 11:30:08', '8bf6123a-b60f-44c1-afa7-1fdba9212fb0', NULL, NULL, NULL, NULL, NULL),
(43, 29, 1, 'Hp_grafik', '2019-02-27 11:31:00', '2019-02-27 11:31:00', 'e22feb21-f9fa-4485-b8fe-b600b1b9ffe6', NULL, NULL, NULL, NULL, NULL),
(44, 29, 2, 'Hp_grafik', '2019-02-27 11:31:11', '2019-02-27 11:31:11', 'b0edf38d-d9d4-425c-8b76-e728ebf46cb4', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `craftidtokens`
--

CREATE TABLE `craftidtokens` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `accessToken` text NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `deprecationerrors`
--

CREATE TABLE `deprecationerrors` (
  `id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `fingerprint` varchar(255) NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) NOT NULL,
  `line` smallint(6) UNSIGNED DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `traces` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elementindexsettings`
--

CREATE TABLE `elementindexsettings` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elements`
--

CREATE TABLE `elements` (
  `id` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `elements`
--

INSERT INTO `elements` (`id`, `fieldLayoutId`, `type`, `enabled`, `archived`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, NULL, 'craft\\elements\\User', 1, 0, '2019-02-12 06:57:58', '2019-02-14 06:15:21', NULL, '42e904ae-6225-47f4-94a3-c62ae6a17fea'),
(2, 2, 'craft\\elements\\Entry', 1, 0, '2019-02-13 14:14:34', '2019-02-27 14:57:50', NULL, '48e7db32-06f9-4176-8268-fa82190db79e'),
(3, 3, 'craft\\elements\\Asset', 1, 0, '2019-02-13 14:32:17', '2019-02-15 06:56:51', NULL, 'aebb55e4-a829-4e48-8a28-c553a4151dbe'),
(4, 3, 'craft\\elements\\Asset', 1, 0, '2019-02-13 14:32:46', '2019-02-15 06:56:51', NULL, '24dea58b-d874-4529-ae03-06510b52e1ef'),
(5, 1, 'craft\\elements\\Entry', 1, 0, '2019-02-13 14:37:11', '2019-02-18 07:06:59', NULL, '9d63e716-53e5-4d0e-9dc2-5d3a38296a89'),
(6, 1, 'craft\\elements\\Entry', 1, 0, '2019-02-13 14:37:35', '2019-02-18 07:06:20', NULL, '8bc175bd-869d-4ed1-9197-9c319c1ba5e4'),
(7, 1, 'craft\\elements\\Entry', 1, 0, '2019-02-13 14:37:55', '2019-02-18 07:06:20', NULL, '0fd6c803-9aea-451a-ac1a-ef73d4e0d835'),
(8, 1, 'craft\\elements\\Entry', 1, 0, '2019-02-13 14:38:09', '2019-02-18 07:06:20', NULL, '26ee9c4c-c90d-4622-bb4e-d1e720fbfac3'),
(9, 4, 'craft\\elements\\Entry', 1, 0, '2019-02-14 10:18:18', '2019-02-18 08:17:48', NULL, '2d1dfed0-eec0-43c9-8da2-e4cd3fdb8aad'),
(10, 4, 'craft\\elements\\Entry', 1, 0, '2019-02-14 10:29:20', '2019-02-18 08:17:48', NULL, '279bb56a-ff21-4d78-a57c-df1ae18a7347'),
(11, 4, 'craft\\elements\\Entry', 1, 0, '2019-02-14 10:35:54', '2019-02-18 08:17:48', NULL, 'be87a2d0-e6ff-457e-8766-30c209cca893'),
(12, 4, 'craft\\elements\\Entry', 1, 0, '2019-02-14 11:18:50', '2019-02-18 08:17:48', NULL, 'ca787a00-ca6c-4be2-b744-d4561ebc5e22'),
(13, 4, 'craft\\elements\\Entry', 1, 0, '2019-02-14 11:51:41', '2019-02-20 11:30:42', NULL, '22f66686-12c6-40a0-8067-83f0f0896324'),
(14, 5, 'craft\\elements\\Entry', 1, 0, '2019-02-18 06:37:19', '2019-02-20 12:16:29', NULL, 'e329da0e-60e9-4916-b433-344442c1c9ef'),
(15, 1, 'craft\\elements\\Entry', 1, 0, '2019-02-18 07:07:45', '2019-02-18 07:07:45', NULL, 'd5042af8-b299-48d7-827c-4f0dd6008baa'),
(16, 4, 'craft\\elements\\Entry', 1, 0, '2019-02-18 08:18:44', '2019-02-20 12:16:58', NULL, 'dd3940ea-0c03-481d-838f-7a692f981f23'),
(17, 4, 'craft\\elements\\Entry', 1, 0, '2019-02-18 08:19:31', '2019-02-18 08:19:31', NULL, '481a9050-afab-4cf0-9e7e-5a47a6fcacb9'),
(18, 4, 'craft\\elements\\Entry', 1, 0, '2019-02-18 08:20:11', '2019-02-18 08:20:11', NULL, 'f680c862-e5b0-4ef7-b7f4-ffa1c3047b62'),
(19, 4, 'craft\\elements\\Entry', 1, 0, '2019-02-18 08:21:26', '2019-02-18 08:21:26', NULL, 'e82ec59f-83fc-4604-8bdd-b62c5c08d191'),
(20, 4, 'craft\\elements\\Entry', 1, 0, '2019-02-18 08:22:12', '2019-02-18 08:22:12', NULL, '180bf60c-7755-4dca-a480-21cb09c2a1c2'),
(21, 1, 'craft\\elements\\Entry', 1, 0, '2019-02-18 08:23:03', '2019-02-18 08:23:03', NULL, '34c3e77f-5903-4cf3-b762-b5149e0ab2f4'),
(22, 1, 'craft\\elements\\Entry', 1, 0, '2019-02-18 08:23:58', '2019-02-18 08:23:58', NULL, '4d184b21-cbbe-4471-880e-dd9da1e477f7'),
(23, 1, 'craft\\elements\\Entry', 1, 0, '2019-02-18 08:24:19', '2019-02-18 08:24:19', NULL, '1d043dda-4ab1-4950-80ee-5c0425f2df87'),
(24, 4, 'craft\\elements\\Entry', 1, 0, '2019-02-20 12:22:05', '2019-02-20 12:22:28', NULL, 'cdcdc132-d03a-48ee-a021-3d571131b076'),
(25, 4, 'craft\\elements\\Entry', 1, 0, '2019-02-20 12:26:23', '2019-02-20 12:26:23', NULL, '7a2a6d4d-cce5-4742-a7e4-707b49321855'),
(26, NULL, 'craft\\elements\\Entry', 1, 0, '2019-02-25 13:41:04', '2019-02-25 13:49:53', '2019-02-25 13:49:53', 'e45749f3-36df-432d-97d0-8f2a9374111f'),
(27, 6, 'craft\\elements\\Entry', 1, 0, '2019-02-26 06:41:43', '2019-02-27 15:00:27', NULL, '181e6b7e-03a0-4944-94ae-2d574ec5166f'),
(28, NULL, 'craft\\elements\\Asset', 1, 0, '2019-02-27 11:30:07', '2019-02-27 11:30:08', NULL, '67a7a11c-2fd5-4f39-8cae-be01193a7d49'),
(29, NULL, 'craft\\elements\\Asset', 1, 0, '2019-02-27 11:31:00', '2019-02-27 11:31:11', NULL, 'a74fdf14-543b-4d55-880a-d830a7bf4190');

-- --------------------------------------------------------

--
-- Table structure for table `elements_sites`
--

CREATE TABLE `elements_sites` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `elements_sites`
--

INSERT INTO `elements_sites` (`id`, `elementId`, `siteId`, `slug`, `uri`, `enabled`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 1, NULL, NULL, 1, '2019-02-12 06:57:58', '2019-02-14 06:15:21', 'bc3c6c07-7293-4b4e-a6e6-8ac4de42cc23'),
(2, 2, 1, 'homepage', '__home__', 1, '2019-02-13 14:14:34', '2019-02-27 14:57:50', '808d6b03-84d9-4fc8-ada6-bfed817a12cb'),
(3, 3, 1, NULL, NULL, 1, '2019-02-13 14:32:17', '2019-02-13 14:32:33', '28bacc8f-763b-4d79-b073-aeb5cafbd54f'),
(4, 4, 1, NULL, NULL, 1, '2019-02-13 14:32:46', '2019-02-13 14:32:50', '21d61eeb-8c59-4436-837c-60c6c0951d62'),
(5, 5, 1, 'impressum', 'impressum', 1, '2019-02-13 14:37:11', '2019-02-18 07:06:59', 'bc220bf3-3974-434b-8d42-eb2e1eef7609'),
(6, 6, 1, 'datenschutzerklarung', 'datenschutzerklarung', 1, '2019-02-13 14:37:35', '2019-02-18 07:06:20', '96d0fabc-6106-4524-9ef9-f3cf7fb5a2a0'),
(7, 7, 1, 'agbs', 'agbs', 1, '2019-02-13 14:37:55', '2019-02-18 07:06:20', 'aaa7d5ad-942f-426d-94b3-1538b3efb4d1'),
(8, 8, 1, 'cookies', 'cookies', 1, '2019-02-13 14:38:09', '2019-02-18 07:06:20', 'b562322f-26ac-47b6-b72a-6da11f59b996'),
(9, 9, 1, 'footer-copyright-text', NULL, 1, '2019-02-14 10:18:18', '2019-02-18 08:17:48', 'e1dc2a46-d086-490f-a298-d98c70dec005'),
(10, 10, 1, 'website-managed-by-text', NULL, 1, '2019-02-14 10:29:20', '2019-02-18 08:17:48', '3d2b9b91-1f28-4152-872c-20fce569daaa'),
(11, 11, 1, 'trademarks-text', NULL, 1, '2019-02-14 10:35:54', '2019-02-18 08:17:48', 'c1558bf5-95f1-45ba-902c-9ab40dff84aa'),
(12, 12, 1, 'verfügbare-tickets', NULL, 1, '2019-02-14 11:18:50', '2019-02-18 08:17:48', '82a8fc2a-5e21-4b0d-8d95-a2c52ff1a7c1'),
(13, 13, 1, 'saalplan', NULL, 1, '2019-02-14 11:51:41', '2019-02-20 11:30:42', '69fccdf6-6f4d-4c87-8a47-6180421080e3'),
(14, 3, 2, NULL, NULL, 1, '2019-02-15 06:56:51', '2019-02-15 06:56:51', '1bc7507c-471c-49bd-b5e6-fdfaca9de3f1'),
(15, 4, 2, NULL, NULL, 1, '2019-02-15 06:56:51', '2019-02-15 06:56:51', '4da7b6bb-e1b4-42f6-ae59-6dfa2c6d5f12'),
(16, 14, 2, 'home-page-english', '__home__', 1, '2019-02-18 06:37:19', '2019-02-20 12:16:29', 'a68f7b7d-f1d0-4c69-a975-ce5b784140c5'),
(26, 15, 2, 'impressum-english', 'impressum-english', 1, '2019-02-18 07:07:45', '2019-02-18 07:07:47', '1dc70ae3-acf2-40b2-bb8b-abaa7cdbe181'),
(27, 16, 2, 'seating-plan', NULL, 1, '2019-02-18 08:18:44', '2019-02-20 12:16:58', 'c1cb97ab-d158-4d4d-a582-29b348c3cc40'),
(28, 17, 2, 'available-tickets', NULL, 1, '2019-02-18 08:19:31', '2019-02-18 08:19:33', '57f889c1-fa9f-4845-b1c6-d2911fca025f'),
(29, 18, 2, 'trademarks-text-english', NULL, 1, '2019-02-18 08:20:11', '2019-02-18 08:20:12', '66b919a2-6e30-40c6-8753-195886cf4849'),
(30, 19, 2, 'website-managed-by-text-english', NULL, 1, '2019-02-18 08:21:26', '2019-02-18 08:21:28', 'f196081b-5ce8-4cec-96df-4385052744c0'),
(31, 20, 2, 'footer-copyright-text-english', NULL, 1, '2019-02-18 08:22:12', '2019-02-18 08:22:14', '9eafd252-4d8b-47fe-b8de-74e397bb92c0'),
(32, 21, 2, 'data-protection', 'data-protection', 1, '2019-02-18 08:23:03', '2019-02-18 08:23:05', 'be56397e-edd5-475d-a172-904ce4686d38'),
(33, 22, 2, 'agbs-english', 'agbs-english', 1, '2019-02-18 08:23:58', '2019-02-18 08:24:00', '62a684f2-be48-44b1-abbb-59474b6122e0'),
(34, 23, 2, 'cookies-english', 'cookies-english', 1, '2019-02-18 08:24:19', '2019-02-18 08:24:21', 'c9421065-1f87-4ce0-b68e-af10fff30c46'),
(35, 24, 2, 'footer-logo-block-english', NULL, 1, '2019-02-20 12:22:05', '2019-02-20 12:22:28', '098798b8-d9a9-445c-bdc3-5a78fd52ea75'),
(36, 25, 1, 'footer-logo-block', NULL, 1, '2019-02-20 12:26:23', '2019-02-20 12:26:25', '9ccf7c3e-0570-4b03-b2be-e8752b46e8af'),
(37, 26, 1, 'process', 'process', 1, '2019-02-25 13:41:04', '2019-02-25 13:49:53', 'a7ebe295-29dc-4612-b371-93dfe3d622ce'),
(38, 26, 2, 'process', 'process', 1, '2019-02-25 13:41:04', '2019-02-25 13:49:53', '9d11aa51-0dcd-4fcb-a6bc-af47c335571d'),
(39, 27, 1, 'presale', 'presale', 1, '2019-02-26 06:41:43', '2019-02-27 15:00:27', '70105ddb-ba65-4fe8-aad7-1e6ab41b4197'),
(40, 27, 2, 'presale', 'presale', 1, '2019-02-26 06:41:44', '2019-02-27 15:00:27', '69ef6fd3-cf43-4c17-9f2f-255812ec5853'),
(41, 28, 1, NULL, NULL, 1, '2019-02-27 11:30:07', '2019-02-27 11:30:07', '3adb58a7-816c-4aeb-8c1d-4a7bb370fe95'),
(42, 28, 2, NULL, NULL, 1, '2019-02-27 11:30:08', '2019-02-27 11:30:08', '906ef3d8-ca8e-404c-bf0a-a484da872708'),
(43, 29, 1, NULL, NULL, 1, '2019-02-27 11:31:00', '2019-02-27 11:31:00', 'e06af5a4-ada5-4ca1-b1f8-a917fb1c7238'),
(44, 29, 2, NULL, NULL, 1, '2019-02-27 11:31:11', '2019-02-27 11:31:11', '657cb147-0882-484a-bd06-263eb23db006');

-- --------------------------------------------------------

--
-- Table structure for table `entries`
--

CREATE TABLE `entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `typeId` int(11) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `deletedWithEntryType` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `entries`
--

INSERT INTO `entries` (`id`, `sectionId`, `parentId`, `typeId`, `authorId`, `postDate`, `expiryDate`, `deletedWithEntryType`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(2, 2, NULL, 2, NULL, '2019-02-13 14:14:00', NULL, NULL, '2019-02-13 14:14:34', '2019-02-27 14:57:50', 'a2f20950-4f3f-4ac4-a230-43b383cd4600'),
(5, 1, NULL, 1, 1, '2019-02-13 14:37:00', NULL, NULL, '2019-02-13 14:37:11', '2019-02-18 07:06:59', '23d07e02-08cc-424c-9032-30092e38975e'),
(6, 1, NULL, 1, 1, '2019-02-13 14:37:00', NULL, NULL, '2019-02-13 14:37:35', '2019-02-18 07:06:20', '2d81907a-12e7-42b2-8271-df748937a364'),
(7, 1, NULL, 1, 1, '2019-02-13 14:37:00', NULL, NULL, '2019-02-13 14:37:55', '2019-02-18 07:06:20', '1db92f8c-0cbc-4be9-b1d3-e5f2afacbd17'),
(8, 1, NULL, 1, 1, '2019-02-13 14:38:00', NULL, NULL, '2019-02-13 14:38:09', '2019-02-18 07:06:20', 'dc0267f4-9173-4fc0-8a8d-049db47b8658'),
(9, 3, NULL, 3, 1, '2019-02-14 10:18:00', NULL, NULL, '2019-02-14 10:18:18', '2019-02-18 08:17:48', 'a27d4ef2-5c38-4be9-abf2-b219789cd27e'),
(10, 3, NULL, 3, 1, '2019-02-14 10:29:00', NULL, NULL, '2019-02-14 10:29:20', '2019-02-18 08:17:48', 'e72a5cca-d24e-4d63-ac5a-2595a3208684'),
(11, 3, NULL, 3, 1, '2019-02-14 10:35:00', NULL, NULL, '2019-02-14 10:35:54', '2019-02-18 08:17:48', 'cb22ad96-3557-46eb-aff4-5544bb4f1779'),
(12, 3, NULL, 3, 1, '2019-02-14 11:18:00', NULL, NULL, '2019-02-14 11:18:50', '2019-02-18 08:17:48', 'd2a3a091-18e5-4c8a-82ae-b8cfa0ca4d61'),
(13, 3, NULL, 3, 1, '2019-02-14 11:51:00', NULL, NULL, '2019-02-14 11:51:41', '2019-02-20 11:30:42', '39f9791b-18cc-4e7f-8033-67546993ceb6'),
(14, 4, NULL, 4, NULL, '2019-02-18 06:37:00', NULL, NULL, '2019-02-18 06:37:19', '2019-02-20 12:16:29', '38890790-417c-4497-b0fc-27397b841f49'),
(15, 1, NULL, 1, 1, '2019-02-18 07:07:00', NULL, NULL, '2019-02-18 07:07:45', '2019-02-18 07:07:45', 'bf52f8ef-b089-479a-8b50-07362626cca8'),
(16, 3, NULL, 3, 1, '2019-02-18 08:18:00', NULL, NULL, '2019-02-18 08:18:44', '2019-02-20 12:16:58', 'e34d4946-2f76-4678-9b0d-626cff9b503e'),
(17, 3, NULL, 3, 1, '2019-02-18 08:19:00', NULL, NULL, '2019-02-18 08:19:31', '2019-02-18 08:19:31', 'ee85704e-720e-418b-81fb-e14569d6ef8d'),
(18, 3, NULL, 3, 1, '2019-02-18 08:20:00', NULL, NULL, '2019-02-18 08:20:11', '2019-02-18 08:20:11', '476daa5f-6317-4119-822e-dc706d797400'),
(19, 3, NULL, 3, 1, '2019-02-18 08:21:00', NULL, NULL, '2019-02-18 08:21:26', '2019-02-18 08:21:26', '00b43819-bc69-4383-ad71-7150529ab479'),
(20, 3, NULL, 3, 1, '2019-02-18 08:22:00', NULL, NULL, '2019-02-18 08:22:12', '2019-02-18 08:22:12', 'fda442f8-018b-4dcd-a767-393f4a3cd05a'),
(21, 1, NULL, 1, 1, '2019-02-18 08:23:00', NULL, NULL, '2019-02-18 08:23:03', '2019-02-18 08:23:03', '36d54d80-c94c-441f-a46d-a1339bdee693'),
(22, 1, NULL, 1, 1, '2019-02-18 08:23:00', NULL, NULL, '2019-02-18 08:23:58', '2019-02-18 08:23:58', '4451ee42-a846-4377-99fe-0b28791bd325'),
(23, 1, NULL, 1, 1, '2019-02-18 08:24:00', NULL, NULL, '2019-02-18 08:24:19', '2019-02-18 08:24:19', '130952e2-aeec-4bca-be8d-862189318451'),
(24, 3, NULL, 3, 1, '2019-02-20 12:22:00', NULL, NULL, '2019-02-20 12:22:05', '2019-02-20 12:22:28', 'e545b2e3-d523-48eb-b148-8252c0fbe918'),
(25, 3, NULL, 3, 1, '2019-02-20 12:26:00', NULL, NULL, '2019-02-20 12:26:24', '2019-02-20 12:26:24', '9b45deb8-1317-4ce2-adfc-0649b611ce72'),
(26, 5, NULL, 5, NULL, '2019-02-25 13:41:00', NULL, 1, '2019-02-25 13:41:04', '2019-02-25 13:49:53', '0652352c-166b-4d79-a544-ecc1f380a344'),
(27, 6, NULL, 6, NULL, '2019-02-26 06:41:00', NULL, NULL, '2019-02-26 06:41:43', '2019-02-27 15:00:27', '85684c4f-1e17-4ea4-b519-0b3daeeda2b4');

-- --------------------------------------------------------

--
-- Table structure for table `entrydrafts`
--

CREATE TABLE `entrydrafts` (
  `id` int(11) NOT NULL,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `notes` text,
  `data` mediumtext NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `entrytypes`
--

CREATE TABLE `entrytypes` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `hasTitleField` tinyint(1) NOT NULL DEFAULT '1',
  `titleLabel` varchar(255) DEFAULT 'Title',
  `titleFormat` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `entrytypes`
--

INSERT INTO `entrytypes` (`id`, `sectionId`, `fieldLayoutId`, `name`, `handle`, `hasTitleField`, `titleLabel`, `titleFormat`, `sortOrder`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 1, 1, 'Basic Page', 'basicPage', 1, 'Title', NULL, 1, '2019-02-13 14:13:49', '2019-02-13 14:22:39', NULL, '5516b7f3-f978-478f-b6fe-d6f9fd4529ac'),
(2, 2, 2, 'Homepage', 'homepage', 0, NULL, '{section.name|raw}', 1, '2019-02-13 14:14:34', '2019-02-13 14:23:35', NULL, 'c656bd59-09b9-49db-b5c2-5f1b76865b51'),
(3, 3, 4, 'Editable Blocks', 'editableBlocks', 1, 'Title', NULL, 1, '2019-02-14 10:17:27', '2019-02-14 10:17:40', NULL, 'b4a8a7da-aec1-4a30-825a-2a30c53c3989'),
(4, 4, 5, 'HomePage English', 'homepageEnglish', 0, '', '{section.name|raw}', 1, '2019-02-18 06:37:18', '2019-02-20 12:11:04', NULL, '40bf0892-5726-4ea7-9dc5-397489d5e941'),
(5, 5, NULL, 'process', 'process', 0, NULL, '{section.name|raw}', 1, '2019-02-25 13:41:04', '2019-02-25 13:41:04', '2019-02-25 13:49:53', 'c9e6da84-4817-4d8f-b62a-a611cec4b003'),
(6, 6, 6, 'Presale', 'presale', 0, '', '{section.name|raw}', 1, '2019-02-26 06:41:43', '2019-02-26 12:51:33', NULL, '364de46c-1368-431c-a67e-b625cc680e62');

-- --------------------------------------------------------

--
-- Table structure for table `entryversions`
--

CREATE TABLE `entryversions` (
  `id` int(11) NOT NULL,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `siteId` int(11) NOT NULL,
  `num` smallint(6) UNSIGNED NOT NULL,
  `notes` text,
  `data` mediumtext NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `entryversions`
--

INSERT INTO `entryversions` (`id`, `entryId`, `sectionId`, `creatorId`, `siteId`, `num`, `notes`, `data`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 2, 2, 1, 1, 1, 'Revision from Feb 13, 2019, 6:23:36 AM', '{"typeId":"2","authorId":null,"title":"Homepage","slug":"homepage","postDate":1550067240,"expiryDate":null,"enabled":"1","newParentId":null,"fields":{"2":[],"3":[]}}', '2019-02-13 14:32:55', '2019-02-13 14:32:55', '9bdc5205-45b5-4d14-9e19-6042fe53cb51'),
(2, 2, 2, 1, 1, 2, '', '{"typeId":"2","authorId":null,"title":"Homepage","slug":"homepage","postDate":1550067240,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<p> </p>"}}', '2019-02-13 14:32:56', '2019-02-13 14:32:56', 'a4e01758-afd2-4aaf-8dc5-9f7c8deb5b31'),
(3, 5, 1, 1, 1, 1, '', '{"typeId":"1","authorId":"1","title":"Impressum","slug":"impressum","postDate":1550068620,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p><strong>Eigentumer und Betreiber der Webseite ist: </strong></p><p>Aka Promotions Ltd </p><p>115 Shaftesbury Avenue,</p><p> Cambridge Circus, London, WC2H 8AF</p><p> </p><p>E-Mail: webteam@akauk.com </p><p>Telefon: +44(0)20 7836 4747 </p><p> </p><p><strong>Produzent fur Harry Potter und das verwunschene Kind in Deutschland ist: </strong></p><p>Mehr--BB Enterainment GmbH </p><p>Erkrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p><strong>Geschaftsfuhrer: </strong></p><p>Maik Klokow, Ralf Kokemuller, Gunter Irmler </p><p>Amtsgericht Dusseldorf - HRB 57980</p><p> </p><p>webmaster@mehr-entertainment.de </p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p><p><strong>Ticketing </strong></p><p>Deutsche Eintrittskarten TKS GmbH - www.eintrittskarten.de </p><p>Banksstr. 28 </p><p>D - 20097 Hamburg</p><p> </p><p>Buro Dusseldorf: </p><p>Erlrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p>Geschaftsfuhrer: Kai Ricke </p><p>Amtsgericht Hamburg - HRB 121731 </p><p>Ust-IdNr: DE 812 702 791</p><p> </p><p>webmaster@mehr-entertainment.de</p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p>"}}', '2019-02-13 14:37:12', '2019-02-13 14:37:12', '272445cf-f61f-4feb-952d-c5edfb63ddfe'),
(4, 6, 1, 1, 1, 1, '', '{"typeId":"1","authorId":"1","title":"Datenschutzerklarung","slug":"datenschutzerklarung","postDate":1550068620,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<h2> </h2><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><h2>1 AKA</h2><p>1.1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><h2>2 USE OF THE SITE</h2><p>2.1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.2 Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p><p>(a) Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>(b) Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>(c) Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>(d) Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.3 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.4 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.5 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.6 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>"}}', '2019-02-13 14:37:35', '2019-02-13 14:37:35', 'f65b47f0-1369-494e-8197-1bc9b9a1666c'),
(5, 7, 1, 1, 1, 1, '', '{"typeId":"1","authorId":"1","title":"Agbs","slug":"agbs","postDate":1550068620,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc accumsan ornare euismod. Vestibulum tincidunt massa at augue faucibus finibus. Duis iaculis sapien quis orci commodo, id commodo tortor volutpat. Etiam luctus est sapien, vel tempor diam cursus et. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque ornare a ipsum at posuere. Maecenas sollicitudin leo id ipsum pellentesque tincidunt. Sed velit quam, gravida at ornare sed, gravida a nisl. Curabitur gravida fringilla erat a venenatis. Nam posuere sit amet lorem ut blandit. Praesent ligula lectus, imperdiet eu leo id, aliquet commodo nisl. Ut venenatis sem commodo auctor gravida. Nulla facilisi. Integer congue at dui nec venenatis. Suspendisse facilisis enim nec porttitor euismod.</p><p>Aliquam ornare, ante pretium feugiat elementum, dui quam suscipit ipsum, a sagittis est libero id diam. Nunc viverra, ante vitae maximus placerat, risus dolor luctus elit, ac aliquet purus erat vel urna. Praesent varius urna purus, sit amet semper turpis scelerisque ac. Morbi aliquet nibh mauris, vel lobortis quam iaculis ut. Aliquam rutrum odio ut urna consectetur fringilla. Suspendisse sit amet urna vitae augue sodales pharetra. Etiam vel felis elit. Etiam at finibus velit, condimentum fringilla nibh. Morbi dignissim sollicitudin diam, ut elementum nisi auctor vitae.</p><p>Etiam egestas dignissim ultricies. Pellentesque vel arcu sed lectus feugiat iaculis et ac tortor. In sit amet maximus arcu, et vestibulum neque. Quisque sollicitudin elit neque, lacinia convallis leo lacinia quis. Sed semper elit orci. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur at sagittis nibh, a egestas ipsum. Ut tellus diam, dapibus nec purus a, porttitor iaculis diam. Sed sagittis mauris eu lacinia ultricies. Pellentesque volutpat in odio pellentesque facilisis.</p><p>Aenean pretium massa ac nisl blandit, aliquet imperdiet mi mollis. Integer erat lacus, finibus et diam sed, vestibulum lobortis quam. Sed porttitor, magna in malesuada accumsan, turpis ipsum placerat lectus, nec interdum tortor leo ut dui. Nunc commodo lorem ut fermentum scelerisque. Aenean id leo malesuada, dapibus diam id, convallis libero. Fusce auctor tortor id risus ultrices tempus. Integer pretium gravida leo, nec iaculis velit commodo ut. Suspendisse posuere ipsum non magna volutpat, sit amet luctus massa scelerisque. Donec dictum tincidunt massa ut gravida.</p><p>Integer fermentum felis sapien, nec rhoncus mi consequat in. Mauris eu dictum risus. Nulla tincidunt a felis nec rhoncus. Aliquam placerat nisi augue, sit amet tincidunt tortor dictum eu. Integer hendrerit rutrum dui, eget pellentesque risus. Duis et molestie risus. Cras lacinia vel dui quis imperdiet. Integer interdum blandit ligula ac congue. Donec semper euismod rhoncus.</p>"}}', '2019-02-13 14:37:55', '2019-02-13 14:37:55', '353906c1-d02c-4588-be42-95bfc35c2b6f'),
(6, 8, 1, 1, 1, 1, '', '{"typeId":"1","authorId":"1","title":"Cookies","slug":"cookies","postDate":1550068680,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc accumsan ornare euismod. Vestibulum tincidunt massa at augue faucibus finibus. Duis iaculis sapien quis orci commodo, id commodo tortor volutpat. Etiam luctus est sapien, vel tempor diam cursus et. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque ornare a ipsum at posuere. Maecenas sollicitudin leo id ipsum pellentesque tincidunt. Sed velit quam, gravida at ornare sed, gravida a nisl. Curabitur gravida fringilla erat a venenatis. Nam posuere sit amet lorem ut blandit. Praesent ligula lectus, imperdiet eu leo id, aliquet commodo nisl. Ut venenatis sem commodo auctor gravida. Nulla facilisi. Integer congue at dui nec venenatis. Suspendisse facilisis enim nec porttitor euismod.</p><p>Aliquam ornare, ante pretium feugiat elementum, dui quam suscipit ipsum, a sagittis est libero id diam. Nunc viverra, ante vitae maximus placerat, risus dolor luctus elit, ac aliquet purus erat vel urna. Praesent varius urna purus, sit amet semper turpis scelerisque ac. Morbi aliquet nibh mauris, vel lobortis quam iaculis ut. Aliquam rutrum odio ut urna consectetur fringilla. Suspendisse sit amet urna vitae augue sodales pharetra. Etiam vel felis elit. Etiam at finibus velit, condimentum fringilla nibh. Morbi dignissim sollicitudin diam, ut elementum nisi auctor vitae.</p><p>Etiam egestas dignissim ultricies. Pellentesque vel arcu sed lectus feugiat iaculis et ac tortor. In sit amet maximus arcu, et vestibulum neque. Quisque sollicitudin elit neque, lacinia convallis leo lacinia quis. Sed semper elit orci. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur at sagittis nibh, a egestas ipsum. Ut tellus diam, dapibus nec purus a, porttitor iaculis diam. Sed sagittis mauris eu lacinia ultricies. Pellentesque volutpat in odio pellentesque facilisis.</p><p>Aenean pretium massa ac nisl blandit, aliquet imperdiet mi mollis. Integer erat lacus, finibus et diam sed, vestibulum lobortis quam. Sed porttitor, magna in malesuada accumsan, turpis ipsum placerat lectus, nec interdum tortor leo ut dui. Nunc commodo lorem ut fermentum scelerisque. Aenean id leo malesuada, dapibus diam id, convallis libero. Fusce auctor tortor id risus ultrices tempus. Integer pretium gravida leo, nec iaculis velit commodo ut. Suspendisse posuere ipsum non magna volutpat, sit amet luctus massa scelerisque. Donec dictum tincidunt massa ut gravida.</p><p>Integer fermentum felis sapien, nec rhoncus mi consequat in. Mauris eu dictum risus. Nulla tincidunt a felis nec rhoncus. Aliquam placerat nisi augue, sit amet tincidunt tortor dictum eu. Integer hendrerit rutrum dui, eget pellentesque risus. Duis et molestie risus. Cras lacinia vel dui quis imperdiet. Integer interdum blandit ligula ac congue. Donec semper euismod rhoncus.</p>"}}', '2019-02-13 14:38:09', '2019-02-13 14:38:09', '1b7efe6c-7da9-4ce1-972b-37d8dd2ef17c'),
(7, 9, 3, 1, 1, 1, '', '{"typeId":"3","authorId":"1","title":"Footer Copyright Text","slug":"footer-copyright-text","postDate":1550139480,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>Harry Potter Publishing and Theatrical Rights © J.k. Rowling</p>"}}', '2019-02-14 10:18:18', '2019-02-14 10:18:18', '5ac737fd-c323-4794-b5aa-870d9fd5d1ae'),
(8, 10, 3, 1, 1, 1, '', '{"typeId":"3","authorId":"1","title":"Website Managed by text","slug":"website-managed-by-text","postDate":1550140140,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>Webseite wird gemanaged von AKA</p>"}}', '2019-02-14 10:29:20', '2019-02-14 10:29:20', 'e6c3cb6f-fb10-4203-9a6d-0a1ed3bd980f'),
(9, 10, 3, 1, 1, 2, '', '{"typeId":"3","authorId":"1","title":"Website Managed by text","slug":"website-managed-by-text","postDate":1550140140,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>Webseite wird gemanaged von <a href=\\"http://www.google.com\\">AKA</a></p>"}}', '2019-02-14 10:29:47', '2019-02-14 10:29:47', 'fa041661-7c87-41c0-a561-978b1ccc6b0e'),
(10, 11, 3, 1, 1, 1, '', '{"typeId":"3","authorId":"1","title":"Trademarks text","slug":"trademarks-text","postDate":1550140500,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>Harry Portter Characters, Name and related indicia are Trademarks of and © Warner Bros. Ent. Alle Rechte vorberhalten.</p>"}}', '2019-02-14 10:35:54', '2019-02-14 10:35:54', '9862cc9a-f4d6-445d-9ced-e9b49a52edce'),
(11, 12, 3, 1, 1, 1, '', '{"typeId":"3","authorId":"1","title":"Verfügbare Tickets","slug":"verfügbare-tickets","postDate":1550143080,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>Es glbt noch letzte Elnzelplatze fur Vorstellungen Irn Januar</p>"}}', '2019-02-14 11:18:51', '2019-02-14 11:18:51', '40f4cf06-11c5-40fa-a3c5-f717555ab88e'),
(12, 12, 3, 1, 1, 2, '', '{"typeId":"3","authorId":"1","title":"Verfügbare Tickets2","slug":"verfügbare-tickets","postDate":1550143080,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>Es glbt noch letzte Elnzelplatze fur Vorstellungen Irn Januar</p>"}}', '2019-02-14 11:21:25', '2019-02-14 11:21:25', '0c588d18-2221-45b1-9abc-8e3708f2f15b'),
(13, 12, 3, 1, 1, 3, '', '{"typeId":"3","authorId":"1","title":"Verfügbare Tickets","slug":"verfügbare-tickets","postDate":1550143080,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>Es glbt noch letzte Elnzelplatze fur Vorstellungen Irn Januar</p>"}}', '2019-02-14 11:25:57', '2019-02-14 11:25:57', 'c409c55a-9f0c-4dc6-83eb-93fb4a1cf62e'),
(14, 13, 3, 1, 1, 1, '', '{"typeId":"3","authorId":"1","title":"Saalplan","slug":"saalplan","postDate":1550145060,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>Mehr!Theater am Grobmarkt, Hamburg</p>"}}', '2019-02-14 11:51:41', '2019-02-14 11:51:41', '7fb1dafc-8478-47e4-8335-c2e6bd531fdb'),
(15, 5, 1, 1, 2, 1, 'Revision from Feb 15, 2019, 3:22:59 AM', '{"typeId":"1","authorId":"1","title":"Impressum","slug":"impressum","postDate":1550068620,"expiryDate":null,"enabled":"1","newParentId":null,"fields":{"1":"<p><strong>Eigentumer und Betreiber der Webseite ist: </strong></p><p>Aka Promotions Ltd </p><p>115 Shaftesbury Avenue,</p><p> Cambridge Circus, London, WC2H 8AF</p><p> </p><p>E-Mail: webteam@akauk.com </p><p>Telefon: +44(0)20 7836 4747 </p><p> </p><p><strong>Produzent fur Harry Potter und das verwunschene Kind in Deutschland ist: </strong></p><p>Mehr--BB Enterainment GmbH </p><p>Erkrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p><strong>Geschaftsfuhrer: </strong></p><p>Maik Klokow, Ralf Kokemuller, Gunter Irmler </p><p>Amtsgericht Dusseldorf - HRB 57980</p><p> </p><p>webmaster@mehr-entertainment.de </p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p><p><strong>Ticketing </strong></p><p>Deutsche Eintrittskarten TKS GmbH - www.eintrittskarten.de </p><p>Banksstr. 28 </p><p>D - 20097 Hamburg</p><p> </p><p>Buro Dusseldorf: </p><p>Erlrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p>Geschaftsfuhrer: Kai Ricke </p><p>Amtsgericht Hamburg - HRB 121731 </p><p>Ust-IdNr: DE 812 702 791</p><p> </p><p>webmaster@mehr-entertainment.de</p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p>"}}', '2019-02-15 11:24:27', '2019-02-15 11:24:27', '3bbd1eff-7cdb-4547-9eb6-1de281664969'),
(16, 5, 1, 1, 2, 2, '', '{"typeId":"1","authorId":"1","title":"Impressum in eglish","slug":"impressum","postDate":1550068620,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p><strong>Eigentumer und Betreiber der Webseite ist: </strong></p><p>Aka Promotions Ltd </p><p>115 Shaftesbury Avenue,</p><p> Cambridge Circus, London, WC2H 8AF</p><p> </p><p>E-Mail: webteam@akauk.com </p><p>Telefon: +44(0)20 7836 4747 </p><p> </p><p><strong>Produzent fur Harry Potter und das verwunschene Kind in Deutschland ist: </strong></p><p>Mehr--BB Enterainment GmbH </p><p>Erkrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p><strong>Geschaftsfuhrer: </strong></p><p>Maik Klokow, Ralf Kokemuller, Gunter Irmler </p><p>Amtsgericht Dusseldorf - HRB 57980</p><p> </p><p>webmaster@mehr-entertainment.de </p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p><p><strong>Ticketing </strong></p><p>Deutsche Eintrittskarten TKS GmbH - www.eintrittskarten.de </p><p>Banksstr. 28 </p><p>D - 20097 Hamburg</p><p> </p><p>Buro Dusseldorf: </p><p>Erlrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p>Geschaftsfuhrer: Kai Ricke </p><p>Amtsgericht Hamburg - HRB 121731 </p><p>Ust-IdNr: DE 812 702 791</p><p> </p><p>webmaster@mehr-entertainment.de</p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p>"}}', '2019-02-15 11:24:27', '2019-02-15 11:24:27', 'bc4c0f8d-05d9-492d-91df-baf5f9b880d8'),
(17, 5, 1, 1, 2, 3, '', '{"typeId":"1","authorId":"1","title":"Impressum in eglish","slug":"impressum","postDate":1550068620,"expiryDate":null,"enabled":false,"newParentId":"","fields":{"1":"<p><strong>Eigentumer und Betreiber der Webseite ist: </strong></p><p>Aka Promotions Ltd </p><p>115 Shaftesbury Avenue,</p><p> Cambridge Circus, London, WC2H 8AF</p><p> </p><p>E-Mail: webteam@akauk.com </p><p>Telefon: +44(0)20 7836 4747 </p><p> </p><p><strong>Produzent fur Harry Potter und das verwunschene Kind in Deutschland ist: </strong></p><p>Mehr--BB Enterainment GmbH </p><p>Erkrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p><strong>Geschaftsfuhrer: </strong></p><p>Maik Klokow, Ralf Kokemuller, Gunter Irmler </p><p>Amtsgericht Dusseldorf - HRB 57980</p><p> </p><p>webmaster@mehr-entertainment.de </p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p><p><strong>Ticketing </strong></p><p>Deutsche Eintrittskarten TKS GmbH - www.eintrittskarten.de </p><p>Banksstr. 28 </p><p>D - 20097 Hamburg</p><p> </p><p>Buro Dusseldorf: </p><p>Erlrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p>Geschaftsfuhrer: Kai Ricke </p><p>Amtsgericht Hamburg - HRB 121731 </p><p>Ust-IdNr: DE 812 702 791</p><p> </p><p>webmaster@mehr-entertainment.de</p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p>"}}', '2019-02-15 11:26:43', '2019-02-15 11:26:43', '94edc7c2-34b4-4e77-ada8-d367ad40f4bf'),
(18, 5, 1, 1, 2, 4, '', '{"typeId":"1","authorId":"1","title":"Impressum in eglish","slug":"impressum","postDate":1550068620,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p><strong>Eigentumer und Betreiber der Webseite ist: </strong></p><p>Aka Promotions Ltd </p><p>115 Shaftesbury Avenue,</p><p> Cambridge Circus, London, WC2H 8AF</p><p> </p><p>E-Mail: webteam@akauk.com </p><p>Telefon: +44(0)20 7836 4747 </p><p> </p><p><strong>Produzent fur Harry Potter und das verwunschene Kind in Deutschland ist: </strong></p><p>Mehr--BB Enterainment GmbH </p><p>Erkrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p><strong>Geschaftsfuhrer: </strong></p><p>Maik Klokow, Ralf Kokemuller, Gunter Irmler </p><p>Amtsgericht Dusseldorf - HRB 57980</p><p> </p><p>webmaster@mehr-entertainment.de </p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p><p><strong>Ticketing </strong></p><p>Deutsche Eintrittskarten TKS GmbH - www.eintrittskarten.de </p><p>Banksstr. 28 </p><p>D - 20097 Hamburg</p><p> </p><p>Buro Dusseldorf: </p><p>Erlrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p>Geschaftsfuhrer: Kai Ricke </p><p>Amtsgericht Hamburg - HRB 121731 </p><p>Ust-IdNr: DE 812 702 791</p><p> </p><p>webmaster@mehr-entertainment.de</p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p>"}}', '2019-02-15 11:26:58', '2019-02-15 11:26:58', 'ab33b4f8-e6b6-43cb-91a5-beec93d1d417'),
(19, 14, 4, 1, 2, 1, 'Revision from Feb 17, 2019, 10:37:52 PM', '{"typeId":"4","authorId":null,"title":"HomePage English","slug":"home-page-english","postDate":1550471820,"expiryDate":null,"enabled":"1","newParentId":null,"fields":{"2":[],"3":[]}}', '2019-02-18 06:40:21', '2019-02-18 06:40:21', '8c685533-a53d-496f-8365-4d4893195c15'),
(20, 14, 4, 1, 2, 2, '', '{"typeId":"4","authorId":null,"title":"HomePage English","slug":"home-page-english","postDate":1550471820,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"]}}', '2019-02-18 06:40:21', '2019-02-18 06:40:21', '91556ccb-75d0-4b7f-b96b-04a00ad3407e'),
(21, 5, 1, 1, 2, 5, '', '{"typeId":"1","authorId":"1","title":"Impressum","slug":"impressum","postDate":1550068620,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p><strong>Eigentumer und Betreiber der Webseite ist: </strong></p><p>Aka Promotions Ltd </p><p>115 Shaftesbury Avenue,</p><p> Cambridge Circus, London, WC2H 8AF</p><p> </p><p>E-Mail: webteam@akauk.com </p><p>Telefon: +44(0)20 7836 4747 </p><p> </p><p><strong>Produzent fur Harry Potter und das verwunschene Kind in Deutschland ist: </strong></p><p>Mehr--BB Enterainment GmbH </p><p>Erkrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p><strong>Geschaftsfuhrer: </strong></p><p>Maik Klokow, Ralf Kokemuller, Gunter Irmler </p><p>Amtsgericht Dusseldorf - HRB 57980</p><p> </p><p>webmaster@mehr-entertainment.de </p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p><p><strong>Ticketing </strong></p><p>Deutsche Eintrittskarten TKS GmbH - www.eintrittskarten.de </p><p>Banksstr. 28 </p><p>D - 20097 Hamburg</p><p> </p><p>Buro Dusseldorf: </p><p>Erlrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p>Geschaftsfuhrer: Kai Ricke </p><p>Amtsgericht Hamburg - HRB 121731 </p><p>Ust-IdNr: DE 812 702 791</p><p> </p><p>webmaster@mehr-entertainment.de</p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p>"}}', '2019-02-18 06:50:31', '2019-02-18 06:50:31', '056487a2-c856-49f5-bf1c-aca0272927dc'),
(22, 5, 1, 1, 2, 6, '', '{"typeId":"1","authorId":"1","title":"Impressum EN","slug":"impressum","postDate":1550068620,"expiryDate":null,"enabled":false,"newParentId":"","fields":{"1":"<p><strong>Eigentumer und Betreiber der Webseite ist: </strong></p><p>Aka Promotions Ltd </p><p>115 Shaftesbury Avenue,</p><p> Cambridge Circus, London, WC2H 8AF</p><p> </p><p>E-Mail: webteam@akauk.com </p><p>Telefon: +44(0)20 7836 4747 </p><p> </p><p><strong>Produzent fur Harry Potter und das verwunschene Kind in Deutschland ist: </strong></p><p>Mehr--BB Enterainment GmbH </p><p>Erkrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p><strong>Geschaftsfuhrer: </strong></p><p>Maik Klokow, Ralf Kokemuller, Gunter Irmler </p><p>Amtsgericht Dusseldorf - HRB 57980</p><p> </p><p>webmaster@mehr-entertainment.de </p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p><p><strong>Ticketing </strong></p><p>Deutsche Eintrittskarten TKS GmbH - www.eintrittskarten.de </p><p>Banksstr. 28 </p><p>D - 20097 Hamburg</p><p> </p><p>Buro Dusseldorf: </p><p>Erlrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p>Geschaftsfuhrer: Kai Ricke </p><p>Amtsgericht Hamburg - HRB 121731 </p><p>Ust-IdNr: DE 812 702 791</p><p> </p><p>webmaster@mehr-entertainment.de</p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p>"}}', '2019-02-18 06:59:43', '2019-02-18 06:59:43', '03afd58b-997b-4849-8d6d-e48b945dbdca'),
(23, 15, 1, 1, 2, 1, '', '{"typeId":"1","authorId":"1","title":"Impressum English","slug":"impressum-english","postDate":1550473620,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p><strong>Eigentumer und Betreiber der Webseite ist: </strong></p><p>Aka Promotions Ltd </p><p>115 Shaftesbury Avenue,</p><p> Cambridge Circus, London, WC2H 8AF</p><p> </p><p>E-Mail: webteam@akauk.com </p><p>Telefon: +44(0)20 7836 4747 </p><p> </p><p><strong>Produzent fur Harry Potter und das verwunschene Kind in Deutschland ist: </strong></p><p>Mehr--BB Enterainment GmbH </p><p>Erkrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p><strong>Geschaftsfuhrer: </strong></p><p>Maik Klokow, Ralf Kokemuller, Gunter Irmler </p><p>Amtsgericht Dusseldorf - HRB 57980</p><p> </p><p>webmaster@mehr-entertainment.de </p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p><p><strong>Ticketing </strong></p><p>Deutsche Eintrittskarten TKS GmbH - www.eintrittskarten.de </p><p>Banksstr. 28 </p><p>D - 20097 Hamburg</p><p> </p><p>Buro Dusseldorf: </p><p>Erlrather Str. 30 </p><p>D - 40233 Dusseldorf</p><p> </p><p>Geschaftsfuhrer: Kai Ricke </p><p>Amtsgericht Hamburg - HRB 121731 </p><p>Ust-IdNr: DE 812 702 791</p><p> </p><p>webmaster@mehr-entertainment.de</p><p>Fon +49 (0) 211 / 73 44 -151 </p><p>Fax +49 (0) 211 / 73 44 -155 </p><p> </p>"}}', '2019-02-18 07:07:45', '2019-02-18 07:07:45', '49625eea-fdba-4fc5-8169-fb5f13acaf67'),
(24, 16, 3, 1, 2, 1, '', '{"typeId":"3","authorId":"1","title":"Seating plan","slug":"seating-plan","postDate":1550477880,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>More! Theater am Grobmarkt, Hambur</p>"}}', '2019-02-18 08:18:44', '2019-02-18 08:18:44', '6c3243a7-d07c-42df-a98d-082bb2d89336'),
(25, 17, 3, 1, 2, 1, '', '{"typeId":"3","authorId":"1","title":"Available tickets","slug":"available-tickets","postDate":1550477940,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>It still gleams last Elnzelplatze for performances in January</p>"}}', '2019-02-18 08:19:31', '2019-02-18 08:19:31', '08dc5779-847e-4690-a09d-fd45a1fd3737'),
(26, 18, 3, 1, 2, 1, '', '{"typeId":"3","authorId":"1","title":"Trademarks text English","slug":"trademarks-text-english","postDate":1550478000,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>Harry Portter Characters, Name and related indicia are Trademarks of and © Warner Bros. Ent. Alle Rechte vorberhalten.</p>"}}', '2019-02-18 08:20:11', '2019-02-18 08:20:11', '1ac0e407-5abd-46b4-9eef-3f7b534dd6fe'),
(27, 19, 3, 1, 2, 1, '', '{"typeId":"3","authorId":"1","title":"Website Managed by text English","slug":"website-managed-by-text-english","postDate":1550478060,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>Website is managed by <a href=\\"http://www/google.com\\">AKA</a></p>"}}', '2019-02-18 08:21:26', '2019-02-18 08:21:26', 'f1ccb7bb-bac2-4063-bdb6-b7d9e4b60edb'),
(28, 20, 3, 1, 2, 1, '', '{"typeId":"3","authorId":"1","title":"Footer Copyright Text English","slug":"footer-copyright-text-english","postDate":1550478120,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>Harry Potter Publishing and Theatrical Rights © J.k. Rowling</p>"}}', '2019-02-18 08:22:12', '2019-02-18 08:22:12', 'b48f87dc-03e4-4df2-998c-f5d6740bb1f5'),
(29, 21, 1, 1, 2, 1, '', '{"typeId":"1","authorId":"1","title":"Data protection","slug":"data-protection","postDate":1550478180,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<h2> </h2><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><h2>1 AKA</h2><p>1.1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><h2>2 USE OF THE SITE</h2><p>2.1 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.2 Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p><p>(a) Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>(b) Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>(c) Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>(d) Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.3 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.4 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.5 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>2.6 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>"}}', '2019-02-18 08:23:04', '2019-02-18 08:23:04', '53bfc055-38cd-4f39-9fc3-db7ab9f4ab23'),
(30, 22, 1, 1, 2, 1, '', '{"typeId":"1","authorId":"1","title":"Agbs English","slug":"agbs-english","postDate":1550478180,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc accumsan ornare euismod. Vestibulum tincidunt massa at augue faucibus finibus. Duis iaculis sapien quis orci commodo, id commodo tortor volutpat. Etiam luctus est sapien, vel tempor diam cursus et. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque ornare a ipsum at posuere. Maecenas sollicitudin leo id ipsum pellentesque tincidunt. Sed velit quam, gravida at ornare sed, gravida a nisl. Curabitur gravida fringilla erat a venenatis. Nam posuere sit amet lorem ut blandit. Praesent ligula lectus, imperdiet eu leo id, aliquet commodo nisl. Ut venenatis sem commodo auctor gravida. Nulla facilisi. Integer congue at dui nec venenatis. Suspendisse facilisis enim nec porttitor euismod.</p><p>Aliquam ornare, ante pretium feugiat elementum, dui quam suscipit ipsum, a sagittis est libero id diam. Nunc viverra, ante vitae maximus placerat, risus dolor luctus elit, ac aliquet purus erat vel urna. Praesent varius urna purus, sit amet semper turpis scelerisque ac. Morbi aliquet nibh mauris, vel lobortis quam iaculis ut. Aliquam rutrum odio ut urna consectetur fringilla. Suspendisse sit amet urna vitae augue sodales pharetra. Etiam vel felis elit. Etiam at finibus velit, condimentum fringilla nibh. Morbi dignissim sollicitudin diam, ut elementum nisi auctor vitae.</p><p>Etiam egestas dignissim ultricies. Pellentesque vel arcu sed lectus feugiat iaculis et ac tortor. In sit amet maximus arcu, et vestibulum neque. Quisque sollicitudin elit neque, lacinia convallis leo lacinia quis. Sed semper elit orci. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur at sagittis nibh, a egestas ipsum. Ut tellus diam, dapibus nec purus a, porttitor iaculis diam. Sed sagittis mauris eu lacinia ultricies. Pellentesque volutpat in odio pellentesque facilisis.</p><p>Aenean pretium massa ac nisl blandit, aliquet imperdiet mi mollis. Integer erat lacus, finibus et diam sed, vestibulum lobortis quam. Sed porttitor, magna in malesuada accumsan, turpis ipsum placerat lectus, nec interdum tortor leo ut dui. Nunc commodo lorem ut fermentum scelerisque. Aenean id leo malesuada, dapibus diam id, convallis libero. Fusce auctor tortor id risus ultrices tempus. Integer pretium gravida leo, nec iaculis velit commodo ut. Suspendisse posuere ipsum non magna volutpat, sit amet luctus massa scelerisque. Donec dictum tincidunt massa ut gravida.</p><p>Integer fermentum felis sapien, nec rhoncus mi consequat in. Mauris eu dictum risus. Nulla tincidunt a felis nec rhoncus. Aliquam placerat nisi augue, sit amet tincidunt tortor dictum eu. Integer hendrerit rutrum dui, eget pellentesque risus. Duis et molestie risus. Cras lacinia vel dui quis imperdiet. Integer interdum blandit ligula ac congue. Donec semper euismod rhoncus.</p>"}}', '2019-02-18 08:23:58', '2019-02-18 08:23:58', '7727d4b7-fbfe-48b5-94ac-5c019d003c27'),
(31, 23, 1, 1, 2, 1, '', '{"typeId":"1","authorId":"1","title":"Cookies English","slug":"cookies-english","postDate":1550478240,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc accumsan ornare euismod. Vestibulum tincidunt massa at augue faucibus finibus. Duis iaculis sapien quis orci commodo, id commodo tortor volutpat. Etiam luctus est sapien, vel tempor diam cursus et. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque ornare a ipsum at posuere. Maecenas sollicitudin leo id ipsum pellentesque tincidunt. Sed velit quam, gravida at ornare sed, gravida a nisl. Curabitur gravida fringilla erat a venenatis. Nam posuere sit amet lorem ut blandit. Praesent ligula lectus, imperdiet eu leo id, aliquet commodo nisl. Ut venenatis sem commodo auctor gravida. Nulla facilisi. Integer congue at dui nec venenatis. Suspendisse facilisis enim nec porttitor euismod.</p><p>Aliquam ornare, ante pretium feugiat elementum, dui quam suscipit ipsum, a sagittis est libero id diam. Nunc viverra, ante vitae maximus placerat, risus dolor luctus elit, ac aliquet purus erat vel urna. Praesent varius urna purus, sit amet semper turpis scelerisque ac. Morbi aliquet nibh mauris, vel lobortis quam iaculis ut. Aliquam rutrum odio ut urna consectetur fringilla. Suspendisse sit amet urna vitae augue sodales pharetra. Etiam vel felis elit. Etiam at finibus velit, condimentum fringilla nibh. Morbi dignissim sollicitudin diam, ut elementum nisi auctor vitae.</p><p>Etiam egestas dignissim ultricies. Pellentesque vel arcu sed lectus feugiat iaculis et ac tortor. In sit amet maximus arcu, et vestibulum neque. Quisque sollicitudin elit neque, lacinia convallis leo lacinia quis. Sed semper elit orci. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur at sagittis nibh, a egestas ipsum. Ut tellus diam, dapibus nec purus a, porttitor iaculis diam. Sed sagittis mauris eu lacinia ultricies. Pellentesque volutpat in odio pellentesque facilisis.</p><p>Aenean pretium massa ac nisl blandit, aliquet imperdiet mi mollis. Integer erat lacus, finibus et diam sed, vestibulum lobortis quam. Sed porttitor, magna in malesuada accumsan, turpis ipsum placerat lectus, nec interdum tortor leo ut dui. Nunc commodo lorem ut fermentum scelerisque. Aenean id leo malesuada, dapibus diam id, convallis libero. Fusce auctor tortor id risus ultrices tempus. Integer pretium gravida leo, nec iaculis velit commodo ut. Suspendisse posuere ipsum non magna volutpat, sit amet luctus massa scelerisque. Donec dictum tincidunt massa ut gravida.</p><p>Integer fermentum felis sapien, nec rhoncus mi consequat in. Mauris eu dictum risus. Nulla tincidunt a felis nec rhoncus. Aliquam placerat nisi augue, sit amet tincidunt tortor dictum eu. Integer hendrerit rutrum dui, eget pellentesque risus. Duis et molestie risus. Cras lacinia vel dui quis imperdiet. Integer interdum blandit ligula ac congue. Donec semper euismod rhoncus.</p>"}}', '2019-02-18 08:24:20', '2019-02-18 08:24:20', '66db44e4-7e7d-4010-a3db-8e0f8a7ab0eb');
INSERT INTO `entryversions` (`id`, `entryId`, `sectionId`, `creatorId`, `siteId`, `num`, `notes`, `data`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(32, 2, 2, 1, 1, 3, '', '{"typeId":"2","authorId":null,"title":"Homepage","slug":"homepage","postDate":1550067240,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<p>&lt;div class=\\"content-block d-none d-sm-block\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;h2 class=\\"content-heading\\"&gt;Vorstellungen&lt;/h2&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;p&gt;Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen &lt;/p&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=\\"schedule-block\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=\\"schedule-row\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=\\"schedule-col schedule-head\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Mittwoch &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=\\"schedule-col schedule-head\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Donnerstag &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=\\"schedule-col schedule-head\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Freitag &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=\\"schedule-col schedule-head\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Samstag &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=\\"schedule-col schedule-head\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Sonntag &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=\\"schedule-row\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=\\"schedule-col schedule-data\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;span&gt;Teil Eins&lt;/span&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;label&gt;14.30 Uhr&lt;/label&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;label class=\\"icon-amp iabc\\"&gt;&amp;&lt;/label&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=\\"schedule-col schedule-data schedule-blank\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;span&gt;&nbsp;&lt;/span&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;label&gt;&nbsp;&lt;/label&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=\\"schedule-col schedule-data schedule-blank\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;span&gt;&nbsp;&lt;/span&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;label&gt;&nbsp;&lt;/label&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=\\"schedule-col schedule-data\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;span&gt;Teil Eins&lt;/span&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;label&gt;14.30 Uhr&lt;/label&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;label class=\\"icon-amp iabc\\"&gt;&amp;&lt;/label&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=\\"schedule-col schedule-data\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;span&gt;Teil Eins&lt;/span&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;label&gt;14.30 Uhr&lt;/label&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;label class=\\"icon-amp iabc\\"&gt;&amp;&lt;/label&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=\\"schedule-row\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=\\"schedule-col schedule-data\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;span&gt;Teil Zwei&lt;/span&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;label&gt;19.30 Uhr&lt;/label&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=\\"schedule-col schedule-data\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;span&gt;Teil Zwei&lt;/span&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;label&gt;19.30 Uhr&lt;/label&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;label class=\\"icon-amp iarc\\"&gt;&amp;&lt;/label&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=\\"schedule-col schedule-data\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;span&gt;Teil Zwei&lt;/span&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;label&gt;19.30 Uhr&lt;/label&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=\\"schedule-col schedule-data\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;span&gt;Teil Zwei&lt;/span&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;label&gt;19.30 Uhr&lt;/label&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=\\"schedule-col schedule-data\\"&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;span&gt;Teil Zwei&lt;/span&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;label&gt;19.30 Uhr&lt;/label&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt;</p>"}}', '2019-02-20 10:42:17', '2019-02-20 10:42:17', '176bfdc1-a137-41a1-88b8-8f54afdccabd'),
(33, 2, 2, 1, 1, 4, '', '{"typeId":"2","authorId":null,"title":"Homepage","slug":"homepage","postDate":1550067240,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<p>&nbsp;</p>"}}', '2019-02-20 10:44:30', '2019-02-20 10:44:30', 'ef73ac08-b9da-48c7-8e71-7c8b056dfc72'),
(34, 2, 2, 1, 1, 5, '', '{"typeId":"2","authorId":null,"title":"Homepage","slug":"homepage","postDate":1550067240,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<h2>Vorstellungen</h2>\\r\\n<p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n<p> Mittwoch</p>\\r\\n<p> Donnerstag</p>\\r\\n<p> Freitag</p>\\r\\n<p> Samstag</p>\\r\\n<p> Sonntag</p>\\r\\n<p> Teil Eins</p>\\r\\n<p> 14.30 Uhr</p>\\r\\n<p> &amp;</p>\\r\\n<p> Teil Eins</p>\\r\\n<p> 14.30 Uhr</p>\\r\\n<p> &amp;</p>\\r\\n<p> Teil Eins</p>\\r\\n<p> 14.30 Uhr</p>\\r\\n<p> &amp;</p>\\r\\n<p> Teil Zwei</p>\\r\\n<p> 19.30 Uhr</p>\\r\\n<p> Teil Zwei</p>\\r\\n<p> 19.30 Uhr</p>\\r\\n<p> &amp;</p>\\r\\n<p> Teil Zwei</p>\\r\\n<p> 19.30 Uhr</p>\\r\\n<p> Teil Zwei</p>\\r\\n<p> 19.30 Uhr</p>\\r\\n<p> Teil Zwei</p>\\r\\n<p> 19.30 Uhr</p>"}}', '2019-02-20 11:18:11', '2019-02-20 11:18:11', '8e3b2b88-5e39-4b9b-b869-ba5457d294c6'),
(35, 2, 2, 1, 1, 6, '', '{"typeId":"2","authorId":null,"title":"Homepage","slug":"homepage","postDate":1550067240,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>"}}', '2019-02-20 11:19:22', '2019-02-20 11:19:22', 'ece9bb11-a4b1-4475-ad8d-a70f513e8332'),
(36, 2, 2, 1, 1, 7, '', '{"typeId":"2","authorId":null,"title":"Homepage","slug":"homepage","postDate":1550067240,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n<div class=\\"content-block nomr\\">\\r\\n <h2 class=\\"content-heading d-none d-sm-block\\">Tickets Kaufen</h2>\\r\\n <p class=\\"d-none d-sm-block\\">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\\r\\n direkt zur Ticketauswahl.\\r\\n </p>\\r\\n <div class=\\"row\\">\\r\\n <div class=\\"col-xs-12 col-lg-12 col-md-12 no-mo-pd\\">\\r\\n <div class=\\"banner-block color1-bg modal-block\\" data-toggle=\\"modal\\" data-target=\\"#myModal\\">\\r\\n <div class=\\"ribbon ribbon-top-left\\">Empfohlen</div>\\r\\n <h4 class=\\"h430\\"><figure><img src=\\"public/app/images/icon-1.png\\" data-image=\\"b7e0qhdvez4q\\"></figure>Kombi</h4>\\r\\n <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\\r\\n <a href=\\"javascript:void()\\" class=\\"modify-btn\\">Kombi Tickets Kaufen</a>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"row\\">\\r\\n <div class=\\"col-xs-12 col-lg-6 col-md-6 col-6 no-mo-pd\\">\\r\\n <div class=\\"banner-block color2-bg\\">\\r\\n <h4><figure><img src=\\"public/app/images/icon-2.png\\" data-image=\\"ojztde71g8xx\\"></figure>Individual</h4>\\r\\n <p>Teil Eins und Teil Zwei unabhängig voneinander schauen </p>\\r\\n <a href=\\"javascript:void()\\" class=\\"modify-btn\\">Individual Tickets Kaufen</a>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"col-xs-12 col-lg-6 col-md-6 col-6 no-mo-pd\\">\\r\\n <div class=\\"banner-block color3-bg\\">\\r\\n <h4 class=\\"h458\\"><figure><img src=\\"public/app/images/icon-3.png\\" data-image=\\"2lasj5kal6b9\\"></figure>Single</h4>\\r\\n <p class=\\"mo-mb-39 tab-mb-60\\">Nur Teil Eins oder<br>nur Teil Zwei schauen</p>\\r\\n <a href=\\"javascript:void()\\" class=\\"modify-btn\\">Single Tickets Kaufen</a>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>"}}', '2019-02-20 11:20:26', '2019-02-20 11:20:26', '0fdb0b4e-35cd-4599-b4c3-014dac637eef'),
(37, 2, 2, 1, 1, 8, '', '{"typeId":"2","authorId":null,"title":"Homepage","slug":"homepage","postDate":1550067240,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n<div class=\\"content-block nomr\\">\\r\\n            <h2 class=\\"content-heading  d-none d-sm-block\\">Tickets Kaufen</h2>\\r\\n            <p class=\\"d-none d-sm-block\\">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\\r\\n              direkt zur Ticketauswahl.\\r\\n            </p>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-12 col-md-12 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color1-bg modal-block\\" data-toggle=\\"modal\\" data-target=\\"#myModal\\">\\r\\n                  <div class=\\"ribbon ribbon-top-left\\">Empfohlen</div>\\r\\n                  <h4 class=\\"h430\\"><figure><img src=\\"public/app/images/icon-1.png\\" data-image=\\"iqd7x1sxiz3z\\"></figure>Kombi</h4>\\r\\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\\r\\n                  <a href=\\"javascript:void()\\" class=\\"modify-btn\\">Kombi Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-6 col-md-6 col-6 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color2-bg\\">\\r\\n                  <h4><figure><img src=\\"public/app/images/icon-2.png\\" data-image=\\"jg0fwanmf9r4\\"></figure>Individual</h4>\\r\\n                  <p>Teil Eins und Teil Zwei unabhängig voneinander schauen </p>\\r\\n                  <a href=\\"javascript:void()\\" class=\\"modify-btn\\">Individual Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n              <div class=\\"col-xs-12 col-lg-6 col-md-6 col-6 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color3-bg\\">\\r\\n                  <h4 class=\\"h458\\"><figure><img src=\\"public/app/images/icon-3.png\\" data-image=\\"fxa0pz83a16q\\"></figure>Single</h4>\\r\\n                  <p class=\\"mo-mb-39 tab-mb-60\\">Nur Teil Eins oder<br>nur Teil Zwei schauen</p>\\r\\n                  <a href=\\"javascript:void()\\" class=\\"modify-btn\\">Single Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n          </div>"}}', '2019-02-20 11:22:59', '2019-02-20 11:22:59', 'ae091486-9143-4553-9cf3-aa6096007e77'),
(38, 2, 2, 1, 1, 9, '', '{"typeId":"2","authorId":null,"title":"Homepage","slug":"homepage","postDate":1550067240,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n<div class=\\"content-block nomr\\">\\r\\n            <h2 class=\\"content-heading  d-none d-sm-block\\">Tickets Kaufen</h2>\\r\\n            <p class=\\"d-none d-sm-block\\">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\\r\\n              direkt zur Ticketauswahl.\\r\\n            </p>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-12 col-md-12 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color1-bg modal-block\\" data-toggle=\\"modal\\" data-target=\\"#myModal\\">\\r\\n                  <div class=\\"ribbon ribbon-top-left\\"><span>Empfohlen</span></div>\\r\\n                  <h4 class=\\"h430\\"><figure><img src=\\"public/app/images/icon-1.png\\" data-image=\\"iqd7x1sxiz3z\\"></figure>Kombi</h4>\\r\\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\\r\\n                  <a href=\\"javascript:void()\\" class=\\"modify-btn\\">Kombi Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-6 col-md-6 col-6 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color2-bg\\">\\r\\n                  <h4><figure><img src=\\"public/app/images/icon-2.png\\" data-image=\\"jg0fwanmf9r4\\"></figure>Individual</h4>\\r\\n                  <p>Teil Eins und Teil Zwei unabhängig voneinander schauen </p>\\r\\n                  <a href=\\"javascript:void()\\" class=\\"modify-btn\\">Individual Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n              <div class=\\"col-xs-12 col-lg-6 col-md-6 col-6 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color3-bg\\">\\r\\n                  <h4 class=\\"h458\\"><figure><img src=\\"public/app/images/icon-3.png\\" data-image=\\"fxa0pz83a16q\\"></figure>Single</h4>\\r\\n                  <p class=\\"mo-mb-39 tab-mb-60\\">Nur Teil Eins oder<br>nur Teil Zwei schauen</p>\\r\\n                  <a href=\\"javascript:void()\\" class=\\"modify-btn\\">Single Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n          </div>"}}', '2019-02-20 11:24:51', '2019-02-20 11:24:51', '460ef39b-6989-4b8c-8c60-a041076336be'),
(39, 13, 3, 1, 1, 2, '', '{"typeId":"3","authorId":"1","title":"Saalplan","slug":"saalplan","postDate":1550145060,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>Mehr!Theater am Grobmarkt, Hamburg</p>\\r\\n<img src=\\"public/app/images/img1.png\\" class=\\"iframe-img\\">"}}', '2019-02-20 11:30:42', '2019-02-20 11:30:42', 'a94a2aa2-ad4a-497b-b5cb-93c5b6bc6f45'),
(40, 16, 3, 1, 2, 2, '', '{"typeId":"3","authorId":"1","title":"Seating plan","slug":"seating-plan","postDate":1550477880,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>More! Theater am Grobmarkt, Hambur</p>\\r\\n\\r\\n<figure><img src=\\"public/app/images/img1.png\\" class=\\"iframe-img\\" data-image=\\"l6webfxrwj2l\\"></figure>"}}', '2019-02-20 12:09:27', '2019-02-20 12:09:27', '4d87bc21-37cf-413b-bc63-aeebe68fc476'),
(41, 14, 4, 1, 2, 3, '', '{"typeId":"4","authorId":null,"title":"HomePage English","slug":"home-page-english","postDate":1550471820,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n<div class=\\"content-block nomr\\">\\r\\n            <h2 class=\\"content-heading  d-none d-sm-block\\">Tickets Kaufen</h2>\\r\\n            <p class=\\"d-none d-sm-block\\">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\\r\\n              direkt zur Ticketauswahl.\\r\\n            </p>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-12 col-md-12 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color1-bg modal-block\\" data-toggle=\\"modal\\" data-target=\\"#myModal\\">\\r\\n                  <div class=\\"ribbon ribbon-top-left\\">Empfohlen</div>\\r\\n                  <h4 class=\\"h430\\"><figure><img src=\\"public/app/images/icon-1.png\\" data-image=\\"iqd7x1sxiz3z\\"></figure>Kombi</h4>\\r\\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\\r\\n                  <a href=\\"javascript:void()\\" class=\\"modify-btn\\">Kombi Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-6 col-md-6 col-6 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color2-bg\\">\\r\\n                  <h4><figure><img src=\\"public/app/images/icon-2.png\\" data-image=\\"jg0fwanmf9r4\\"></figure>Individual</h4>\\r\\n                  <p>Teil Eins und Teil Zwei unabhängig voneinander schauen </p>\\r\\n                  <a href=\\"javascript:void()\\" class=\\"modify-btn\\">Individual Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n              <div class=\\"col-xs-12 col-lg-6 col-md-6 col-6 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color3-bg\\">\\r\\n                  <h4 class=\\"h458\\"><figure><img src=\\"public/app/images/icon-3.png\\" data-image=\\"fxa0pz83a16q\\"></figure>Single</h4>\\r\\n                  <p class=\\"mo-mb-39 tab-mb-60\\">Nur Teil Eins oder<br>nur Teil Zwei schauen</p>\\r\\n                  <a href=\\"javascript:void()\\" class=\\"modify-btn\\">Single Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n          </div>"}}', '2019-02-20 12:11:27', '2019-02-20 12:11:27', '667650c0-8af1-4486-b030-aa39ee0251eb'),
(42, 14, 4, 1, 2, 4, '', '{"typeId":"4","authorId":null,"title":"HomePage English","slug":"home-page-english","postDate":1550471820,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n<div class=\\"content-block nomr\\">\\r\\n            <h2 class=\\"content-heading  d-none d-sm-block\\">Tickets Kaufen</h2>\\r\\n            <p class=\\"d-none d-sm-block\\">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\\r\\n              direkt zur Ticketauswahl.\\r\\n            </p>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-12 col-md-12 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color1-bg modal-block\\" data-toggle=\\"modal\\" data-target=\\"#myModal\\">\\r\\n                  <div class=\\"ribbon ribbon-top-left\\">Empfohlen</div>\\r\\n                  <h4 class=\\"h430\\"><figure><img src=\\"/public/app/images/icon-1.png\\" data-image=\\"iqd7x1sxiz3z\\"></figure>Kombi</h4>\\r\\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\\r\\n                  <a href=\\"javascript:void()\\" class=\\"modify-btn\\">Kombi Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-6 col-md-6 col-6 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color2-bg\\">\\r\\n                  <h4><figure><img src=\\"/public/app/images/icon-2.png\\" data-image=\\"jg0fwanmf9r4\\"><span id=\\"selection-marker-start\\" class=\\"redactor-selection-marker\\">﻿</span><span id=\\"selection-marker-end\\" class=\\"redactor-selection-marker\\">﻿</span></figure>Individual</h4>\\r\\n                  <p>Teil Eins und Teil Zwei unabhängig voneinander schauen </p>\\r\\n                  <a href=\\"javascript:void()\\" class=\\"modify-btn\\">Individual Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n              <div class=\\"col-xs-12 col-lg-6 col-md-6 col-6 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color3-bg\\">\\r\\n                  <h4 class=\\"h458\\"><figure><img src=\\"/public/app/images/icon-3.png\\" data-image=\\"fxa0pz83a16q\\"></figure>Single</h4>\\r\\n                  <p class=\\"mo-mb-39 tab-mb-60\\">Nur Teil Eins oder<br>nur Teil Zwei schauen</p>\\r\\n                  <a href=\\"javascript:void()\\" class=\\"modify-btn\\">Single Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n          </div>"}}', '2019-02-20 12:14:31', '2019-02-20 12:14:31', '33c18116-0166-4014-99e5-dcac46558e63'),
(43, 14, 4, 1, 2, 5, '', '{"typeId":"4","authorId":null,"title":"HomePage English","slug":"home-page-english","postDate":1550471820,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n<div class=\\"content-block nomr\\">\\r\\n            <h2 class=\\"content-heading  d-none d-sm-block\\">Tickets Kaufen</h2>\\r\\n            <p class=\\"d-none d-sm-block\\">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\\r\\n              direkt zur Ticketauswahl.\\r\\n            </p>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-12 col-md-12 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color1-bg modal-block\\" data-toggle=\\"modal\\" data-target=\\"#myModal\\">\\r\\n                  <div class=\\"ribbon ribbon-top-left\\"><span>Empfohlen</span></div>\\r\\n                  <h4 class=\\"h430\\"><figure><img src=\\"/public/app/images/icon-1.png\\" data-image=\\"iqd7x1sxiz3z\\"></figure>Kombi</h4>\\r\\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\\r\\n                  <a href=\\"javascript:void()\\" class=\\"modify-btn\\">Kombi Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-6 col-md-6 col-6 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color2-bg\\">\\r\\n                  <h4><figure><img src=\\"/public/app/images/icon-2.png\\" data-image=\\"jg0fwanmf9r4\\"><span id=\\"selection-marker-start\\" class=\\"redactor-selection-marker\\">﻿</span><span id=\\"selection-marker-end\\" class=\\"redactor-selection-marker\\">﻿</span></figure>Individual</h4>\\r\\n                  <p>Teil Eins und Teil Zwei unabhängig voneinander schauen </p>\\r\\n                  <a href=\\"javascript:void()\\" class=\\"modify-btn\\">Individual Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n              <div class=\\"col-xs-12 col-lg-6 col-md-6 col-6 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color3-bg\\">\\r\\n                  <h4 class=\\"h458\\"><figure><img src=\\"/public/app/images/icon-3.png\\" data-image=\\"fxa0pz83a16q\\"></figure>Single</h4>\\r\\n                  <p class=\\"mo-mb-39 tab-mb-60\\">Nur Teil Eins oder<br>nur Teil Zwei schauen</p>\\r\\n                  <a href=\\"javascript:void()\\" class=\\"modify-btn\\">Single Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n          </div>"}}', '2019-02-20 12:16:29', '2019-02-20 12:16:29', 'cde45960-47e1-408c-af61-ac19b0d6159a'),
(44, 16, 3, 1, 2, 3, '', '{"typeId":"3","authorId":"1","title":"Seating plan","slug":"seating-plan","postDate":1550477880,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<p>More! Theater am Grobmarkt, Hambur</p>\\r\\n<figure><img src=\\"/public/app/images/img1.png\\" class=\\"iframe-img\\" data-image=\\"l6webfxrwj2l\\"></figure>"}}', '2019-02-20 12:16:58', '2019-02-20 12:16:58', 'f208e324-c901-4eea-8ce3-a3bda04565c0'),
(45, 24, 3, 1, 2, 1, '', '{"typeId":"3","authorId":"1","title":"Footer Logo Block","slug":"footer-logo-block","postDate":1550665320,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<figure><img src=\\"/public/app/images/footer-logo-hp.png\\" class=\\"logo-hp\\" data-image=\\"76st2dbitket\\"></figure>\\r\\n<figure><img src=\\"/public/app/images/footer-logo-wizard-world.png\\" class=\\"logo-wizard\\" data-image=\\"jsx9zbr1jx3k\\"></figure>\\r\\n<figure><img src=\\"/public/app/images/footer-logo-Mehr.png\\" class=\\"logo-mehr\\" data-image=\\"33ome26msee7\\"></figure>"}}', '2019-02-20 12:22:05', '2019-02-20 12:22:05', 'ba995c27-3cac-4ae3-a20d-165822667009'),
(46, 24, 3, 1, 2, 2, '', '{"typeId":"3","authorId":"1","title":"Footer Logo Block English","slug":"footer-logo-block-english","postDate":1550665320,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<figure><img src=\\"/public/app/images/footer-logo-hp.png\\" class=\\"logo-hp\\" data-image=\\"76st2dbitket\\"></figure>\\r\\n<figure><img src=\\"/public/app/images/footer-logo-wizard-world.png\\" class=\\"logo-wizard\\" data-image=\\"jsx9zbr1jx3k\\"></figure>\\r\\n<figure><img src=\\"/public/app/images/footer-logo-Mehr.png\\" class=\\"logo-mehr\\" data-image=\\"33ome26msee7\\"></figure>"}}', '2019-02-20 12:22:20', '2019-02-20 12:22:20', '32543b8f-0e55-4970-84ea-edbe67ba1013'),
(47, 25, 3, 1, 1, 1, '', '{"typeId":"3","authorId":"1","title":"Footer Logo Block","slug":"footer-logo-block","postDate":1550665560,"expiryDate":null,"enabled":true,"newParentId":"","fields":{"1":"<figure><img src=\\"/public/app/images/footer-logo-hp.png\\" class=\\"logo-hp\\" data-image=\\"76st2dbitket\\"></figure>\\r\\n<figure><img src=\\"/public/app/images/footer-logo-wizard-world.png\\" class=\\"logo-wizard\\" data-image=\\"jsx9zbr1jx3k\\"></figure>\\r\\n<figure><img src=\\"/public/app/images/footer-logo-Mehr.png\\" class=\\"logo-mehr\\" data-image=\\"33ome26msee7\\"></figure>"}}', '2019-02-20 12:26:24', '2019-02-20 12:26:24', '9ab166d5-3a13-452d-806d-3643403ca21f'),
(48, 2, 2, 1, 1, 10, '', '{"typeId":"2","authorId":null,"title":"Homepage","slug":"homepage","postDate":1550067240,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n<div class=\\"content-block nomr\\">\\r\\n            <h2 class=\\"content-heading  d-none d-sm-block\\">Tickets Kaufen</h2>\\r\\n            <p class=\\"d-none d-sm-block\\">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\\r\\n              direkt zur Ticketauswahl.\\r\\n            </p>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-12 col-md-12 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color1-bg modal-block\\">\\r\\n                  <div class=\\"ribbon ribbon-top-left\\">Empfohlen</div>\\r\\n                  <h4 class=\\"h430\\"><figure><img src=\\"public/app/images/icon-1.png\\" data-image=\\"0yn5tbbn9n7l\\"></figure>Kombi</h4>\\r\\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\\r\\n                  <a href=\\"javascript:void(0);\\" class=\\"modify-btn ajax-data\\" data-toggle=\\"modal\\" data-target=\\"#myModal\\" data-id=\\"HPKOMBI\\" data-local-json=\\"public/sample_json/HPKOMBI.json\\">Kombi\\r\\n                    Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n          </div>"}}', '2019-02-25 14:23:41', '2019-02-25 14:23:41', 'bb80b8be-080f-4d74-9552-6cfeaeb22c01');
INSERT INTO `entryversions` (`id`, `entryId`, `sectionId`, `creatorId`, `siteId`, `num`, `notes`, `data`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(49, 2, 2, 1, 1, 11, '', '{"typeId":"2","authorId":null,"title":"Homepage","slug":"homepage","postDate":1550067240,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n<div class=\\"content-block nomr\\">\\r\\n            <h2 class=\\"content-heading  d-none d-sm-block\\">Tickets Kaufen</h2>\\r\\n            <p class=\\"d-none d-sm-block\\">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\\r\\n              direkt zur Ticketauswahl.\\r\\n            </p>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-12 col-md-12 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color1-bg modal-block\\">\\r\\n                  <div class=\\"ribbon ribbon-top-left\\"><span>Empfohlen</span></div>\\r\\n                  <h4 class=\\"h430\\"><figure><img src=\\"public/app/images/icon-1.png\\" data-image=\\"0yn5tbbn9n7l\\"></figure>Kombi</h4>\\r\\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\\r\\n                  <a href=\\"javascript:void(0);\\" class=\\"modify-btn ajax-data\\" data-toggle=\\"modal\\" data-target=\\"#myModal\\" data-id=\\"HPKOMBI\\" data-local-json=\\"public/sample_json/HPKOMBI.json\\">Kombi\\r\\n                    Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n          </div>"}}', '2019-02-25 14:24:51', '2019-02-25 14:24:51', '9a64d925-55d7-48c5-b45a-df3f09699f59'),
(50, 27, 6, 1, 1, 1, 'Revision from Feb 26, 2019, 2:59:46 AM', '{"typeId":"6","authorId":null,"title":"Presale","slug":"presale","postDate":1551163260,"expiryDate":null,"enabled":"1","newParentId":null,"fields":{"2":[],"3":[]}}', '2019-02-26 11:00:20', '2019-02-26 11:00:20', 'e9d4ac00-398a-486e-9df5-4b4d322552a1'),
(51, 27, 6, 1, 1, 2, '', '{"typeId":"6","authorId":null,"title":"Presale","slug":"presale","postDate":1551163260,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"]}}', '2019-02-26 11:00:20', '2019-02-26 11:00:20', '2a04a475-3967-496d-ad31-f08916aa99d7'),
(52, 27, 6, 1, 1, 3, '', '{"typeId":"6","authorId":null,"title":"Presale","slug":"presale","postDate":1551163260,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"6":"https://www.google.com","5":"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the                industry\'s standard dummy text ever since the 1500s, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</p>\\r\\n\\r\\n\\r\\n","4":"Noch 10 Tage bis zum Tickets Presale"}}', '2019-02-26 12:04:35', '2019-02-26 12:04:35', 'e4a88391-3d7d-4beb-98a1-9dc87d16228a'),
(53, 27, 6, 1, 1, 4, '', '{"typeId":"6","authorId":null,"title":"Presale","slug":"presale","postDate":1551163260,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<p>asdasdasdasd</p>","6":"https://www.google.com","5":"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the                industry\'s standard dummy text ever since the 1500s, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</p>","4":"Noch 10 Tage bis zum Tickets Presale"}}', '2019-02-26 12:22:20', '2019-02-26 12:22:20', '894f77ae-6c23-4ee3-a652-f7af3022dbe7'),
(54, 27, 6, 1, 1, 5, '', '{"typeId":"6","authorId":null,"title":"Presale","slug":"presale","postDate":1551163260,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<p>asdasdasdasd</p>","7":"2019-03-04 09:00:00","6":"https://www.google.com","5":"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the                industry\'s standard dummy text ever since the 1500s, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</p>","4":"Noch 10 Tage bis zum Tickets Presale"}}', '2019-02-26 12:28:34', '2019-02-26 12:28:34', '15b27c43-58f5-4f08-85c2-393f60a42079'),
(55, 27, 6, 1, 1, 6, '', '{"typeId":"6","authorId":null,"title":"Presale","slug":"presale","postDate":1551163260,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<p>asdasdasdasd</p>","8":"<p>Wenn Du Dich bei <a href=\\"http://www.XYZ.de\\">www.XYZ.de</a> registriert hast, hast Du einen Code per Email bekommen. Den kannst Du hier eintragen und exklusiv vor offiziellem Verkaufsstart Tickets Kir Harry Potter und das verwunschene Kind kaufen.</p>","6":"https://www.google.com","5":"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the                industry\'s standard dummy text ever since the 1500s, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</p>","4":"Noch 10 Tage bis zum Tickets Presale"}}', '2019-02-26 12:51:49', '2019-02-26 12:51:49', 'd6c7f065-576e-4288-b785-5730ba763d80'),
(56, 27, 6, 1, 1, 7, '', '{"typeId":"6","authorId":null,"title":"Presale","slug":"presale","postDate":1551163260,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<p>asdasdasdasd</p>","8":"<p>Wenn Du Dich bei <a href=\\"http://www.XYZ.de\\">www.XYZ.de</a> registriert hast, hast Du einen Code per Email bekommen. Den kannst Du hier eintragen und exklusiv vor offiziellem Verkaufsstart Tickets Kir Harry Potter und das verwunschene Kind kaufen.</p>","6":"https://www.google.com","5":"<p> Atte Fans, die sich bis zum 15. Marz unter <a href=\\"http://www.XYZ.de\\">www.XYZ.de</a> fur den Newsletter registrieren, haben vier Tage vor dem offiziellen Vorverkaufsstart am 25. Marz die Meglichkeit, hier Tickets zu bestehen. </p>","4":"Noch 10 Tage bis zum Tickets Presale"}}', '2019-02-26 12:55:25', '2019-02-26 12:55:25', 'b7676d8a-cf95-466a-a231-7f6c31895a14'),
(57, 27, 6, 1, 1, 8, '', '{"typeId":"6","authorId":null,"title":"Presale","slug":"presale","postDate":1551163260,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"          <div class=\\"content-block d-none d-sm-block\\">\\r\\n            <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n            <p>Harry Potter und das verwunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n              Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n            <div class=\\"schedule-block\\">\\r\\n              <div class=\\"schedule-row\\">\\r\\n                <div class=\\"schedule-col schedule-head\\">\\r\\n                  Mittwoch\\r\\n                </div>\\r\\n                <div class=\\"schedule-col schedule-head\\">\\r\\n                  Donnerstag\\r\\n                </div>\\r\\n                <div class=\\"schedule-col schedule-head\\">\\r\\n                  Freitag\\r\\n                </div>\\r\\n                <div class=\\"schedule-col schedule-head\\">\\r\\n                  Samstag\\r\\n                </div>\\r\\n                <div class=\\"schedule-col schedule-head\\">\\r\\n                  Sonntag\\r\\n                </div>\\r\\n              </div>\\r\\n              <div class=\\"schedule-row\\">\\r\\n                <div class=\\"schedule-col schedule-data\\">\\r\\n                  <span>Teil Eins</span>\\r\\n                  <label>14.30 Uhr</label>\\r\\n                  <label class=\\"icon-amp iabc\\">&</label>\\r\\n                </div>\\r\\n                <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n                  <span>&nbsp;</span>\\r\\n                  <label>&nbsp;</label>\\r\\n                </div>\\r\\n                <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n                  <span>&nbsp;</span>\\r\\n                  <label>&nbsp;</label>\\r\\n                </div>\\r\\n                <div class=\\"schedule-col schedule-data\\">\\r\\n                  <span>Teil Eins</span>\\r\\n                  <label>14.30 Uhr</label>\\r\\n                  <label class=\\"icon-amp iabc\\">&</label>\\r\\n                </div>\\r\\n                <div class=\\"schedule-col schedule-data\\">\\r\\n                  <span>Teil Eins</span>\\r\\n                  <label>14.30 Uhr</label>\\r\\n                  <label class=\\"icon-amp iabc\\">&</label>\\r\\n                </div>\\r\\n              </div>\\r\\n              <div class=\\"schedule-row\\">\\r\\n                <div class=\\"schedule-col schedule-data\\">\\r\\n                  <span>Teil Zwei</span>\\r\\n                  <label>19.30 Uhr</label>\\r\\n                </div>\\r\\n                <div class=\\"schedule-col schedule-data\\">\\r\\n                  <span>TEIL EINS</span>\\r\\n                  <label>19.30 Uhr</label>\\r\\n                  <label class=\\"icon-amp iarc\\">&</label>\\r\\n                </div>\\r\\n                <div class=\\"schedule-col schedule-data\\">\\r\\n                  <span>Teil Zwei</span>\\r\\n                  <label>19.30 Uhr</label>\\r\\n                </div>\\r\\n                <div class=\\"schedule-col schedule-data\\">\\r\\n                  <span>Teil Zwei</span>\\r\\n                  <label>19.30 Uhr</label>\\r\\n                </div>\\r\\n                <div class=\\"schedule-col schedule-data\\">\\r\\n                  <span>Teil Zwei</span>\\r\\n                  <label>19.30 Uhr</label>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n          </div>","8":"<p>Wenn Du Dich bei <a href=\\"http://www.XYZ.de\\">www.XYZ.de</a> registriert hast, hast Du einen Code per Email bekommen. Den kannst Du hier eintragen und exklusiv vor offiziellem Verkaufsstart Tickets Kir Harry Potter und das verwunschene Kind kaufen.</p>","6":"https://www.google.com","5":"<p> Atte Fans, die sich bis zum 15. Marz unter <a href=\\"http://www.XYZ.de\\">www.XYZ.de</a> fur den Newsletter registrieren, haben vier Tage vor dem offiziellen Vorverkaufsstart am 25. Marz die Meglichkeit, hier Tickets zu bestehen. </p>","4":"Noch 10 Tage bis zum Tickets Presale"}}', '2019-02-26 12:59:50', '2019-02-26 12:59:50', '7857561f-4017-4115-bd1a-8bb57906be1d'),
(58, 27, 6, 1, 1, 9, '', '{"typeId":"6","authorId":null,"title":"Presale","slug":"presale","postDate":1551163260,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n<div class=\\"content-block nomr\\">\\r\\n            <h2 class=\\"content-heading  d-none d-sm-block\\">Tickets Kaufen</h2>\\r\\n            <p class=\\"d-none d-sm-block\\">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\\r\\n              direkt zur Ticketauswahl.\\r\\n            </p>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-12 col-md-12 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color1-bg modal-block\\">\\r\\n                  <div class=\\"ribbon ribbon-top-left\\">Empfohlen</div>\\r\\n                  <h4 class=\\"h430\\"><figure><img src=\\"public/app/images/icon-1.png\\" data-image=\\"0yn5tbbn9n7l\\"></figure>Kombi</h4>\\r\\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\\r\\n                  <a href=\\"javascript:void(0);\\" class=\\"modify-btn ajax-data\\" data-toggle=\\"modal\\" data-target=\\"#myModal\\" data-id=\\"HPKOMBI\\" data-local-json=\\"public/sample_json/HPKOMBI.json\\">Kombi\\r\\n                    Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n          </div>","8":"<p>Wenn Du Dich bei <a href=\\"http://www.XYZ.de\\">www.XYZ.de</a> registriert hast, hast Du einen Code per Email bekommen. Den kannst Du hier eintragen und exklusiv vor offiziellem Verkaufsstart Tickets Kir Harry Potter und das verwunschene Kind kaufen.</p>","6":"https://www.google.com","5":"<p> Atte Fans, die sich bis zum 15. Marz unter <a href=\\"http://www.XYZ.de\\">www.XYZ.de</a> fur den Newsletter registrieren, haben vier Tage vor dem offiziellen Vorverkaufsstart am 25. Marz die Meglichkeit, hier Tickets zu bestehen. </p>","4":"Noch 10 Tage bis zum Tickets Presale"}}', '2019-02-26 13:17:52', '2019-02-26 13:17:52', '6dbbe4fe-08ac-4f20-a9ff-00814714ee9d'),
(59, 2, 2, 1, 1, 12, '', '{"typeId":"2","authorId":null,"title":"Homepage","slug":"homepage","postDate":1550067240,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n<div class=\\"content-block nomr close\\">\\r\\n            <h2 class=\\"content-heading  d-none d-sm-block\\">Tickets Kaufen</h2>\\r\\n            <p class=\\"d-none d-sm-block\\">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\\r\\n              direkt zur Ticketauswahl.\\r\\n            </p>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-12 col-md-12 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color1-bg modal-block\\">\\r\\n                  <div class=\\"ribbon ribbon-top-left\\">Empfohlen</div>\\r\\n                  <h4 class=\\"h430\\"><figure><img src=\\"public/app/images/icon-1.png\\" data-image=\\"0yn5tbbn9n7l\\"></figure>Kombi</h4>\\r\\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\\r\\n                  <a href=\\"javascript:void(0);\\" class=\\"modify-btn ajax-data\\" data-toggle=\\"modal\\" data-target=\\"#myModal\\" data-id=\\"HPKOMBI\\" data-local-json=\\"public/sample_json/HPKOMBI.json\\">Kombi\\r\\n                    Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n          </div>"}}', '2019-02-26 13:18:41', '2019-02-26 13:18:41', '6ffae437-a230-49b3-8421-f25bb92f91d2'),
(60, 2, 2, 1, 1, 13, '', '{"typeId":"2","authorId":null,"title":"Homepage","slug":"homepage","postDate":1550067240,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n<div class=\\"content-block nomr\\">\\r\\n            <h2 class=\\"content-heading  d-none d-sm-block\\">Tickets Kaufen</h2>\\r\\n            <p class=\\"d-none d-sm-block\\">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\\r\\n              direkt zur Ticketauswahl.\\r\\n            </p>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-12 col-md-12 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color1-bg modal-block\\">\\r\\n                  <div class=\\"ribbon ribbon-top-left\\">Empfohlen</div>\\r\\n                  <h4 class=\\"h430\\"><figure><img src=\\"public/app/images/icon-1.png\\" data-image=\\"0yn5tbbn9n7l\\"></figure>Kombi</h4>\\r\\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\\r\\n                  <a href=\\"javascript:void(0);\\" class=\\"modify-btn ajax-data\\" data-toggle=\\"modal\\" data-target=\\"#myModal\\" data-id=\\"HPKOMBI\\" data-local-json=\\"public/sample_json/HPKOMBI.json\\">Kombi\\r\\n                    Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n          </div>"}}', '2019-02-26 13:18:59', '2019-02-26 13:18:59', '9e72d027-61a2-4825-a2ae-6bee153bcf60'),
(61, 27, 6, 1, 1, 10, '', '{"typeId":"6","authorId":null,"title":"Presale","slug":"presale","postDate":1551163260,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n<div class=\\"content-block nomr close\\">\\r\\n            <h2 class=\\"content-heading  d-none d-sm-block\\">Tickets Kaufen</h2>\\r\\n            <p class=\\"d-none d-sm-block\\">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\\r\\n              direkt zur Ticketauswahl.\\r\\n            </p>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-12 col-md-12 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color1-bg modal-block\\">\\r\\n                  <div class=\\"ribbon ribbon-top-left\\">Empfohlen</div>\\r\\n                  <h4 class=\\"h430\\"><figure><img src=\\"public/app/images/icon-1.png\\" data-image=\\"0yn5tbbn9n7l\\"></figure>Kombi</h4>\\r\\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\\r\\n                  <a href=\\"javascript:void(0);\\" class=\\"modify-btn ajax-data\\" data-toggle=\\"modal\\" data-target=\\"#myModal\\" data-id=\\"HPKOMBI\\" data-local-json=\\"public/sample_json/HPKOMBI.json\\">Kombi\\r\\n                    Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n          </div>","8":"<p>Wenn Du Dich bei <a href=\\"http://www.XYZ.de\\">www.XYZ.de</a> registriert hast, hast Du einen Code per Email bekommen. Den kannst Du hier eintragen und exklusiv vor offiziellem Verkaufsstart Tickets Kir Harry Potter und das verwunschene Kind kaufen.</p>","6":"https://www.google.com","5":"<p> Atte Fans, die sich bis zum 15. Marz unter <a href=\\"http://www.XYZ.de\\">www.XYZ.de</a> fur den Newsletter registrieren, haben vier Tage vor dem offiziellen Vorverkaufsstart am 25. Marz die Meglichkeit, hier Tickets zu bestehen. </p>","4":"Noch 10 Tage bis zum Tickets Presale"}}', '2019-02-26 13:19:14', '2019-02-26 13:19:14', '52e0d5f1-7f28-4dec-9226-8783c93e474b'),
(62, 27, 6, 1, 1, 11, '', '{"typeId":"6","authorId":null,"title":"Presale","slug":"presale","postDate":1551163260,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n<div class=\\"content-block nomr close\\">\\r\\n            <h2 class=\\"content-heading  d-none d-sm-block\\">Tickets Kaufen</h2>\\r\\n            <p class=\\"d-none d-sm-block\\">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\\r\\n              direkt zur Ticketauswahl.\\r\\n            </p>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-12 col-md-12 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color1-bg modal-block\\">\\r\\n                  <div class=\\"ribbon ribbon-top-left\\">Empfohlen</div>\\r\\n                  <h4 class=\\"h430\\"><figure><img src=\\"/public/app/images/icon-1.png\\" data-image=\\"0yn5tbbn9n7l\\"></figure>Kombi</h4>\\r\\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\\r\\n                  <a href=\\"javascript:void(0);\\" class=\\"modify-btn ajax-data\\" data-toggle=\\"modal\\" data-target=\\"#myModal\\" data-id=\\"HPKOMBI\\" data-local-json=\\"/public/sample_json/HPKOMBI.json\\">Kombi\\r\\n                    Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n          </div>","8":"<p>Wenn Du Dich bei <a href=\\"http://www.XYZ.de\\">www.XYZ.de</a> registriert hast, hast Du einen Code per Email bekommen. Den kannst Du hier eintragen und exklusiv vor offiziellem Verkaufsstart Tickets Kir Harry Potter und das verwunschene Kind kaufen.</p>","6":"https://www.google.com","5":"<p> Atte Fans, die sich bis zum 15. Marz unter <a href=\\"http://www.XYZ.de\\">www.XYZ.de</a> fur den Newsletter registrieren, haben vier Tage vor dem offiziellen Vorverkaufsstart am 25. Marz die Meglichkeit, hier Tickets zu bestehen. </p>","4":"Noch 10 Tage bis zum Tickets Presale"}}', '2019-02-26 13:27:26', '2019-02-26 13:27:26', '4f6eed56-95fd-46a1-b6f8-e76e7bb3204a'),
(63, 2, 2, 1, 1, 14, '', '{"typeId":"2","authorId":null,"title":"Homepage","slug":"homepage","postDate":1550067240,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n<div class=\\"content-block nomr\\">\\r\\n            <h2 class=\\"content-heading  d-none d-sm-block\\">Tickets Kaufen</h2>\\r\\n            <p class=\\"d-none d-sm-block\\">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\\r\\n              direkt zur Ticketauswahl.\\r\\n            </p>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-12 col-md-12 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color1-bg modal-block\\">\\r\\n                  <div class=\\"ribbon ribbon-top-left\\"><span>Empfohlen</span></div>\\r\\n                  <h4 class=\\"h430\\"><figure><img src=\\"public/app/images/icon-1.png\\" data-image=\\"0yn5tbbn9n7l\\"></figure>Kombi</h4>\\r\\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\\r\\n                  <a href=\\"javascript:void(0);\\" class=\\"modify-btn ajax-data\\" data-toggle=\\"modal\\" data-target=\\"#myModal\\" data-id=\\"HPKOMBI\\" data-local-json=\\"public/sample_json/HPKOMBI.json\\">Kombi\\r\\n                    Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n          </div>"}}', '2019-02-26 14:39:22', '2019-02-26 14:39:22', '6a7a0d55-a290-4677-806c-193f187bb3ca'),
(64, 2, 2, 1, 1, 15, '', '{"typeId":"2","authorId":null,"title":"Homepage","slug":"homepage","postDate":1550067240,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["3"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n<div class=\\"content-block nomr\\">\\r\\n            <h2 class=\\"content-heading  d-none d-sm-block\\">Tickets Kaufen</h2>\\r\\n            <p class=\\"d-none d-sm-block\\">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\\r\\n              direkt zur Ticketauswahl.\\r\\n            </p>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-12 col-md-12 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color1-bg modal-block\\">\\r\\n                  <div class=\\"ribbon ribbon-top-left\\">Empfohlen</div>\\r\\n                  <h4 class=\\"h430\\"><figure><img src=\\"public/app/images/icon-1.png\\" data-image=\\"0yn5tbbn9n7l\\"></figure>Kombi</h4>\\r\\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\\r\\n                  <a href=\\"javascript:void(0);\\" class=\\"modify-btn ajax-data\\" data-toggle=\\"modal\\" data-target=\\"#myModal\\" data-id=\\"HPKOMBI\\" data-local-json=\\"public/sample_json/HPKOMBI.json\\">Kombi\\r\\n                    Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n          </div>"}}', '2019-02-27 09:21:13', '2019-02-27 09:21:13', '286c632a-bb38-4aff-8821-c8c12da230b4'),
(65, 2, 2, 1, 1, 16, '', '{"typeId":"2","authorId":null,"title":"Homepage","slug":"homepage","postDate":1550067240,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["29"],"3":["28"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n<div class=\\"content-block nomr\\">\\r\\n            <h2 class=\\"content-heading  d-none d-sm-block\\">Tickets Kaufen</h2>\\r\\n            <p class=\\"d-none d-sm-block\\">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\\r\\n              direkt zur Ticketauswahl.\\r\\n            </p>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-12 col-md-12 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color1-bg modal-block\\">\\r\\n                  <div class=\\"ribbon ribbon-top-left\\">Empfohlen</div>\\r\\n                  <h4 class=\\"h430\\"><figure><img src=\\"public/app/images/icon-1.png\\" data-image=\\"0yn5tbbn9n7l\\"></figure>Kombi</h4>\\r\\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\\r\\n                  <a href=\\"javascript:void(0);\\" class=\\"modify-btn ajax-data\\" data-toggle=\\"modal\\" data-target=\\"#myModal\\" data-id=\\"HPKOMBI\\" data-local-json=\\"public/sample_json/HPKOMBI.json\\">Kombi\\r\\n                    Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n          </div>"}}', '2019-02-27 11:31:34', '2019-02-27 11:31:34', 'fec65ecb-87fc-4039-9602-033e16e9fbdf'),
(66, 2, 2, 1, 1, 17, '', '{"typeId":"2","authorId":null,"title":"Homepage","slug":"homepage","postDate":1550067240,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["28"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n<div class=\\"content-block nomr\\">\\r\\n            <h2 class=\\"content-heading  d-none d-sm-block\\">Tickets Kaufen</h2>\\r\\n            <p class=\\"d-none d-sm-block\\">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\\r\\n              direkt zur Ticketauswahl.\\r\\n            </p>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-12 col-md-12 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color1-bg modal-block\\">\\r\\n                  <div class=\\"ribbon ribbon-top-left\\">Empfohlen</div>\\r\\n                  <h4 class=\\"h430\\"><figure><img src=\\"public/app/images/icon-1.png\\" data-image=\\"0yn5tbbn9n7l\\"></figure>Kombi</h4>\\r\\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\\r\\n                  <a href=\\"javascript:void(0);\\" class=\\"modify-btn ajax-data\\" data-toggle=\\"modal\\" data-target=\\"#myModal\\" data-id=\\"HPKOMBI\\" data-local-json=\\"public/sample_json/HPKOMBI.json\\">Kombi\\r\\n                    Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n          </div>"}}', '2019-02-27 14:56:52', '2019-02-27 14:56:52', '7c9b3c73-83d5-4f25-aaa6-6b29d3221646');
INSERT INTO `entryversions` (`id`, `entryId`, `sectionId`, `creatorId`, `siteId`, `num`, `notes`, `data`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(67, 2, 2, 1, 1, 18, '', '{"typeId":"2","authorId":null,"title":"Homepage","slug":"homepage","postDate":1550067240,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["28"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n<div class=\\"content-block nomr\\">\\r\\n            <h2 class=\\"content-heading  d-none d-sm-block\\">Tickets Kaufen</h2>\\r\\n            <p class=\\"d-none d-sm-block\\">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\\r\\n              direkt zur Ticketauswahl.\\r\\n            </p>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-12 col-md-12 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color1-bg modal-block\\">\\r\\n                  <div class=\\"ribbon ribbon-top-left\\"><span>Empfohlen</span></div>\\r\\n                  <h4 class=\\"h430\\"><figure><img src=\\"public/app/images/icon-1.png\\" data-image=\\"0yn5tbbn9n7l\\"></figure>Kombi</h4>\\r\\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\\r\\n                  <a href=\\"javascript:void(0);\\" class=\\"modify-btn ajax-data\\" data-toggle=\\"modal\\" data-target=\\"#myModal\\" data-id=\\"HPKOMBI\\" data-local-json=\\"public/sample_json/HPKOMBI.json\\">Kombi\\r\\n                    Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n          </div>"}}', '2019-02-27 14:57:50', '2019-02-27 14:57:50', 'd9add27b-c9c6-4d3e-bcda-e2e6f3407e19'),
(68, 27, 6, 1, 1, 12, '', '{"typeId":"6","authorId":null,"title":"Presale","slug":"presale","postDate":1551163260,"expiryDate":null,"enabled":true,"newParentId":null,"fields":{"2":["3"],"3":["4"],"1":"<div class=\\"content-block d-none d-sm-block\\">\\r\\n <h2 class=\\"content-heading\\">Vorstellungen</h2>\\r\\n <p>Harry Potter und das venvunschene Kind ist ein Theaterstück in zwei Teilen. Wir empfehlen das\\r\\n Theatererlebnis am glelchen Tag oder an zwei aufeinander folgenden Tagen zu sehen </p>\\r\\n <div class=\\"schedule-block\\">\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Mittwoch\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Donnerstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Freitag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Samstag\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-head\\">\\r\\n Sonntag\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data schedule-blank\\">\\r\\n \\r\\n <label> </label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Eins\\r\\n <label>14.30 Uhr</label>\\r\\n <label class=\\"icon-amp iabc\\">&</label>\\r\\n </div>\\r\\n </div>\\r\\n <div class=\\"schedule-row\\">\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n <label class=\\"icon-amp iarc\\">&</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n <div class=\\"schedule-col schedule-data\\">\\r\\n Teil Zwei\\r\\n <label>19.30 Uhr</label>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n </div>\\r\\n<div class=\\"content-block nomr close\\" style=\\"display:none;\\">\\r\\n            <h2 class=\\"content-heading  d-none d-sm-block\\">Tickets Kaufen</h2>\\r\\n            <p class=\\"d-none d-sm-block\\">Wählen Sie hier die Art der Tickets. die Sie erwerben mochten und Sie gelangen\\r\\n              direkt zur Ticketauswahl.\\r\\n            </p>\\r\\n            <div class=\\"row\\">\\r\\n              <div class=\\"col-xs-12 col-lg-12 col-md-12 no-mo-pd\\">\\r\\n                <div class=\\"banner-block color1-bg modal-block\\">\\r\\n                  <div class=\\"ribbon ribbon-top-left\\">Empfohlen</div>\\r\\n                  <h4 class=\\"h430\\"><figure><img src=\\"/public/app/images/icon-1.png\\" data-image=\\"0yn5tbbn9n7l\\"></figure>Kombi</h4>\\r\\n                  <p>Teil Eins und Teil Zwei aufeinander folgend schauen (empfohlen) </p>\\r\\n                  <a href=\\"javascript:void(0);\\" class=\\"modify-btn ajax-data\\" data-toggle=\\"modal\\" data-target=\\"#myModal\\" data-id=\\"HPKOMBI\\" data-local-json=\\"/public/sample_json/HPKOMBI.json\\">Kombi\\r\\n                    Tickets Kaufen</a>\\r\\n                </div>\\r\\n              </div>\\r\\n            </div>\\r\\n          </div>","8":"<p>Wenn Du Dich bei <a href=\\"http://www.XYZ.de\\">www.XYZ.de</a> registriert hast, hast Du einen Code per Email bekommen. Den kannst Du hier eintragen und exklusiv vor offiziellem Verkaufsstart Tickets Kir Harry Potter und das verwunschene Kind kaufen.</p>","6":"https://www.google.com","5":"<p> Atte Fans, die sich bis zum 15. Marz unter <a href=\\"http://www.XYZ.de\\">www.XYZ.de</a> fur den Newsletter registrieren, haben vier Tage vor dem offiziellen Vorverkaufsstart am 25. Marz die Meglichkeit, hier Tickets zu bestehen. </p>","4":"Noch 10 Tage bis zum Tickets Presale"}}', '2019-02-27 15:00:27', '2019-02-27 15:00:27', '655dabbd-9170-49cd-a69b-4f14ad18d32a');

-- --------------------------------------------------------

--
-- Table structure for table `fieldgroups`
--

CREATE TABLE `fieldgroups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fieldgroups`
--

INSERT INTO `fieldgroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'Common', '2019-02-12 06:57:57', '2019-02-12 06:57:57', '4b12ef31-6e7b-46ab-8d9f-3f4e02b7f1d0'),
(2, 'Homepage', '2019-02-13 14:18:24', '2019-02-13 14:18:24', 'ba148a37-e7fb-446c-babf-82275ed516ca'),
(3, 'Presale', '2019-02-26 11:59:25', '2019-02-26 11:59:25', 'abc46359-3008-4378-a7cc-940a371342ba');

-- --------------------------------------------------------

--
-- Table structure for table `fieldlayoutfields`
--

CREATE TABLE `fieldlayoutfields` (
  `id` int(11) NOT NULL,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fieldlayoutfields`
--

INSERT INTO `fieldlayoutfields` (`id`, `layoutId`, `tabId`, `fieldId`, `required`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 1, 1, 0, 1, '2019-02-13 14:22:39', '2019-02-13 14:22:39', '62dec8ce-811d-4c82-9f52-cc57d1866cfd'),
(2, 2, 2, 2, 0, 1, '2019-02-13 14:23:35', '2019-02-13 14:23:35', '12ec900b-1e62-47a4-8683-05590af642ee'),
(3, 2, 2, 3, 0, 2, '2019-02-13 14:23:35', '2019-02-13 14:23:35', '9c7fc2b0-9802-4067-b726-b9a40ba3910b'),
(4, 2, 2, 1, 0, 3, '2019-02-13 14:23:35', '2019-02-13 14:23:35', '64670eeb-998c-4558-88f8-62bd4efe1ff2'),
(5, 4, 3, 1, 0, 1, '2019-02-14 10:17:40', '2019-02-14 10:17:40', 'b48c84a8-c42f-43c8-aa0f-b48d81bc4457'),
(14, 5, 8, 2, 0, 1, '2019-02-20 12:11:04', '2019-02-20 12:11:04', '930ffcbf-4de0-4242-a5b9-9d9b19f14530'),
(15, 5, 8, 3, 0, 2, '2019-02-20 12:11:04', '2019-02-20 12:11:04', 'aa90b92f-d559-45ce-869c-467512386eba'),
(16, 5, 8, 1, 0, 3, '2019-02-20 12:11:04', '2019-02-20 12:11:04', 'e39012d0-a927-4aa9-b0f9-794fceb297d0'),
(39, 6, 16, 2, 0, 1, '2019-02-26 12:51:33', '2019-02-26 12:51:33', 'c3126632-dd18-434d-b925-8d6e70087596'),
(40, 6, 16, 3, 0, 2, '2019-02-26 12:51:33', '2019-02-26 12:51:33', '66127ddd-acf0-408d-ba5a-3277bf91cadf'),
(41, 6, 16, 1, 0, 3, '2019-02-26 12:51:33', '2019-02-26 12:51:33', '722797c5-24a9-4219-bbc3-66344c156a64'),
(42, 6, 17, 4, 0, 1, '2019-02-26 12:51:33', '2019-02-26 12:51:33', '5f837404-35dd-4f5c-bc2a-784682cc5cd9'),
(43, 6, 17, 5, 0, 2, '2019-02-26 12:51:33', '2019-02-26 12:51:33', '8a44eb51-0016-4d77-8cb5-bcb9896dfae5'),
(44, 6, 17, 6, 0, 3, '2019-02-26 12:51:33', '2019-02-26 12:51:33', '72d697fb-c407-4bfd-aa96-fb0f54fb2a5b'),
(45, 6, 17, 8, 0, 4, '2019-02-26 12:51:33', '2019-02-26 12:51:33', 'b906efba-7467-4ee9-8b7e-814fafa73ad6');

-- --------------------------------------------------------

--
-- Table structure for table `fieldlayouts`
--

CREATE TABLE `fieldlayouts` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fieldlayouts`
--

INSERT INTO `fieldlayouts` (`id`, `type`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 'craft\\elements\\Entry', '2019-02-13 14:13:49', '2019-02-13 14:22:39', NULL, '569da376-0af7-4b55-ab7c-508b7bd27b62'),
(2, 'craft\\elements\\Entry', '2019-02-13 14:14:34', '2019-02-13 14:23:35', NULL, '791eafe6-6f58-49ed-a338-ddab36961686'),
(3, 'craft\\elements\\Asset', '2019-02-13 14:19:34', '2019-02-13 14:32:01', NULL, 'f974a9a0-d21a-4aee-9854-2f799a55702c'),
(4, 'craft\\elements\\Entry', '2019-02-14 10:17:27', '2019-02-14 10:17:40', NULL, '6bf8fa54-6b1b-4b17-bc4a-c397e62b85f7'),
(5, 'craft\\elements\\Entry', '2019-02-18 06:37:18', '2019-02-20 12:11:04', NULL, 'd7e500f2-7902-412e-8f6f-81834d07409a'),
(6, 'craft\\elements\\Entry', '2019-02-26 10:59:45', '2019-02-26 12:51:33', NULL, '6e32d84f-4d5d-4716-9b67-e44fc5277f9f');

-- --------------------------------------------------------

--
-- Table structure for table `fieldlayouttabs`
--

CREATE TABLE `fieldlayouttabs` (
  `id` int(11) NOT NULL,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fieldlayouttabs`
--

INSERT INTO `fieldlayouttabs` (`id`, `layoutId`, `name`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 'Common', 1, '2019-02-13 14:22:39', '2019-02-13 14:22:39', '448749c2-0724-4863-b179-3f621f9bb5ca'),
(2, 2, 'Homepage', 1, '2019-02-13 14:23:35', '2019-02-13 14:23:35', '94270f59-9461-4c17-a14a-0e5dc57c8932'),
(3, 4, 'Common', 1, '2019-02-14 10:17:40', '2019-02-14 10:17:40', 'fc0aef3e-7704-4ab8-83ac-7db9a158d7f8'),
(8, 5, 'Homepage', 1, '2019-02-20 12:11:04', '2019-02-20 12:11:04', '30efb79c-f9b5-40d4-a286-c671b641bb4b'),
(16, 6, 'Banner Section', 1, '2019-02-26 12:51:33', '2019-02-26 12:51:33', '12a8c62a-cecc-42c5-b14d-488bc2ff3143'),
(17, 6, 'Presale Section', 2, '2019-02-26 12:51:33', '2019-02-26 12:51:33', '6158926c-a1b3-42ec-911c-a12f7500b578');

-- --------------------------------------------------------

--
-- Table structure for table `fields`
--

CREATE TABLE `fields` (
  `id` int(11) NOT NULL,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(64) NOT NULL,
  `context` varchar(255) NOT NULL DEFAULT 'global',
  `instructions` text,
  `searchable` tinyint(1) NOT NULL DEFAULT '1',
  `translationMethod` varchar(255) NOT NULL DEFAULT 'none',
  `translationKeyFormat` text,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fields`
--

INSERT INTO `fields` (`id`, `groupId`, `name`, `handle`, `context`, `instructions`, `searchable`, `translationMethod`, `translationKeyFormat`, `type`, `settings`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 'Description', 'description', 'global', 'Add page content.', 1, 'none', NULL, 'craft\\redactor\\Field', '{"redactorConfig":"","purifierConfig":"","cleanupHtml":"","purifyHtml":"","columnType":"text","availableVolumes":"*","availableTransforms":"*"}', '2019-02-13 14:17:54', '2019-02-20 11:23:56', '4b97a3f8-71c2-43a1-92bc-cd3c0b956372'),
(2, 2, 'Banner Image Desktop', 'bannerImage', 'global', 'Upload Desktop Banner Image.', 1, 'site', NULL, 'craft\\fields\\Assets', '{"useSingleFolder":"","defaultUploadLocationSource":"volume:bfe274c4-e8ae-4fdc-9067-b773ff0fe455","defaultUploadLocationSubpath":"public/assets/images/","singleUploadLocationSource":"volume:fe6fbf03-dfa3-47c3-b97e-e4af5c3181b9","singleUploadLocationSubpath":"","restrictFiles":"1","allowedKinds":["image"],"sources":"*","source":null,"targetSiteId":null,"viewMode":"large","limit":"1","selectionLabel":"","localizeRelations":""}', '2019-02-13 14:20:40', '2019-02-27 09:20:07', '894b1035-6236-4783-b58e-74a01ec8d0cb'),
(3, 2, 'Banner Image Mobile', 'bannerImageMobile', 'global', 'Add Banner for the mobile version of the site.', 1, 'site', NULL, 'craft\\fields\\Assets', '{"useSingleFolder":"","defaultUploadLocationSource":"volume:bfe274c4-e8ae-4fdc-9067-b773ff0fe455","defaultUploadLocationSubpath":"","singleUploadLocationSource":"volume:fe6fbf03-dfa3-47c3-b97e-e4af5c3181b9","singleUploadLocationSubpath":"","restrictFiles":"1","allowedKinds":["image"],"sources":"*","source":null,"targetSiteId":null,"viewMode":"large","limit":"1","selectionLabel":"","localizeRelations":""}', '2019-02-13 14:21:33', '2019-02-27 09:20:35', '11d21469-675e-4444-9b9e-cc14e7e8001b'),
(4, 3, 'Registration section title', 'registrationSectionTitle', 'global', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{"placeholder":"","code":"","multiline":"","initialRows":"4","charLimit":"","columnType":"text"}', '2019-02-26 11:59:52', '2019-02-26 11:59:52', '3632190f-0900-4016-ba90-ccf8b7624ce1'),
(5, 3, 'Registration section description', 'registrationSectionDescription', 'global', '', 1, 'none', NULL, 'craft\\redactor\\Field', '{"redactorConfig":"","purifierConfig":"","cleanupHtml":"","purifyHtml":"","columnType":"text","availableVolumes":"*","availableTransforms":"*"}', '2019-02-26 12:00:30', '2019-02-26 12:00:30', '75fdba86-f610-4fcf-8095-9ff4aa3023fb'),
(6, 3, 'Registration Link', 'registrationLink', 'global', '', 1, 'none', NULL, 'craft\\fields\\Url', '{"placeholder":""}', '2019-02-26 12:00:56', '2019-02-26 12:00:56', '8f6d38b3-d933-42d0-9c36-f1bb4eb3a7c9'),
(8, 3, 'Registration code description', 'registrationCodeDescription', 'global', '', 1, 'none', NULL, 'craft\\redactor\\Field', '{"redactorConfig":"","purifierConfig":"","cleanupHtml":"","purifyHtml":"","columnType":"text","availableVolumes":"*","availableTransforms":"*"}', '2019-02-26 12:51:13', '2019-02-26 12:51:13', 'af29dd7e-bfc6-49d1-b58c-a33138f64b69');

-- --------------------------------------------------------

--
-- Table structure for table `globalsets`
--

CREATE TABLE `globalsets` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `id` int(11) NOT NULL,
  `version` varchar(50) NOT NULL,
  `schemaVersion` varchar(15) NOT NULL,
  `maintenance` tinyint(1) NOT NULL DEFAULT '0',
  `config` mediumtext,
  `configMap` mediumtext,
  `fieldVersion` char(12) NOT NULL DEFAULT '000000000000',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`id`, `version`, `schemaVersion`, `maintenance`, `config`, `configMap`, `fieldVersion`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, '3.1.12', '3.1.25', 0, 'a:17:{s:12:"dateModified";i:1551259235;s:10:"siteGroups";a:1:{s:36:"874ec50f-856d-46fb-867c-d9b4af8688bf";a:1:{s:4:"name";s:24:"Harrypotter Ticketmaster";}}s:5:"sites";a:2:{s:36:"729f3241-22db-46b8-bb9c-d71237b64b1b";a:8:{s:4:"name";s:24:"Harrypotter Ticketmaster";s:6:"handle";s:7:"default";s:8:"language";s:2:"de";s:7:"hasUrls";s:1:"1";s:7:"baseUrl";s:5:"@web/";s:9:"sortOrder";s:1:"1";s:7:"primary";s:1:"1";s:9:"siteGroup";s:36:"874ec50f-856d-46fb-867c-d9b4af8688bf";}s:36:"68cc0d46-d25a-403b-bac6-43627f738486";a:8:{s:4:"name";s:32:"Harrypotter Ticketmaster English";s:6:"handle";s:30:"harrypotterTicketmasterEnglish";s:8:"language";s:2:"en";s:7:"hasUrls";s:1:"1";s:7:"baseUrl";s:8:"@web/en/";s:9:"sortOrder";s:1:"2";s:7:"primary";s:1:"0";s:9:"siteGroup";s:36:"874ec50f-856d-46fb-867c-d9b4af8688bf";}}s:8:"sections";a:5:{s:36:"e1b05292-79dc-42a6-baa4-8d24956e278a";a:8:{s:4:"name";s:10:"Basic Page";s:6:"handle";s:9:"basicPage";s:4:"type";s:9:"structure";s:16:"enableVersioning";s:1:"1";s:16:"propagateEntries";s:1:"0";s:9:"structure";a:2:{s:3:"uid";s:36:"6c5e095e-48ec-40b1-880a-41bd87a28529";s:9:"maxLevels";N;}s:10:"entryTypes";a:1:{s:36:"5516b7f3-f978-478f-b6fe-d6f9fd4529ac";a:7:{s:4:"name";s:10:"Basic Page";s:6:"handle";s:9:"basicPage";s:13:"hasTitleField";s:1:"1";s:10:"titleLabel";s:5:"Title";s:11:"titleFormat";N;s:9:"sortOrder";s:1:"1";s:12:"fieldLayouts";a:1:{s:36:"569da376-0af7-4b55-ab7c-508b7bd27b62";a:1:{s:4:"tabs";a:1:{i:0;a:3:{s:4:"name";s:6:"Common";s:9:"sortOrder";s:1:"1";s:6:"fields";a:1:{s:36:"4b97a3f8-71c2-43a1-92bc-cd3c0b956372";a:2:{s:8:"required";s:1:"0";s:9:"sortOrder";s:1:"1";}}}}}}}}s:12:"siteSettings";a:2:{s:36:"729f3241-22db-46b8-bb9c-d71237b64b1b";a:4:{s:16:"enabledByDefault";s:1:"1";s:7:"hasUrls";s:1:"1";s:9:"uriFormat";s:6:"{slug}";s:8:"template";s:16:"basicPage/_entry";}s:36:"68cc0d46-d25a-403b-bac6-43627f738486";a:4:{s:16:"enabledByDefault";s:1:"1";s:7:"hasUrls";s:1:"1";s:9:"uriFormat";s:6:"{slug}";s:8:"template";s:19:"en/basicPage/_entry";}}}s:36:"5afdd829-3b74-411b-a718-30f79c756455";a:8:{s:4:"name";s:15:"Editable Blocks";s:6:"handle";s:14:"editableBlocks";s:4:"type";s:9:"structure";s:16:"enableVersioning";s:1:"1";s:16:"propagateEntries";s:1:"0";s:9:"structure";a:2:{s:3:"uid";s:36:"e521864e-772a-4828-be80-c15f248d77d4";s:9:"maxLevels";N;}s:10:"entryTypes";a:1:{s:36:"b4a8a7da-aec1-4a30-825a-2a30c53c3989";a:7:{s:4:"name";s:15:"Editable Blocks";s:6:"handle";s:14:"editableBlocks";s:13:"hasTitleField";s:1:"1";s:10:"titleLabel";s:5:"Title";s:11:"titleFormat";N;s:9:"sortOrder";s:1:"1";s:12:"fieldLayouts";a:1:{s:36:"6bf8fa54-6b1b-4b17-bc4a-c397e62b85f7";a:1:{s:4:"tabs";a:1:{i:0;a:3:{s:4:"name";s:6:"Common";s:9:"sortOrder";s:1:"1";s:6:"fields";a:1:{s:36:"4b97a3f8-71c2-43a1-92bc-cd3c0b956372";a:2:{s:8:"required";s:1:"0";s:9:"sortOrder";s:1:"1";}}}}}}}}s:12:"siteSettings";a:2:{s:36:"729f3241-22db-46b8-bb9c-d71237b64b1b";a:4:{s:16:"enabledByDefault";s:1:"1";s:7:"hasUrls";s:1:"0";s:9:"uriFormat";N;s:8:"template";N;}s:36:"68cc0d46-d25a-403b-bac6-43627f738486";a:4:{s:16:"enabledByDefault";s:1:"1";s:7:"hasUrls";s:1:"0";s:9:"uriFormat";N;s:8:"template";N;}}}s:36:"9f15624d-d02b-4800-8f08-19a98b8b8118";a:7:{s:4:"name";s:8:"Homepage";s:6:"handle";s:8:"homepage";s:4:"type";s:6:"single";s:16:"enableVersioning";s:1:"1";s:16:"propagateEntries";s:1:"1";s:10:"entryTypes";a:1:{s:36:"c656bd59-09b9-49db-b5c2-5f1b76865b51";a:7:{s:4:"name";s:8:"Homepage";s:6:"handle";s:8:"homepage";s:13:"hasTitleField";s:1:"0";s:10:"titleLabel";N;s:11:"titleFormat";s:18:"{section.name|raw}";s:9:"sortOrder";s:1:"1";s:12:"fieldLayouts";a:1:{s:36:"791eafe6-6f58-49ed-a338-ddab36961686";a:1:{s:4:"tabs";a:1:{i:0;a:3:{s:4:"name";s:8:"Homepage";s:9:"sortOrder";s:1:"1";s:6:"fields";a:3:{s:36:"894b1035-6236-4783-b58e-74a01ec8d0cb";a:2:{s:8:"required";s:1:"0";s:9:"sortOrder";s:1:"1";}s:36:"11d21469-675e-4444-9b9e-cc14e7e8001b";a:2:{s:8:"required";s:1:"0";s:9:"sortOrder";s:1:"2";}s:36:"4b97a3f8-71c2-43a1-92bc-cd3c0b956372";a:2:{s:8:"required";s:1:"0";s:9:"sortOrder";s:1:"3";}}}}}}}}s:12:"siteSettings";a:1:{s:36:"729f3241-22db-46b8-bb9c-d71237b64b1b";a:4:{s:16:"enabledByDefault";s:1:"1";s:7:"hasUrls";s:1:"1";s:9:"uriFormat";s:8:"__home__";s:8:"template";s:5:"index";}}}s:36:"c641c838-23d8-4911-96cf-76b439df5a95";a:7:{s:4:"name";s:16:"HomePage English";s:6:"handle";s:15:"homepageEnglish";s:4:"type";s:6:"single";s:16:"enableVersioning";s:1:"1";s:16:"propagateEntries";s:1:"1";s:10:"entryTypes";a:1:{s:36:"40bf0892-5726-4ea7-9dc5-397489d5e941";a:7:{s:4:"name";s:16:"HomePage English";s:6:"handle";s:15:"homepageEnglish";s:13:"hasTitleField";b:0;s:10:"titleLabel";s:0:"";s:11:"titleFormat";s:18:"{section.name|raw}";s:9:"sortOrder";i:1;s:12:"fieldLayouts";a:1:{s:36:"d7e500f2-7902-412e-8f6f-81834d07409a";a:1:{s:4:"tabs";a:1:{i:0;a:3:{s:4:"name";s:8:"Homepage";s:9:"sortOrder";i:1;s:6:"fields";a:3:{s:36:"894b1035-6236-4783-b58e-74a01ec8d0cb";a:2:{s:8:"required";b:0;s:9:"sortOrder";i:1;}s:36:"11d21469-675e-4444-9b9e-cc14e7e8001b";a:2:{s:8:"required";b:0;s:9:"sortOrder";i:2;}s:36:"4b97a3f8-71c2-43a1-92bc-cd3c0b956372";a:2:{s:8:"required";b:0;s:9:"sortOrder";i:3;}}}}}}}}s:12:"siteSettings";a:1:{s:36:"68cc0d46-d25a-403b-bac6-43627f738486";a:4:{s:16:"enabledByDefault";s:1:"1";s:7:"hasUrls";s:1:"1";s:9:"uriFormat";s:8:"__home__";s:8:"template";s:8:"en/index";}}}s:36:"30b6bf6f-7f03-4793-bc60-e6ad58c51bcd";a:7:{s:4:"name";s:7:"Presale";s:6:"handle";s:7:"presale";s:4:"type";s:6:"single";s:16:"enableVersioning";b:1;s:16:"propagateEntries";b:1;s:12:"siteSettings";a:2:{s:36:"729f3241-22db-46b8-bb9c-d71237b64b1b";a:4:{s:16:"enabledByDefault";b:1;s:7:"hasUrls";b:1;s:9:"uriFormat";s:7:"presale";s:8:"template";s:13:"presale/index";}s:36:"68cc0d46-d25a-403b-bac6-43627f738486";a:4:{s:16:"enabledByDefault";b:1;s:7:"hasUrls";b:1;s:9:"uriFormat";s:7:"presale";s:8:"template";s:13:"presale/index";}}s:10:"entryTypes";a:1:{s:36:"364de46c-1368-431c-a67e-b625cc680e62";a:7:{s:4:"name";s:7:"Presale";s:6:"handle";s:7:"presale";s:13:"hasTitleField";b:0;s:10:"titleLabel";s:0:"";s:11:"titleFormat";s:18:"{section.name|raw}";s:9:"sortOrder";i:1;s:12:"fieldLayouts";a:1:{s:36:"6e32d84f-4d5d-4716-9b67-e44fc5277f9f";a:1:{s:4:"tabs";a:2:{i:0;a:3:{s:4:"name";s:14:"Banner Section";s:9:"sortOrder";i:1;s:6:"fields";a:3:{s:36:"894b1035-6236-4783-b58e-74a01ec8d0cb";a:2:{s:8:"required";b:0;s:9:"sortOrder";i:1;}s:36:"11d21469-675e-4444-9b9e-cc14e7e8001b";a:2:{s:8:"required";b:0;s:9:"sortOrder";i:2;}s:36:"4b97a3f8-71c2-43a1-92bc-cd3c0b956372";a:2:{s:8:"required";b:0;s:9:"sortOrder";i:3;}}}i:1;a:3:{s:4:"name";s:15:"Presale Section";s:9:"sortOrder";i:2;s:6:"fields";a:4:{s:36:"3632190f-0900-4016-ba90-ccf8b7624ce1";a:2:{s:8:"required";b:0;s:9:"sortOrder";i:1;}s:36:"75fdba86-f610-4fcf-8095-9ff4aa3023fb";a:2:{s:8:"required";b:0;s:9:"sortOrder";i:2;}s:36:"8f6d38b3-d933-42d0-9c36-f1bb4eb3a7c9";a:2:{s:8:"required";b:0;s:9:"sortOrder";i:3;}s:36:"af29dd7e-bfc6-49d1-b58c-a33138f64b69";a:2:{s:8:"required";b:0;s:9:"sortOrder";i:4;}}}}}}}}}}s:11:"fieldGroups";a:3:{s:36:"4b12ef31-6e7b-46ab-8d9f-3f4e02b7f1d0";a:1:{s:4:"name";s:6:"Common";}s:36:"ba148a37-e7fb-446c-babf-82275ed516ca";a:1:{s:4:"name";s:8:"Homepage";}s:36:"abc46359-3008-4378-a7cc-940a371342ba";a:1:{s:4:"name";s:7:"Presale";}}s:6:"fields";a:7:{s:36:"4b97a3f8-71c2-43a1-92bc-cd3c0b956372";a:10:{s:4:"name";s:11:"Description";s:6:"handle";s:11:"description";s:12:"instructions";s:17:"Add page content.";s:10:"searchable";b:1;s:17:"translationMethod";s:4:"none";s:20:"translationKeyFormat";N;s:4:"type";s:20:"craft\\redactor\\Field";s:8:"settings";a:7:{s:14:"redactorConfig";s:0:"";s:14:"purifierConfig";s:0:"";s:11:"cleanupHtml";s:0:"";s:10:"purifyHtml";s:0:"";s:10:"columnType";s:4:"text";s:16:"availableVolumes";s:1:"*";s:19:"availableTransforms";s:1:"*";}s:17:"contentColumnType";s:4:"text";s:10:"fieldGroup";s:36:"4b12ef31-6e7b-46ab-8d9f-3f4e02b7f1d0";}s:36:"894b1035-6236-4783-b58e-74a01ec8d0cb";a:10:{s:4:"name";s:20:"Banner Image Desktop";s:6:"handle";s:11:"bannerImage";s:12:"instructions";s:28:"Upload Desktop Banner Image.";s:10:"searchable";b:1;s:17:"translationMethod";s:4:"site";s:20:"translationKeyFormat";N;s:4:"type";s:19:"craft\\fields\\Assets";s:8:"settings";a:14:{s:15:"useSingleFolder";s:0:"";s:27:"defaultUploadLocationSource";s:43:"volume:bfe274c4-e8ae-4fdc-9067-b773ff0fe455";s:28:"defaultUploadLocationSubpath";s:21:"public/assets/images/";s:26:"singleUploadLocationSource";s:43:"volume:fe6fbf03-dfa3-47c3-b97e-e4af5c3181b9";s:27:"singleUploadLocationSubpath";s:0:"";s:13:"restrictFiles";s:1:"1";s:12:"allowedKinds";a:1:{i:0;s:5:"image";}s:7:"sources";s:1:"*";s:6:"source";N;s:12:"targetSiteId";N;s:8:"viewMode";s:5:"large";s:5:"limit";s:1:"1";s:14:"selectionLabel";s:0:"";s:17:"localizeRelations";s:0:"";}s:17:"contentColumnType";s:6:"string";s:10:"fieldGroup";s:36:"ba148a37-e7fb-446c-babf-82275ed516ca";}s:36:"11d21469-675e-4444-9b9e-cc14e7e8001b";a:10:{s:4:"name";s:19:"Banner Image Mobile";s:6:"handle";s:17:"bannerImageMobile";s:12:"instructions";s:46:"Add Banner for the mobile version of the site.";s:10:"searchable";b:1;s:17:"translationMethod";s:4:"site";s:20:"translationKeyFormat";N;s:4:"type";s:19:"craft\\fields\\Assets";s:8:"settings";a:14:{s:15:"useSingleFolder";s:0:"";s:27:"defaultUploadLocationSource";s:43:"volume:bfe274c4-e8ae-4fdc-9067-b773ff0fe455";s:28:"defaultUploadLocationSubpath";s:0:"";s:26:"singleUploadLocationSource";s:43:"volume:fe6fbf03-dfa3-47c3-b97e-e4af5c3181b9";s:27:"singleUploadLocationSubpath";s:0:"";s:13:"restrictFiles";s:1:"1";s:12:"allowedKinds";a:1:{i:0;s:5:"image";}s:7:"sources";s:1:"*";s:6:"source";N;s:12:"targetSiteId";N;s:8:"viewMode";s:5:"large";s:5:"limit";s:1:"1";s:14:"selectionLabel";s:0:"";s:17:"localizeRelations";s:0:"";}s:17:"contentColumnType";s:6:"string";s:10:"fieldGroup";s:36:"ba148a37-e7fb-446c-babf-82275ed516ca";}s:36:"3632190f-0900-4016-ba90-ccf8b7624ce1";a:10:{s:4:"name";s:26:"Registration section title";s:6:"handle";s:24:"registrationSectionTitle";s:12:"instructions";s:0:"";s:10:"searchable";b:1;s:17:"translationMethod";s:4:"none";s:20:"translationKeyFormat";N;s:4:"type";s:22:"craft\\fields\\PlainText";s:8:"settings";a:6:{s:11:"placeholder";s:0:"";s:4:"code";s:0:"";s:9:"multiline";s:0:"";s:11:"initialRows";s:1:"4";s:9:"charLimit";s:0:"";s:10:"columnType";s:4:"text";}s:17:"contentColumnType";s:4:"text";s:10:"fieldGroup";s:36:"abc46359-3008-4378-a7cc-940a371342ba";}s:36:"75fdba86-f610-4fcf-8095-9ff4aa3023fb";a:10:{s:4:"name";s:32:"Registration section description";s:6:"handle";s:30:"registrationSectionDescription";s:12:"instructions";s:0:"";s:10:"searchable";b:1;s:17:"translationMethod";s:4:"none";s:20:"translationKeyFormat";N;s:4:"type";s:20:"craft\\redactor\\Field";s:8:"settings";a:7:{s:14:"redactorConfig";s:0:"";s:14:"purifierConfig";s:0:"";s:11:"cleanupHtml";s:0:"";s:10:"purifyHtml";s:0:"";s:10:"columnType";s:4:"text";s:16:"availableVolumes";s:1:"*";s:19:"availableTransforms";s:1:"*";}s:17:"contentColumnType";s:4:"text";s:10:"fieldGroup";s:36:"abc46359-3008-4378-a7cc-940a371342ba";}s:36:"8f6d38b3-d933-42d0-9c36-f1bb4eb3a7c9";a:10:{s:4:"name";s:17:"Registration Link";s:6:"handle";s:16:"registrationLink";s:12:"instructions";s:0:"";s:10:"searchable";b:1;s:17:"translationMethod";s:4:"none";s:20:"translationKeyFormat";N;s:4:"type";s:16:"craft\\fields\\Url";s:8:"settings";a:1:{s:11:"placeholder";s:0:"";}s:17:"contentColumnType";s:6:"string";s:10:"fieldGroup";s:36:"abc46359-3008-4378-a7cc-940a371342ba";}s:36:"af29dd7e-bfc6-49d1-b58c-a33138f64b69";a:10:{s:4:"name";s:29:"Registration code description";s:6:"handle";s:27:"registrationCodeDescription";s:12:"instructions";s:0:"";s:10:"searchable";b:1;s:17:"translationMethod";s:4:"none";s:20:"translationKeyFormat";N;s:4:"type";s:20:"craft\\redactor\\Field";s:8:"settings";a:7:{s:14:"redactorConfig";s:0:"";s:14:"purifierConfig";s:0:"";s:11:"cleanupHtml";s:0:"";s:10:"purifyHtml";s:0:"";s:10:"columnType";s:4:"text";s:16:"availableVolumes";s:1:"*";s:19:"availableTransforms";s:1:"*";}s:17:"contentColumnType";s:4:"text";s:10:"fieldGroup";s:36:"abc46359-3008-4378-a7cc-940a371342ba";}}s:16:"matrixBlockTypes";a:0:{}s:7:"volumes";a:2:{s:36:"fe6fbf03-dfa3-47c3-b97e-e4af5c3181b9";a:8:{s:4:"name";s:7:"uploads";s:6:"handle";s:7:"uploads";s:4:"type";s:19:"craft\\volumes\\Local";s:7:"hasUrls";s:1:"1";s:3:"url";s:26:"@web/public/assets/images/";s:8:"settings";a:1:{s:4:"path";s:30:"@webroot/public/assets/images/";}s:9:"sortOrder";s:1:"1";s:12:"fieldLayouts";a:1:{s:36:"f974a9a0-d21a-4aee-9854-2f799a55702c";a:1:{s:4:"tabs";a:0:{}}}}s:36:"bfe274c4-e8ae-4fdc-9067-b773ff0fe455";a:7:{s:4:"name";s:21:"Object Storage Assets";s:6:"handle";s:19:"objectStorageAssets";s:4:"type";s:31:"fortrabbit\\ObjectStorage\\Volume";s:7:"hasUrls";b:1;s:3:"url";b:0;s:8:"settings";a:7:{s:9:"subfolder";s:6:"assets";s:5:"keyId";s:13:"tmharrypotter";s:6:"secret";s:23:"(OBJECT_STORAGE_SECRET)";s:6:"bucket";s:13:"tmharrypotter";s:6:"region";s:21:"(us-east-1|eu-west-1)";s:7:"expires";s:0:"";s:8:"endpoint";s:35:"https://objects.(us1|eu2).frbit.com";}s:9:"sortOrder";i:2;}}s:14:"categoryGroups";a:0:{}s:9:"tagGroups";a:0:{}s:5:"users";a:5:{s:24:"requireEmailVerification";b:1;s:23:"allowPublicRegistration";b:0;s:12:"defaultGroup";N;s:14:"photoVolumeUid";N;s:12:"photoSubpath";s:0:"";}s:10:"globalSets";a:0:{}s:7:"plugins";a:5:{s:10:"olivemenus";a:4:{s:8:"settings";N;s:10:"licenseKey";N;s:7:"enabled";s:1:"1";s:13:"schemaVersion";s:5:"1.0.0";}s:21:"cookie-consent-banner";a:4:{s:8:"settings";a:17:{s:8:"position";s:7:"toppush";s:6:"layout";s:5:"block";s:7:"palette";s:7:"default";s:14:"palette_banner";s:7:"#907D46";s:14:"palette_button";s:4:"#000";s:19:"palette_banner_text";s:7:"#ffffff";s:19:"palette_button_text";s:4:"#fff";s:15:"learn_more_link";s:25:"http://cookiesandyou.com/";s:7:"message";s:79:"This website uses cookies to ensure you get the best experience on our website.";s:7:"dismiss";s:10:"ALLES KLAR";s:5:"learn";s:10:"Learn More";s:11:"preload_css";s:0:"";s:8:"async_js";s:0:"";s:8:"defer_js";s:0:"";s:23:"disable_in_live_preview";s:0:"";s:20:"excluded_entry_types";N;s:19:"excluded_categories";N;}s:10:"licenseKey";N;s:7:"enabled";s:1:"1";s:13:"schemaVersion";s:5:"1.0.0";}s:8:"redactor";a:3:{s:7:"edition";s:8:"standard";s:7:"enabled";b:1;s:13:"schemaVersion";s:5:"2.2.1";}s:12:"contact-form";a:4:{s:7:"edition";s:8:"standard";s:7:"enabled";b:1;s:13:"schemaVersion";s:5:"1.0.0";s:8:"settings";a:5:{s:7:"toEmail";s:31:"abhinglajia@cygnet-infotech.com";s:13:"prependSender";s:12:"On behalf of";s:14:"prependSubject";s:41:"New message from Harrypotter Ticketmaster";s:16:"allowAttachments";s:0:"";s:19:"successFlashMessage";s:27:"Your message has been sent.";}}s:25:"fortrabbit-object-storage";a:3:{s:7:"edition";s:8:"standard";s:7:"enabled";b:1;s:13:"schemaVersion";s:5:"1.0.0";}}s:5:"email";a:3:{s:9:"fromEmail";s:31:"abhinglajia@cygnet-infotech.com";s:8:"fromName";s:24:"Harrypotter Ticketmaster";s:13:"transportType";s:37:"craft\\mail\\transportadapters\\Sendmail";}s:6:"system";a:5:{s:7:"edition";s:4:"solo";s:4:"live";b:1;s:4:"name";s:24:"Harrypotter Ticketmaster";s:8:"timeZone";s:19:"America/Los_Angeles";s:13:"schemaVersion";s:6:"3.1.25";}s:15:"imageTransforms";a:0:{}s:6:"routes";a:0:{}}', NULL, 'f8xU1BHFwHPG', '2019-02-12 06:57:57', '2019-02-27 09:20:36', '51bd7342-66e9-42d5-b14a-4441773e4558');

-- --------------------------------------------------------

--
-- Table structure for table `matrixblocks`
--

CREATE TABLE `matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `ownerSiteId` int(11) DEFAULT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `deletedWithOwner` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `matrixblocktypes`
--

CREATE TABLE `matrixblocktypes` (
  `id` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(11) NOT NULL,
  `pluginId` int(11) DEFAULT NULL,
  `type` enum('app','plugin','content') NOT NULL DEFAULT 'app',
  `name` varchar(255) NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `pluginId`, `type`, `name`, `applyTime`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, NULL, 'app', 'Install', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '36d546bc-222a-411f-b368-f44849b01fe6'),
(2, NULL, 'app', 'm150403_183908_migrations_table_changes', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '1475ae2c-2169-407d-8327-4d2b5ed6a8b2'),
(3, NULL, 'app', 'm150403_184247_plugins_table_changes', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '7f4da2ce-0c68-4a9b-b66f-b42280c3c685'),
(4, NULL, 'app', 'm150403_184533_field_version', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '6b9ddd47-404a-4931-97b5-c539d30af6ba'),
(5, NULL, 'app', 'm150403_184729_type_columns', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'b892020c-8f92-489a-8910-7618805c433d'),
(6, NULL, 'app', 'm150403_185142_volumes', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2b48c2da-79f6-4709-bb35-8acf89587495'),
(7, NULL, 'app', 'm150428_231346_userpreferences', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '64d5908e-fa63-455c-9e6d-ccfa16e3164c'),
(8, NULL, 'app', 'm150519_150900_fieldversion_conversion', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'e6288c04-00a3-40d9-962c-f842d1921075'),
(9, NULL, 'app', 'm150617_213829_update_email_settings', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'bd9be1c0-a099-43b5-b979-1571a6e47483'),
(10, NULL, 'app', 'm150721_124739_templatecachequeries', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '6c7ce1be-a69a-4d23-a336-a7e8c7e0caa2'),
(11, NULL, 'app', 'm150724_140822_adjust_quality_settings', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'fb4aea72-e23a-4b8e-bc71-17dc5f400eba'),
(12, NULL, 'app', 'm150815_133521_last_login_attempt_ip', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'ea987620-2389-411d-b1c3-9b3f0d269db5'),
(13, NULL, 'app', 'm151002_095935_volume_cache_settings', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '64dadb77-3551-4928-98fb-05d993623003'),
(14, NULL, 'app', 'm151005_142750_volume_s3_storage_settings', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '821d0140-771f-4383-bafa-183bc66b1c76'),
(15, NULL, 'app', 'm151016_133600_delete_asset_thumbnails', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'e6cf950f-8f88-4da6-86b9-166861ecfe00'),
(16, NULL, 'app', 'm151209_000000_move_logo', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '51b957c3-5a1f-4933-8cc8-59099875be3f'),
(17, NULL, 'app', 'm151211_000000_rename_fileId_to_assetId', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'b03a30ad-4cd3-488b-85e4-156572705847'),
(18, NULL, 'app', 'm151215_000000_rename_asset_permissions', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '40524670-63e2-4be9-ab9e-eff65d3e7e4c'),
(19, NULL, 'app', 'm160707_000001_rename_richtext_assetsource_setting', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'b8f8730b-06a5-4a21-a3e1-7eeec00844fd'),
(20, NULL, 'app', 'm160708_185142_volume_hasUrls_setting', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'd220ba11-3bf6-40e6-a9ab-49bd540c3074'),
(21, NULL, 'app', 'm160714_000000_increase_max_asset_filesize', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '66aa4e7e-c631-47cd-8e01-0ffebe44e302'),
(22, NULL, 'app', 'm160727_194637_column_cleanup', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '5afbb37a-8ee7-4d88-95cc-02169f333232'),
(23, NULL, 'app', 'm160804_110002_userphotos_to_assets', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '4cf18f81-457e-4ff3-93d7-75104776a996'),
(24, NULL, 'app', 'm160807_144858_sites', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '9c55f64a-735b-453f-8646-14bdfb75b4f8'),
(25, NULL, 'app', 'm160829_000000_pending_user_content_cleanup', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '8da71c27-7c3c-42a4-9771-ea899861d8b8'),
(26, NULL, 'app', 'm160830_000000_asset_index_uri_increase', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2a3b3c39-f2d7-445c-a769-b327b4b2626f'),
(27, NULL, 'app', 'm160912_230520_require_entry_type_id', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '54535a72-f5b9-420c-abbe-d5981c88c7c9'),
(28, NULL, 'app', 'm160913_134730_require_matrix_block_type_id', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '4e7222a9-0579-4ffa-a85f-5ef6cf15e8bd'),
(29, NULL, 'app', 'm160920_174553_matrixblocks_owner_site_id_nullable', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2cdef05e-a9f6-4f92-8fce-413fbc4fed57'),
(30, NULL, 'app', 'm160920_231045_usergroup_handle_title_unique', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '39679fa3-21a8-46c6-862d-41db2adb0124'),
(31, NULL, 'app', 'm160925_113941_route_uri_parts', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '35b6fe99-916a-46bf-99ee-3fb7bd584348'),
(32, NULL, 'app', 'm161006_205918_schemaVersion_not_null', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '200d7f92-44e5-4315-b780-4215cfa488d1'),
(33, NULL, 'app', 'm161007_130653_update_email_settings', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '628e5e0a-dddf-4158-8402-62e588e75d8c'),
(34, NULL, 'app', 'm161013_175052_newParentId', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'f62f4da6-0a87-4981-8ccd-34ff9aba6f56'),
(35, NULL, 'app', 'm161021_102916_fix_recent_entries_widgets', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'c75235ae-0a69-46c7-851f-f795c5cb5732'),
(36, NULL, 'app', 'm161021_182140_rename_get_help_widget', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '4a46ac20-2eca-4297-8dc6-c293b92c76fe'),
(37, NULL, 'app', 'm161025_000000_fix_char_columns', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '1e073a36-95c8-47eb-a687-b816d57e8309'),
(38, NULL, 'app', 'm161029_124145_email_message_languages', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '559ff18b-5e67-4b30-a7f2-2582dd3c2f4a'),
(39, NULL, 'app', 'm161108_000000_new_version_format', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'ffb57139-ede5-432e-84ee-fec1d7a731f1'),
(40, NULL, 'app', 'm161109_000000_index_shuffle', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '64794ad2-c70b-4c7f-be52-755ca68b1fcf'),
(41, NULL, 'app', 'm161122_185500_no_craft_app', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '07c910d4-8d70-467d-9dcc-635df5f1f9ac'),
(42, NULL, 'app', 'm161125_150752_clear_urlmanager_cache', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2e196082-4799-40de-9487-4085f1318f08'),
(43, NULL, 'app', 'm161220_000000_volumes_hasurl_notnull', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'b81f4845-280a-4cb3-8711-872eb1f90ad9'),
(44, NULL, 'app', 'm170114_161144_udates_permission', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '8e6139a8-f015-4a88-b852-4b07468d6125'),
(45, NULL, 'app', 'm170120_000000_schema_cleanup', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'e09fb8c9-68f2-45b5-b21b-9b081a10696b'),
(46, NULL, 'app', 'm170126_000000_assets_focal_point', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'a1730130-8886-47f8-902c-4c790df92e70'),
(47, NULL, 'app', 'm170206_142126_system_name', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'dbcd2a69-6ada-4ade-8589-6c9223942366'),
(48, NULL, 'app', 'm170217_044740_category_branch_limits', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'fd606a0b-76ac-4688-a512-cf21b422505b'),
(49, NULL, 'app', 'm170217_120224_asset_indexing_columns', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '9feb3d40-d73b-4573-901e-a969a658919c'),
(50, NULL, 'app', 'm170223_224012_plain_text_settings', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'fde8e005-6306-4cce-9de9-50fe9d9aa8ff'),
(51, NULL, 'app', 'm170227_120814_focal_point_percentage', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'abb9e31f-bc7f-4907-87c0-5873a71b7ef8'),
(52, NULL, 'app', 'm170228_171113_system_messages', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'd17a3b27-ad8c-4e43-a3d9-3f4fada593ff'),
(53, NULL, 'app', 'm170303_140500_asset_field_source_settings', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '0fc9f788-b59e-4e4c-922b-88e545d5a1aa'),
(54, NULL, 'app', 'm170306_150500_asset_temporary_uploads', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'dcbbddb0-84fd-4579-aa8e-4d6e73c7be4a'),
(55, NULL, 'app', 'm170414_162429_rich_text_config_setting', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '16020901-d2b9-47fd-bc94-f67874fb2f11'),
(56, NULL, 'app', 'm170523_190652_element_field_layout_ids', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'a62663b6-c544-497d-8dfd-25687efaebac'),
(57, NULL, 'app', 'm170612_000000_route_index_shuffle', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'dfad29d5-4700-40b2-b720-bee212fef582'),
(58, NULL, 'app', 'm170621_195237_format_plugin_handles', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'fb26094c-049e-44fb-b7aa-c46b610b55c5'),
(59, NULL, 'app', 'm170630_161028_deprecation_changes', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'ce3fa14a-5a35-4754-9030-53b4ea794d1d'),
(60, NULL, 'app', 'm170703_181539_plugins_table_tweaks', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '0aff83f8-9080-46b7-b463-8b3b4f5e9499'),
(61, NULL, 'app', 'm170704_134916_sites_tables', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'd97cb756-78c9-44cf-a0e7-e5fc674ede4d'),
(62, NULL, 'app', 'm170706_183216_rename_sequences', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '8c756612-5630-47e0-a620-2f184cd920b9'),
(63, NULL, 'app', 'm170707_094758_delete_compiled_traits', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'e12417c5-033c-4acd-87de-20eca46620ec'),
(64, NULL, 'app', 'm170731_190138_drop_asset_packagist', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'c1ba0dc4-f3ea-4cb8-a2fc-c43cd85eb3cc'),
(65, NULL, 'app', 'm170810_201318_create_queue_table', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '7c847804-44e3-447f-87e2-0aec3d97c6ce'),
(66, NULL, 'app', 'm170816_133741_delete_compiled_behaviors', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '7c40f1fe-f50d-4091-9049-1eb3c6146e41'),
(67, NULL, 'app', 'm170821_180624_deprecation_line_nullable', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '70a5e19c-1c08-448a-94f2-1cd5122c2fb1'),
(68, NULL, 'app', 'm170903_192801_longblob_for_queue_jobs', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '3b922a9d-c993-4e9a-bd78-d5f284506465'),
(69, NULL, 'app', 'm170914_204621_asset_cache_shuffle', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '6f8f2531-059a-418e-9f27-c99ad31918ab'),
(70, NULL, 'app', 'm171011_214115_site_groups', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'e0a56300-ff28-4214-9bee-992c630e177e'),
(71, NULL, 'app', 'm171012_151440_primary_site', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '44566818-b2ec-46d6-a527-5360155522ce'),
(72, NULL, 'app', 'm171013_142500_transform_interlace', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '47750f42-35bc-4d3b-ba12-7edc495f69ac'),
(73, NULL, 'app', 'm171016_092553_drop_position_select', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'cc5c646d-af08-4935-ace3-2f7b7836d050'),
(74, NULL, 'app', 'm171016_221244_less_strict_translation_method', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '30807fd0-8313-44a5-b901-e7625e10e008'),
(75, NULL, 'app', 'm171107_000000_assign_group_permissions', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '05e81069-f98d-4ba3-a631-263038f48ab9'),
(76, NULL, 'app', 'm171117_000001_templatecache_index_tune', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '29a051a1-f9d9-4a44-b161-1cf1dc0f93fa'),
(77, NULL, 'app', 'm171126_105927_disabled_plugins', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'a5140ed5-fe18-4d01-b04d-92cbb792d247'),
(78, NULL, 'app', 'm171130_214407_craftidtokens_table', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '1f7bd483-fa90-4d8a-a144-b9222f40607c'),
(79, NULL, 'app', 'm171202_004225_update_email_settings', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '990a89e4-de63-4f9d-bb28-d114e3196204'),
(80, NULL, 'app', 'm171204_000001_templatecache_index_tune_deux', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'd1419d7d-05b8-4ccd-be54-dd2b30068f88'),
(81, NULL, 'app', 'm171205_130908_remove_craftidtokens_refreshtoken_column', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'a625f6fa-636f-44dc-91ea-79d1713a52f4'),
(82, NULL, 'app', 'm171218_143135_longtext_query_column', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '8c7a7878-7f3f-45e4-b2a6-cf60bce16b85'),
(83, NULL, 'app', 'm171231_055546_environment_variables_to_aliases', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '5a01b5b5-92ff-47a1-9b0d-ce2ad89452e8'),
(84, NULL, 'app', 'm180113_153740_drop_users_archived_column', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '893f329b-d3d0-4944-9c10-d4139bd8cbe4'),
(85, NULL, 'app', 'm180122_213433_propagate_entries_setting', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2b61a0df-7319-43f2-8ed6-d64e33c7e63a'),
(86, NULL, 'app', 'm180124_230459_fix_propagate_entries_values', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'bd367d1e-d464-4cc9-9443-2e64a14c95ae'),
(87, NULL, 'app', 'm180128_235202_set_tag_slugs', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '9062c665-493c-4165-aa3d-778e1f963886'),
(88, NULL, 'app', 'm180202_185551_fix_focal_points', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'd9814b80-7a37-4688-ab33-f1a499df4292'),
(89, NULL, 'app', 'm180217_172123_tiny_ints', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'beeddd18-4a86-41b4-b4e6-a4662a7f2fac'),
(90, NULL, 'app', 'm180321_233505_small_ints', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '05f266db-13e6-4c00-a8a6-6e61a2c7ee2c'),
(91, NULL, 'app', 'm180328_115523_new_license_key_statuses', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '7de943e3-3364-44a4-a8b1-0800eb95f414'),
(92, NULL, 'app', 'm180404_182320_edition_changes', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '7733ae47-ebae-4689-ab41-76d585c6ff2b'),
(93, NULL, 'app', 'm180411_102218_fix_db_routes', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '9c7ea49e-c9f3-4394-9632-031731806935'),
(94, NULL, 'app', 'm180416_205628_resourcepaths_table', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '069b45da-a79f-40c2-a9ca-e1c0e1eaf822'),
(95, NULL, 'app', 'm180418_205713_widget_cleanup', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '8c2476e1-d2bd-4e54-b696-067fdbc187b5'),
(96, NULL, 'app', 'm180824_193422_case_sensitivity_fixes', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '07b52f56-89cb-423a-a449-1d5c8a3501e4'),
(97, NULL, 'app', 'm180901_151639_fix_matrixcontent_tables', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', 'b68b2c86-29a2-4543-b8ff-9f5d590c6f88'),
(98, NULL, 'app', 'm181112_203955_sequences_table', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '2019-02-12 06:57:59', '10e5c2a7-ea64-4636-8514-b2bffbe50ac9'),
(99, 2, 'plugin', 'Install', '2019-02-13 14:17:07', '2019-02-13 14:17:07', '2019-02-13 14:17:07', '36c8fe36-5b94-44fc-bff4-4a83279a3414'),
(100, NULL, 'app', 'm170630_161027_deprecation_line_nullable', '2019-02-20 11:13:53', '2019-02-20 11:13:53', '2019-02-20 11:13:53', '735c69f4-0677-4498-9db4-c014f9d7f410'),
(101, NULL, 'app', 'm180425_203349_searchable_fields', '2019-02-20 11:13:54', '2019-02-20 11:13:54', '2019-02-20 11:13:54', 'fba871c4-2f45-4206-be0d-61c3b956de49'),
(102, NULL, 'app', 'm180516_153000_uids_in_field_settings', '2019-02-20 11:13:55', '2019-02-20 11:13:55', '2019-02-20 11:13:55', 'aa1d28a7-3dca-4bc3-bfcb-555b0e80c149'),
(103, NULL, 'app', 'm180517_173000_user_photo_volume_to_uid', '2019-02-20 11:13:55', '2019-02-20 11:13:55', '2019-02-20 11:13:55', 'c12c2c4e-0f36-4df3-ae29-fb326e267b81'),
(104, NULL, 'app', 'm180518_173000_permissions_to_uid', '2019-02-20 11:13:55', '2019-02-20 11:13:55', '2019-02-20 11:13:55', '0ca3d1ca-dc08-4cb3-b8c3-3031fced56f2'),
(105, NULL, 'app', 'm180520_173000_matrix_context_to_uids', '2019-02-20 11:13:55', '2019-02-20 11:13:55', '2019-02-20 11:13:55', '80a0845f-96fc-4451-ba13-f235f415758f'),
(106, NULL, 'app', 'm180521_173000_initial_yml_and_snapshot', '2019-02-20 11:13:56', '2019-02-20 11:13:56', '2019-02-20 11:13:56', '8d6831be-20fc-49f3-b4a9-fc9b94dedb13'),
(107, NULL, 'app', 'm180731_162030_soft_delete_sites', '2019-02-20 11:13:56', '2019-02-20 11:13:56', '2019-02-20 11:13:56', '35f08010-90bf-4888-94ce-8ac9a3758505'),
(108, NULL, 'app', 'm180810_214427_soft_delete_field_layouts', '2019-02-20 11:13:56', '2019-02-20 11:13:56', '2019-02-20 11:13:56', '139450ad-53e3-4b82-a65e-6e1439070b42'),
(109, NULL, 'app', 'm180810_214439_soft_delete_elements', '2019-02-20 11:13:58', '2019-02-20 11:13:58', '2019-02-20 11:13:58', '69719c96-6941-474a-9467-775d720d792d'),
(110, NULL, 'app', 'm180904_112109_permission_changes', '2019-02-20 11:13:58', '2019-02-20 11:13:58', '2019-02-20 11:13:58', '95c63284-52af-46c9-a44e-3bfafea3a506'),
(111, NULL, 'app', 'm180910_142030_soft_delete_sitegroups', '2019-02-20 11:13:58', '2019-02-20 11:13:58', '2019-02-20 11:13:58', 'fed69e91-7e83-445d-8326-07435e62e794'),
(112, NULL, 'app', 'm181011_160000_soft_delete_asset_support', '2019-02-20 11:13:59', '2019-02-20 11:13:59', '2019-02-20 11:13:59', 'bfe373e5-6d1c-456d-982f-363ffa8e6d22'),
(113, NULL, 'app', 'm181016_183648_set_default_user_settings', '2019-02-20 11:13:59', '2019-02-20 11:13:59', '2019-02-20 11:13:59', '847fea3b-349a-4688-8e44-d618f2f57af1'),
(114, NULL, 'app', 'm181017_225222_system_config_settings', '2019-02-20 11:13:59', '2019-02-20 11:13:59', '2019-02-20 11:13:59', 'cdb498e9-124d-4201-adad-c8463c5e6899'),
(115, NULL, 'app', 'm181018_222343_drop_userpermissions_from_config', '2019-02-20 11:13:59', '2019-02-20 11:13:59', '2019-02-20 11:13:59', '9ec6bc62-b08f-4478-bf67-fc694c683c1c'),
(116, NULL, 'app', 'm181029_130000_add_transforms_routes_to_config', '2019-02-20 11:13:59', '2019-02-20 11:13:59', '2019-02-20 11:13:59', '2dfbeb2d-bed7-412b-96bd-bc6caca63771'),
(117, NULL, 'app', 'm181121_001712_cleanup_field_configs', '2019-02-20 11:13:59', '2019-02-20 11:13:59', '2019-02-20 11:13:59', 'f248f788-6327-42e0-96ef-c5b570b3e1ca'),
(118, NULL, 'app', 'm181128_193942_fix_project_config', '2019-02-20 11:13:59', '2019-02-20 11:13:59', '2019-02-20 11:13:59', 'bfd04cee-e7e1-4f8e-9e3d-11066b8bd94f'),
(119, NULL, 'app', 'm181130_143040_fix_schema_version', '2019-02-20 11:13:59', '2019-02-20 11:13:59', '2019-02-20 11:13:59', '04c2134f-310f-437b-9af3-fa7ea967b6df'),
(120, NULL, 'app', 'm181211_143040_fix_entry_type_uids', '2019-02-20 11:13:59', '2019-02-20 11:13:59', '2019-02-20 11:13:59', '0778ec4a-b20e-4d6a-855c-f6b4a81c8558'),
(121, NULL, 'app', 'm181213_102500_config_map_aliases', '2019-02-20 11:13:59', '2019-02-20 11:13:59', '2019-02-20 11:13:59', 'e63b0687-32ef-49b6-b6cb-77309a88206c'),
(122, NULL, 'app', 'm181217_153000_fix_structure_uids', '2019-02-20 11:13:59', '2019-02-20 11:13:59', '2019-02-20 11:13:59', '0dc2d348-51bd-464d-aeed-38ddbe9ea9bf'),
(123, NULL, 'app', 'm190104_152725_store_licensed_plugin_editions', '2019-02-20 11:14:00', '2019-02-20 11:14:00', '2019-02-20 11:14:00', 'b20068d0-b7b8-4b3f-89b0-04da169cc3fd'),
(124, NULL, 'app', 'm190108_110000_cleanup_project_config', '2019-02-20 11:14:00', '2019-02-20 11:14:00', '2019-02-20 11:14:00', 'e4e28ec2-56e2-4ac9-b6a3-282d589c5bae'),
(125, NULL, 'app', 'm190108_113000_asset_field_setting_change', '2019-02-20 11:14:00', '2019-02-20 11:14:00', '2019-02-20 11:14:00', '00b26468-1915-466c-987d-5c585a31cee2'),
(126, NULL, 'app', 'm190109_172845_fix_colspan', '2019-02-20 11:14:00', '2019-02-20 11:14:00', '2019-02-20 11:14:00', '6bf8f174-e51a-4ec0-90c1-ebdbba4721a2'),
(127, NULL, 'app', 'm190110_150000_prune_nonexisting_sites', '2019-02-20 11:14:00', '2019-02-20 11:14:00', '2019-02-20 11:14:00', 'c055f3fc-2baa-47b1-a09d-513ac7a06d22'),
(128, NULL, 'app', 'm190110_214819_soft_delete_volumes', '2019-02-20 11:14:00', '2019-02-20 11:14:00', '2019-02-20 11:14:00', '3867f143-2379-433b-87cb-5f384968cf74'),
(129, NULL, 'app', 'm190112_124737_fix_user_settings', '2019-02-20 11:14:00', '2019-02-20 11:14:00', '2019-02-20 11:14:00', 'c8154940-20a6-4190-b769-79645f407984'),
(130, NULL, 'app', 'm190112_131225_fix_field_layouts', '2019-02-20 11:14:00', '2019-02-20 11:14:00', '2019-02-20 11:14:00', '4f9603aa-10dd-43ff-9026-af25411260d0'),
(131, NULL, 'app', 'm190112_201010_more_soft_deletes', '2019-02-20 11:14:03', '2019-02-20 11:14:03', '2019-02-20 11:14:03', '7599c4b2-e230-4370-81f2-e7c1347e0c49'),
(132, NULL, 'app', 'm190114_143000_more_asset_field_setting_changes', '2019-02-20 11:14:03', '2019-02-20 11:14:03', '2019-02-20 11:14:03', '41d59b2a-179e-4a8a-99c9-be612ad5b242'),
(133, NULL, 'app', 'm190121_120000_rich_text_config_setting', '2019-02-20 11:14:03', '2019-02-20 11:14:03', '2019-02-20 11:14:03', '09f47ac2-b38a-431f-b169-cec954c41a85'),
(134, NULL, 'app', 'm190125_191628_fix_email_transport_password', '2019-02-20 11:14:03', '2019-02-20 11:14:03', '2019-02-20 11:14:03', 'babdfd31-6f33-4095-80f7-ec5c2f263a68'),
(135, NULL, 'app', 'm190128_181422_cleanup_volume_folders', '2019-02-20 11:14:03', '2019-02-20 11:14:03', '2019-02-20 11:14:03', '3afdfe3c-8858-4934-aaed-4ec203758c99'),
(136, NULL, 'app', 'm190205_140000_fix_asset_soft_delete_index', '2019-02-20 11:14:03', '2019-02-20 11:14:03', '2019-02-20 11:14:03', '5d0ac97a-1f81-407f-89fa-3e1f8d4b0067'),
(137, NULL, 'app', 'm190208_140000_reset_project_config_mapping', '2019-02-20 11:14:03', '2019-02-20 11:14:03', '2019-02-20 11:14:03', 'd5b5e95c-26e6-4e60-9124-9d8e60fc224e'),
(138, NULL, 'app', 'm190218_143000_element_index_settings_uid', '2019-02-20 11:14:03', '2019-02-20 11:14:03', '2019-02-20 11:14:03', '5ed92204-dda8-4a87-9f05-cdd19a6b20db'),
(139, 4, 'plugin', 'm180430_204710_remove_old_plugins', '2019-02-20 11:15:38', '2019-02-20 11:15:38', '2019-02-20 11:15:38', '189c363f-8540-48c3-b293-c688f91a2ebb'),
(140, 4, 'plugin', 'Install', '2019-02-20 11:15:38', '2019-02-20 11:15:38', '2019-02-20 11:15:38', '1288ac86-2937-4254-a699-8a2904fa31f6'),
(141, 4, 'plugin', 'm181101_110000_ids_in_settings_to_uids', '2019-02-20 11:15:38', '2019-02-20 11:15:38', '2019-02-20 11:15:38', 'c606894e-747a-4b0d-bfcc-bed1a0bf753e');

-- --------------------------------------------------------

--
-- Table structure for table `olivemenus`
--

CREATE TABLE `olivemenus` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `handle` varchar(255) NOT NULL DEFAULT '',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `olivemenus`
--

INSERT INTO `olivemenus` (`id`, `name`, `handle`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'Footer Menu', 'footerMenu', '2019-02-13 14:36:25', '2019-02-13 14:36:25', '2e85703b-3bd4-4219-bdb4-4ae6e8ab717c'),
(3, 'Footer Menu English', 'footerMenuEnglish', '2019-02-18 08:31:09', '2019-02-18 08:31:09', '1a190634-8c56-4162-adfe-31f0cde0999d');

-- --------------------------------------------------------

--
-- Table structure for table `olivemenus_items`
--

CREATE TABLE `olivemenus_items` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `item_order` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `entry_id` int(11) DEFAULT NULL,
  `custom_url` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `class_parent` varchar(255) DEFAULT NULL,
  `data_json` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `olivemenus_items`
--

INSERT INTO `olivemenus_items` (`id`, `menu_id`, `parent_id`, `item_order`, `name`, `entry_id`, `custom_url`, `class`, `class_parent`, `data_json`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 0, 0, 'Impressum', 5, '', '', '', '', '2019-02-13 14:38:26', '2019-02-13 14:38:26', 'fcccb959-94eb-4a61-b155-15ac9b14d24a'),
(2, 1, 0, 1, 'Datenschutzerklarung', 6, '', '', '', '', '2019-02-13 14:38:26', '2019-02-13 14:38:26', '7b612042-bf68-4808-ad90-7933742ee4d3'),
(3, 1, 0, 2, 'Agbs', 7, '', '', '', '', '2019-02-13 14:38:26', '2019-02-13 14:38:26', '4694b014-ea4f-4db7-bc5c-3e69e78a7060'),
(4, 1, 0, 3, 'Cookies', 8, '', '', '', '', '2019-02-13 14:38:26', '2019-02-13 14:38:26', 'b8254806-4226-4184-a48d-2514db491d72'),
(5, 3, 0, 0, 'impressum', NULL, 'http://craftcms.phpdevtest.com/en/impressum-english', '', '', '', '2019-02-18 08:38:01', '2019-02-20 10:04:53', 'b42aba9b-410e-4349-bc8a-c2db9a8d9ca8'),
(6, 3, 0, 1, 'Data Protection', NULL, 'http://craftcms.phpdevtest.com/en/data-protection', '', '', '', '2019-02-18 08:38:01', '2019-02-20 10:04:53', '6bd1ae96-49aa-460f-882e-aed43a48a4db'),
(7, 3, 0, 2, 'Agbs', NULL, 'http://craftcms.phpdevtest.com/en/agbs-english', '', '', '', '2019-02-18 08:38:01', '2019-02-20 10:04:53', '08d27f51-e12f-43f7-b146-61a74c493e71'),
(8, 3, 0, 3, 'Cookies', NULL, 'http://craftcms.phpdevtest.com/en/cookies-english', '', '', '', '2019-02-18 08:38:01', '2019-02-20 10:04:53', 'dc7ce14d-0e47-49a0-baee-017ff0e9a425');

-- --------------------------------------------------------

--
-- Table structure for table `plugins`
--

CREATE TABLE `plugins` (
  `id` int(11) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `schemaVersion` varchar(255) NOT NULL,
  `licenseKeyStatus` enum('valid','invalid','mismatched','astray','unknown') NOT NULL DEFAULT 'unknown',
  `licensedEdition` varchar(255) DEFAULT NULL,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plugins`
--

INSERT INTO `plugins` (`id`, `handle`, `version`, `schemaVersion`, `licenseKeyStatus`, `licensedEdition`, `installDate`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(2, 'olivemenus', '1.0.8', '1.0.0', 'unknown', NULL, '2019-02-13 14:17:06', '2019-02-13 14:17:06', '2019-02-27 11:24:21', 'dbd504c5-c3dd-4815-9d9a-67a439c9294d'),
(3, 'cookie-consent-banner', '1.1.2', '1.0.0', 'unknown', NULL, '2019-02-13 14:17:11', '2019-02-13 14:17:11', '2019-02-27 11:24:21', '348e1825-ee0b-4f2b-ad40-a195b31f6ec6'),
(4, 'redactor', '2.3.1', '2.2.1', 'unknown', NULL, '2019-02-20 11:15:38', '2019-02-20 11:15:38', '2019-02-27 11:24:21', '93dd2d29-6e1a-4efa-90de-5bd3557bd1a8'),
(5, 'contact-form', '2.2.3', '1.0.0', 'unknown', NULL, '2019-02-26 08:56:48', '2019-02-26 08:56:48', '2019-02-27 11:24:21', 'a53e14dd-edf2-4e35-b47f-ece425fe6c02'),
(6, 'fortrabbit-object-storage', '1.0.0', '1.0.0', 'unknown', NULL, '2019-02-26 14:31:31', '2019-02-26 14:31:31', '2019-02-27 11:24:21', 'f64d8d68-9e4c-48d2-92fd-b24e327c2ccb');

-- --------------------------------------------------------

--
-- Table structure for table `presale_code`
--

CREATE TABLE `presale_code` (
  `id` int(11) NOT NULL,
  `code` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `used_count` int(11) NOT NULL,
  `dateCreated` datetime DEFAULT NULL,
  `dateUpdated` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `presale_code`
--

INSERT INTO `presale_code` (`id`, `code`, `status`, `used_count`, `dateCreated`, `dateUpdated`) VALUES
(1, 'ABC', 1, 2, '2019-02-12 00:00:00', '2019-02-27 10:01:23'),
(2, 'DEF', 1, 0, '2019-02-12 00:00:00', '2019-02-26 14:38:01');

-- --------------------------------------------------------

--
-- Table structure for table `queue`
--

CREATE TABLE `queue` (
  `id` int(11) NOT NULL,
  `job` longblob NOT NULL,
  `description` text,
  `timePushed` int(11) NOT NULL,
  `ttr` int(11) NOT NULL,
  `delay` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) UNSIGNED NOT NULL DEFAULT '1024',
  `dateReserved` datetime DEFAULT NULL,
  `timeUpdated` int(11) DEFAULT NULL,
  `progress` smallint(6) NOT NULL DEFAULT '0',
  `attempt` int(11) DEFAULT NULL,
  `fail` tinyint(1) DEFAULT '0',
  `dateFailed` datetime DEFAULT NULL,
  `error` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `relations`
--

CREATE TABLE `relations` (
  `id` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceSiteId` int(11) DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `relations`
--

INSERT INTO `relations` (`id`, `fieldId`, `sourceId`, `sourceSiteId`, `targetId`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(53, 2, 14, NULL, 3, 1, '2019-02-20 12:16:29', '2019-02-20 12:16:29', '4d27c161-57ea-4753-815c-c456f75a15b2'),
(54, 3, 14, NULL, 4, 1, '2019-02-20 12:16:29', '2019-02-20 12:16:29', '2837e5c6-cbc3-4b49-8a08-aa4d2ba00839'),
(119, 2, 2, NULL, 3, 1, '2019-02-27 14:57:50', '2019-02-27 14:57:50', '9852f647-3706-47c5-97cf-c8d880eb9a34'),
(120, 3, 2, NULL, 28, 1, '2019-02-27 14:57:50', '2019-02-27 14:57:50', '4e0f9c16-cd7a-4e7e-b527-e691b8c8cff0'),
(121, 2, 27, NULL, 3, 1, '2019-02-27 15:00:27', '2019-02-27 15:00:27', '35f8a380-b1c9-42f4-94e1-9e742da7b2ba'),
(122, 3, 27, NULL, 4, 1, '2019-02-27 15:00:27', '2019-02-27 15:00:27', 'd8cd9755-2625-47be-9ca0-26f1d1ec5aba');

-- --------------------------------------------------------

--
-- Table structure for table `resourcepaths`
--

CREATE TABLE `resourcepaths` (
  `hash` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `resourcepaths`
--

INSERT INTO `resourcepaths` (`hash`, `path`) VALUES
('13726b14', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\ckeditor\\src\\assets\\field\\dist'),
('15a3862e', '@craft/web/assets/recententries/dist'),
('16ccee9f', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\picturefill'),
('1ba40235', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\generalsettings\\dist'),
('1d25abb1', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\fabric'),
('1d587f17', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\clearcaches\\dist'),
('1dd38c54', '@lib'),
('1e903ea7', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\login\\dist'),
('1ef0bd28', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\element-resize-detector'),
('1f1c03bb', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\clearcaches\\dist'),
('21198f3', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\cp\\dist'),
('24d9d6e9', 'C:\\wamp64\\www\\craftcms\\vendor\\yiisoft\\yii2\\assets'),
('260f09b5', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\fields\\dist'),
('298304be', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\dashboard\\dist'),
('2a29ea48', 'C:\\wamp64\\www\\craftcms\\vendor\\yiisoft\\yii2-debug\\src\\assets'),
('2a321356', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\jquery-ui'),
('2a821f18', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\timepicker'),
('2b19c440', '@craft/web/assets/updateswidget/dist'),
('2db2467', '@lib/timepicker'),
('2f2b025c', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\updater\\dist'),
('31671dd1', '@craft/web/assets/cp/dist'),
('37483898', '@lib/selectize'),
('380e092d', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\jquery.payment'),
('3d247c12', '@lib/xregexp'),
('3f8bf663', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\garnishjs'),
('4406c70f', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\d3'),
('481aad30', '@app/web/assets/dashboard/dist'),
('4adcc19', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\deprecationerrors\\dist'),
('4b68f6f5', '@lib/fabric'),
('4bad3f18', '@craft/web/assets/fields/dist'),
('533880c6', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\jquery-ui'),
('53872f9d', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\fileupload'),
('53888c88', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\timepicker'),
('5395d4cc', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\login\\dist'),
('5e2e22cb', '@lib/garnishjs'),
('6336151c', '@lib/picturefill'),
('63bf8d6d', '@craft/web/assets/tablesettings/dist'),
('649229de', '@lib/jquery-touch-events'),
('66142357', '@lib/prismjs'),
('66c0508c', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\dashboard\\dist'),
('6b0ae3de', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\fields\\dist'),
('6b1e8f9e', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\jquery-touch-events'),
('6b95251b', '@craft/web/assets/utilities/dist'),
('6cf0e24d', '@bower/bootstrap/dist'),
('6d93354d', '@lib/d3'),
('76130733', 'C:\\wamp64\\www\\craftcms\\vendor\\adigital\\cookie-consent-banner\\src\\assetbundles\\cookieconsentbanner\\dist'),
('7b8c6bfe', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\xregexp'),
('7bde14e2', '@lib/fileupload'),
('7dc0bff3', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\selectize'),
('7faf2791', 'C:\\wamp64\\www\\craftcms\\vendor\\olivestudio\\craft-olivemenus\\src\\assetbundles\\olivemenus\\dist'),
('8064cfdf', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\searchindexes\\dist'),
('8454079e', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\plugins\\dist'),
('84b38584', '@craft/web/assets/login/dist'),
('86107b32', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\plugins\\dist'),
('875c8dd1', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\feed\\dist'),
('8b17e37c', '@app/web/assets/cp/dist'),
('8bb11d2e', '@vendor/craftcms/redactor/lib/redactor'),
('8cb53249', '@craft/web/assets/craftsupport/dist'),
('8da2d023', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\pluginstore\\dist'),
('8f29f602', '@app/web/assets/login/dist'),
('91ee3922', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\utilities\\dist'),
('925c6be', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\editentry\\dist'),
('94ebffb2', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\picturefill'),
('95458011', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\assetindexes\\dist'),
('965ac4f1', '@app/web/assets/recententries/dist'),
('98c05529', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\craftsupport\\dist'),
('9c96c59b', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\matrixsettings\\dist'),
('9cd7ac05', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\element-resize-detector'),
('9ed2b937', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\matrixsettings\\dist'),
('9ff49d79', '@lib'),
('a29b2596', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\prismjs'),
('a6722a5e', '@app/web/assets/craftsupport/dist'),
('a7dc6dd3', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\velocity'),
('a7de5c81', '@bower/jquery/dist'),
('a8b24b07', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\craftsupport\\dist'),
('ad1e023', '@app/web/assets/plugins/dist'),
('ae04f4f2', '@craft/web/assets/plugins/dist'),
('af1bf825', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\findreplace\\dist'),
('af2b76f0', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\updateswidget\\dist'),
('b0ba4543', '@lib/jquery-ui'),
('b393e4f7', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\tablesettings\\dist'),
('b3a00f0d', '@craft/web/assets/feed/dist'),
('b3b9cf75', '@craft/web/assets/clearcaches/dist'),
('b42fe95f', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\fabric'),
('b5fb9a12', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\cp\\dist'),
('b61ba12', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\editentry\\dist'),
('ba291800', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\jquery.payment'),
('bdace74e', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\garnishjs'),
('c00c826e', '@lib/element-resize-detector'),
('c58fefb4', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\recententries\\dist'),
('c7cb9318', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\recententries\\dist'),
('ca5967ba', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\feed\\dist'),
('cae329c6', '@craft/web/assets/dashboard/dist'),
('cdbd6400', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\edituser\\dist'),
('d4cafd1d', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\selectize'),
('d529caaa', '@craft/web/assets/matrixsettings/dist'),
('d77eb19b', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\dbbackup\\dist'),
('d7b6e863', '@lib'),
('db91b606', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\prismjs'),
('dd3f17d3', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\installer\\dist'),
('ded4cf11', '@bower/jquery/dist'),
('ded6fe43', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\velocity'),
('df83a013', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\redactor\\src\\assets\\field\\dist'),
('e234710e', '@lib/jquery.payment'),
('e2913c4f', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\routes\\dist'),
('e4d44b6f', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\edituser\\dist'),
('e78d3fdd', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\tablesettings\\dist'),
('e9399eb3', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\jquery-touch-events'),
('e9ccd7d2', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\pluginstore\\dist'),
('ea45ebc6', '@craft/web/assets/editentry/dist'),
('ec9b0f92', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\utilities\\dist'),
('ed0c85e1', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\d3'),
('ed28b242', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\sites\\dist'),
('ef9c299b', '@bower/jquery/dist'),
('f20ca8c2', '@app/web/assets/feed/dist'),
('f6eb80ae', '@craft/redactor/assets/field/dist'),
('f9ab7ad3', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\xregexp'),
('fa8d6d73', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\lib\\fileupload'),
('fb35adda', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\cms\\src\\web\\assets\\updateswidget\\dist'),
('fc05e024', '@adigital/cookieconsentbanner/assetbundles/cookieconsentbanner/dist'),
('fe13dee6', '@lib/velocity'),
('fe91146c', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\redactor\\lib\\redactor'),
('ffb30e58', 'C:\\wamp64\\www\\craftcms\\vendor\\craftcms\\ckeditor\\lib\\ckeditor\\dist');

-- --------------------------------------------------------

--
-- Table structure for table `searchindex`
--

CREATE TABLE `searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `keywords` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `searchindex`
--

INSERT INTO `searchindex` (`elementId`, `attribute`, `fieldId`, `siteId`, `keywords`) VALUES
(1, 'username', 0, 1, ' admin '),
(1, 'firstname', 0, 1, ''),
(1, 'lastname', 0, 1, ''),
(1, 'fullname', 0, 1, ''),
(1, 'email', 0, 1, ' abhinglajia cygnet infotech com '),
(1, 'slug', 0, 1, ''),
(2, 'slug', 0, 1, ' homepage '),
(2, 'title', 0, 1, ' homepage '),
(2, 'field', 2, 1, ' hp grafik '),
(2, 'field', 3, 1, ' hp_grafik mobile '),
(2, 'field', 1, 1, ' vorstellungen harry potter und das venvunschene kind ist ein theaterstueck in zwei teilen wir empfehlen das theatererlebnis am glelchen tag oder an zwei aufeinander folgenden tagen zu sehen mittwoch donnerstag freitag samstag sonntag teil eins 14 30 uhr teil eins 14 30 uhr teil eins 14 30 uhr teil zwei 19 30 uhr teil zwei 19 30 uhr teil zwei 19 30 uhr teil zwei 19 30 uhr teil zwei 19 30 uhr tickets kaufen waehlen sie hier die art der tickets die sie erwerben mochten und sie gelangen direkt zur ticketauswahl empfohlen kombi teil eins und teil zwei aufeinander folgend schauen empfohlen kombi tickets kaufen '),
(3, 'filename', 0, 1, ' hp_grafik png '),
(3, 'extension', 0, 1, ' png '),
(3, 'kind', 0, 1, ' image '),
(3, 'slug', 0, 1, ''),
(3, 'title', 0, 1, ' hp grafik '),
(4, 'filename', 0, 1, ' hp_grafik mobile png '),
(4, 'extension', 0, 1, ' png '),
(4, 'kind', 0, 1, ' image '),
(4, 'slug', 0, 1, ''),
(4, 'title', 0, 1, ' hp grafik mobile '),
(5, 'field', 1, 1, ' eigentumer und betreiber der webseite ist aka promotions ltd 115 shaftesbury avenue cambridge circus london wc2h 8af e mail webteam akauk com telefon 44 0 20 7836 4747 produzent fur harry potter und das verwunschene kind in deutschland ist mehr bb enterainment gmbh erkrather str 30 d 40233 dusseldorf geschaftsfuhrer maik klokow ralf kokemuller gunter irmler amtsgericht dusseldorf hrb 57980 webmaster mehr entertainment de fon 49 0 211 73 44 151 fax 49 0 211 73 44 155 ticketing deutsche eintrittskarten tks gmbh www eintrittskarten de banksstr 28 d 20097 hamburg buro dusseldorf erlrather str 30 d 40233 dusseldorf geschaftsfuhrer kai ricke amtsgericht hamburg hrb 121731 ust idnr de 812 702 791 webmaster mehr entertainment defon 49 0 211 73 44 151 fax 49 0 211 73 44 155 '),
(5, 'slug', 0, 1, ' impressum '),
(5, 'title', 0, 1, ' impressum '),
(6, 'field', 1, 1, ' lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 1 aka1 1 lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 2 use of the site2 1 lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 2 2 lorem ipsum is simply dummy text of the printing and typesetting industry a lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum b lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum c lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum d lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 2 3 lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 2 4 lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 2 5 lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 2 6 lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum '),
(6, 'slug', 0, 1, ' datenschutzerklarung '),
(6, 'title', 0, 1, ' datenschutzerklarung '),
(7, 'field', 1, 1, ' lorem ipsum dolor sit amet consectetur adipiscing elit nunc accumsan ornare euismod vestibulum tincidunt massa at augue faucibus finibus duis iaculis sapien quis orci commodo id commodo tortor volutpat etiam luctus est sapien vel tempor diam cursus et pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas pellentesque ornare a ipsum at posuere maecenas sollicitudin leo id ipsum pellentesque tincidunt sed velit quam gravida at ornare sed gravida a nisl curabitur gravida fringilla erat a venenatis nam posuere sit amet lorem ut blandit praesent ligula lectus imperdiet eu leo id aliquet commodo nisl ut venenatis sem commodo auctor gravida nulla facilisi integer congue at dui nec venenatis suspendisse facilisis enim nec porttitor euismod aliquam ornare ante pretium feugiat elementum dui quam suscipit ipsum a sagittis est libero id diam nunc viverra ante vitae maximus placerat risus dolor luctus elit ac aliquet purus erat vel urna praesent varius urna purus sit amet semper turpis scelerisque ac morbi aliquet nibh mauris vel lobortis quam iaculis ut aliquam rutrum odio ut urna consectetur fringilla suspendisse sit amet urna vitae augue sodales pharetra etiam vel felis elit etiam at finibus velit condimentum fringilla nibh morbi dignissim sollicitudin diam ut elementum nisi auctor vitae etiam egestas dignissim ultricies pellentesque vel arcu sed lectus feugiat iaculis et ac tortor in sit amet maximus arcu et vestibulum neque quisque sollicitudin elit neque lacinia convallis leo lacinia quis sed semper elit orci vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae curabitur at sagittis nibh a egestas ipsum ut tellus diam dapibus nec purus a porttitor iaculis diam sed sagittis mauris eu lacinia ultricies pellentesque volutpat in odio pellentesque facilisis aenean pretium massa ac nisl blandit aliquet imperdiet mi mollis integer erat lacus finibus et diam sed vestibulum lobortis quam sed porttitor magna in malesuada accumsan turpis ipsum placerat lectus nec interdum tortor leo ut dui nunc commodo lorem ut fermentum scelerisque aenean id leo malesuada dapibus diam id convallis libero fusce auctor tortor id risus ultrices tempus integer pretium gravida leo nec iaculis velit commodo ut suspendisse posuere ipsum non magna volutpat sit amet luctus massa scelerisque donec dictum tincidunt massa ut gravida integer fermentum felis sapien nec rhoncus mi consequat in mauris eu dictum risus nulla tincidunt a felis nec rhoncus aliquam placerat nisi augue sit amet tincidunt tortor dictum eu integer hendrerit rutrum dui eget pellentesque risus duis et molestie risus cras lacinia vel dui quis imperdiet integer interdum blandit ligula ac congue donec semper euismod rhoncus '),
(7, 'slug', 0, 1, ' agbs '),
(7, 'title', 0, 1, ' agbs '),
(8, 'field', 1, 1, ' lorem ipsum dolor sit amet consectetur adipiscing elit nunc accumsan ornare euismod vestibulum tincidunt massa at augue faucibus finibus duis iaculis sapien quis orci commodo id commodo tortor volutpat etiam luctus est sapien vel tempor diam cursus et pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas pellentesque ornare a ipsum at posuere maecenas sollicitudin leo id ipsum pellentesque tincidunt sed velit quam gravida at ornare sed gravida a nisl curabitur gravida fringilla erat a venenatis nam posuere sit amet lorem ut blandit praesent ligula lectus imperdiet eu leo id aliquet commodo nisl ut venenatis sem commodo auctor gravida nulla facilisi integer congue at dui nec venenatis suspendisse facilisis enim nec porttitor euismod aliquam ornare ante pretium feugiat elementum dui quam suscipit ipsum a sagittis est libero id diam nunc viverra ante vitae maximus placerat risus dolor luctus elit ac aliquet purus erat vel urna praesent varius urna purus sit amet semper turpis scelerisque ac morbi aliquet nibh mauris vel lobortis quam iaculis ut aliquam rutrum odio ut urna consectetur fringilla suspendisse sit amet urna vitae augue sodales pharetra etiam vel felis elit etiam at finibus velit condimentum fringilla nibh morbi dignissim sollicitudin diam ut elementum nisi auctor vitae etiam egestas dignissim ultricies pellentesque vel arcu sed lectus feugiat iaculis et ac tortor in sit amet maximus arcu et vestibulum neque quisque sollicitudin elit neque lacinia convallis leo lacinia quis sed semper elit orci vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae curabitur at sagittis nibh a egestas ipsum ut tellus diam dapibus nec purus a porttitor iaculis diam sed sagittis mauris eu lacinia ultricies pellentesque volutpat in odio pellentesque facilisis aenean pretium massa ac nisl blandit aliquet imperdiet mi mollis integer erat lacus finibus et diam sed vestibulum lobortis quam sed porttitor magna in malesuada accumsan turpis ipsum placerat lectus nec interdum tortor leo ut dui nunc commodo lorem ut fermentum scelerisque aenean id leo malesuada dapibus diam id convallis libero fusce auctor tortor id risus ultrices tempus integer pretium gravida leo nec iaculis velit commodo ut suspendisse posuere ipsum non magna volutpat sit amet luctus massa scelerisque donec dictum tincidunt massa ut gravida integer fermentum felis sapien nec rhoncus mi consequat in mauris eu dictum risus nulla tincidunt a felis nec rhoncus aliquam placerat nisi augue sit amet tincidunt tortor dictum eu integer hendrerit rutrum dui eget pellentesque risus duis et molestie risus cras lacinia vel dui quis imperdiet integer interdum blandit ligula ac congue donec semper euismod rhoncus '),
(8, 'slug', 0, 1, ' cookies '),
(8, 'title', 0, 1, ' cookies '),
(9, 'field', 1, 1, ' harry potter publishing and theatrical rights j k rowling '),
(9, 'slug', 0, 1, ' footer copyright text '),
(9, 'title', 0, 1, ' footer copyright text '),
(10, 'field', 1, 1, ' webseite wird gemanaged von aka '),
(10, 'slug', 0, 1, ' website managed by text '),
(10, 'title', 0, 1, ' website managed by text '),
(11, 'field', 1, 1, ' harry portter characters name and related indicia are trademarks of and warner bros ent alle rechte vorberhalten '),
(11, 'slug', 0, 1, ' trademarks text '),
(11, 'title', 0, 1, ' trademarks text '),
(12, 'field', 1, 1, ' es glbt noch letzte elnzelplatze fur vorstellungen irn januar '),
(12, 'slug', 0, 1, ' verfuegbare tickets '),
(12, 'title', 0, 1, ' verfuegbare tickets '),
(13, 'field', 1, 1, ' mehr theater am grobmarkt hamburg '),
(13, 'slug', 0, 1, ' saalplan '),
(13, 'title', 0, 1, ' saalplan '),
(3, 'filename', 0, 2, ' hp_grafik png '),
(3, 'extension', 0, 2, ' png '),
(3, 'kind', 0, 2, ' image '),
(3, 'slug', 0, 2, ''),
(3, 'title', 0, 2, ' hp grafik '),
(4, 'filename', 0, 2, ' hp_grafik mobile png '),
(4, 'extension', 0, 2, ' png '),
(4, 'kind', 0, 2, ' image '),
(4, 'slug', 0, 2, ''),
(4, 'title', 0, 2, ' hp grafik mobile '),
(5, 'field', 1, 2, ' eigentumer und betreiber der webseite ist aka promotions ltd 115 shaftesbury avenue cambridge circus london wc2h 8af e mail webteam akauk com telefon 44 0 20 7836 4747 produzent fur harry potter und das verwunschene kind in deutschland ist mehr bb enterainment gmbh erkrather str 30 d 40233 dusseldorf geschaftsfuhrer maik klokow ralf kokemuller gunter irmler amtsgericht dusseldorf hrb 57980 webmaster mehr entertainment de fon 49 0 211 73 44 151 fax 49 0 211 73 44 155 ticketing deutsche eintrittskarten tks gmbh www eintrittskarten de banksstr 28 d 20097 hamburg buro dusseldorf erlrather str 30 d 40233 dusseldorf geschaftsfuhrer kai ricke amtsgericht hamburg hrb 121731 ust idnr de 812 702 791 webmaster mehr entertainment defon 49 0 211 73 44 151 fax 49 0 211 73 44 155 '),
(5, 'slug', 0, 2, ' impressum '),
(5, 'title', 0, 2, ' impressum en '),
(6, 'field', 1, 2, ' lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 1 aka1 1 lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 2 use of the site2 1 lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 2 2 lorem ipsum is simply dummy text of the printing and typesetting industry a lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum b lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum c lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum d lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 2 3 lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 2 4 lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 2 5 lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 2 6 lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum '),
(6, 'slug', 0, 2, ' datenschutzerklarung '),
(6, 'title', 0, 2, ' datenschutzerklarung '),
(7, 'field', 1, 2, ' lorem ipsum dolor sit amet consectetur adipiscing elit nunc accumsan ornare euismod vestibulum tincidunt massa at augue faucibus finibus duis iaculis sapien quis orci commodo id commodo tortor volutpat etiam luctus est sapien vel tempor diam cursus et pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas pellentesque ornare a ipsum at posuere maecenas sollicitudin leo id ipsum pellentesque tincidunt sed velit quam gravida at ornare sed gravida a nisl curabitur gravida fringilla erat a venenatis nam posuere sit amet lorem ut blandit praesent ligula lectus imperdiet eu leo id aliquet commodo nisl ut venenatis sem commodo auctor gravida nulla facilisi integer congue at dui nec venenatis suspendisse facilisis enim nec porttitor euismod aliquam ornare ante pretium feugiat elementum dui quam suscipit ipsum a sagittis est libero id diam nunc viverra ante vitae maximus placerat risus dolor luctus elit ac aliquet purus erat vel urna praesent varius urna purus sit amet semper turpis scelerisque ac morbi aliquet nibh mauris vel lobortis quam iaculis ut aliquam rutrum odio ut urna consectetur fringilla suspendisse sit amet urna vitae augue sodales pharetra etiam vel felis elit etiam at finibus velit condimentum fringilla nibh morbi dignissim sollicitudin diam ut elementum nisi auctor vitae etiam egestas dignissim ultricies pellentesque vel arcu sed lectus feugiat iaculis et ac tortor in sit amet maximus arcu et vestibulum neque quisque sollicitudin elit neque lacinia convallis leo lacinia quis sed semper elit orci vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae curabitur at sagittis nibh a egestas ipsum ut tellus diam dapibus nec purus a porttitor iaculis diam sed sagittis mauris eu lacinia ultricies pellentesque volutpat in odio pellentesque facilisis aenean pretium massa ac nisl blandit aliquet imperdiet mi mollis integer erat lacus finibus et diam sed vestibulum lobortis quam sed porttitor magna in malesuada accumsan turpis ipsum placerat lectus nec interdum tortor leo ut dui nunc commodo lorem ut fermentum scelerisque aenean id leo malesuada dapibus diam id convallis libero fusce auctor tortor id risus ultrices tempus integer pretium gravida leo nec iaculis velit commodo ut suspendisse posuere ipsum non magna volutpat sit amet luctus massa scelerisque donec dictum tincidunt massa ut gravida integer fermentum felis sapien nec rhoncus mi consequat in mauris eu dictum risus nulla tincidunt a felis nec rhoncus aliquam placerat nisi augue sit amet tincidunt tortor dictum eu integer hendrerit rutrum dui eget pellentesque risus duis et molestie risus cras lacinia vel dui quis imperdiet integer interdum blandit ligula ac congue donec semper euismod rhoncus '),
(7, 'slug', 0, 2, ' agbs '),
(7, 'title', 0, 2, ' agbs '),
(8, 'field', 1, 2, ' lorem ipsum dolor sit amet consectetur adipiscing elit nunc accumsan ornare euismod vestibulum tincidunt massa at augue faucibus finibus duis iaculis sapien quis orci commodo id commodo tortor volutpat etiam luctus est sapien vel tempor diam cursus et pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas pellentesque ornare a ipsum at posuere maecenas sollicitudin leo id ipsum pellentesque tincidunt sed velit quam gravida at ornare sed gravida a nisl curabitur gravida fringilla erat a venenatis nam posuere sit amet lorem ut blandit praesent ligula lectus imperdiet eu leo id aliquet commodo nisl ut venenatis sem commodo auctor gravida nulla facilisi integer congue at dui nec venenatis suspendisse facilisis enim nec porttitor euismod aliquam ornare ante pretium feugiat elementum dui quam suscipit ipsum a sagittis est libero id diam nunc viverra ante vitae maximus placerat risus dolor luctus elit ac aliquet purus erat vel urna praesent varius urna purus sit amet semper turpis scelerisque ac morbi aliquet nibh mauris vel lobortis quam iaculis ut aliquam rutrum odio ut urna consectetur fringilla suspendisse sit amet urna vitae augue sodales pharetra etiam vel felis elit etiam at finibus velit condimentum fringilla nibh morbi dignissim sollicitudin diam ut elementum nisi auctor vitae etiam egestas dignissim ultricies pellentesque vel arcu sed lectus feugiat iaculis et ac tortor in sit amet maximus arcu et vestibulum neque quisque sollicitudin elit neque lacinia convallis leo lacinia quis sed semper elit orci vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae curabitur at sagittis nibh a egestas ipsum ut tellus diam dapibus nec purus a porttitor iaculis diam sed sagittis mauris eu lacinia ultricies pellentesque volutpat in odio pellentesque facilisis aenean pretium massa ac nisl blandit aliquet imperdiet mi mollis integer erat lacus finibus et diam sed vestibulum lobortis quam sed porttitor magna in malesuada accumsan turpis ipsum placerat lectus nec interdum tortor leo ut dui nunc commodo lorem ut fermentum scelerisque aenean id leo malesuada dapibus diam id convallis libero fusce auctor tortor id risus ultrices tempus integer pretium gravida leo nec iaculis velit commodo ut suspendisse posuere ipsum non magna volutpat sit amet luctus massa scelerisque donec dictum tincidunt massa ut gravida integer fermentum felis sapien nec rhoncus mi consequat in mauris eu dictum risus nulla tincidunt a felis nec rhoncus aliquam placerat nisi augue sit amet tincidunt tortor dictum eu integer hendrerit rutrum dui eget pellentesque risus duis et molestie risus cras lacinia vel dui quis imperdiet integer interdum blandit ligula ac congue donec semper euismod rhoncus '),
(8, 'slug', 0, 2, ' cookies '),
(8, 'title', 0, 2, ' cookies '),
(9, 'field', 1, 2, ' harry potter publishing and theatrical rights j k rowling '),
(9, 'slug', 0, 2, ' footer copyright text '),
(9, 'title', 0, 2, ' footer copyright text '),
(10, 'field', 1, 2, ' webseite wird gemanaged von aka '),
(10, 'slug', 0, 2, ' website managed by text '),
(10, 'title', 0, 2, ' website managed by text '),
(11, 'field', 1, 2, ' harry portter characters name and related indicia are trademarks of and warner bros ent alle rechte vorberhalten '),
(11, 'slug', 0, 2, ' trademarks text '),
(11, 'title', 0, 2, ' trademarks text '),
(12, 'field', 1, 2, ' es glbt noch letzte elnzelplatze fur vorstellungen irn januar '),
(12, 'slug', 0, 2, ' verfugbare tickets '),
(12, 'title', 0, 2, ' verfugbare tickets '),
(13, 'field', 1, 2, ' mehr theater am grobmarkt hamburg '),
(13, 'slug', 0, 2, ' saalplan '),
(13, 'title', 0, 2, ' saalplan '),
(14, 'slug', 0, 2, ' home page english '),
(14, 'title', 0, 2, ' homepage english '),
(14, 'field', 2, 2, ' hp grafik '),
(14, 'field', 3, 2, ' hp grafik mobile '),
(15, 'field', 1, 2, ' eigentumer und betreiber der webseite ist aka promotions ltd 115 shaftesbury avenue cambridge circus london wc2h 8af e mail webteam akauk com telefon 44 0 20 7836 4747 produzent fur harry potter und das verwunschene kind in deutschland ist mehr bb enterainment gmbh erkrather str 30 d 40233 dusseldorf geschaftsfuhrer maik klokow ralf kokemuller gunter irmler amtsgericht dusseldorf hrb 57980 webmaster mehr entertainment de fon 49 0 211 73 44 151 fax 49 0 211 73 44 155 ticketing deutsche eintrittskarten tks gmbh www eintrittskarten de banksstr 28 d 20097 hamburg buro dusseldorf erlrather str 30 d 40233 dusseldorf geschaftsfuhrer kai ricke amtsgericht hamburg hrb 121731 ust idnr de 812 702 791 webmaster mehr entertainment defon 49 0 211 73 44 151 fax 49 0 211 73 44 155 '),
(15, 'slug', 0, 2, ' impressum english '),
(15, 'title', 0, 2, ' impressum english '),
(16, 'field', 1, 2, ' more theater am grobmarkt hambur '),
(16, 'slug', 0, 2, ' seating plan '),
(16, 'title', 0, 2, ' seating plan '),
(17, 'field', 1, 2, ' it still gleams last elnzelplatze for performances in january '),
(17, 'slug', 0, 2, ' available tickets '),
(17, 'title', 0, 2, ' available tickets '),
(18, 'field', 1, 2, ' harry portter characters name and related indicia are trademarks of and warner bros ent alle rechte vorberhalten '),
(18, 'slug', 0, 2, ' trademarks text english '),
(18, 'title', 0, 2, ' trademarks text english '),
(19, 'field', 1, 2, ' website is managed by aka '),
(19, 'slug', 0, 2, ' website managed by text english '),
(19, 'title', 0, 2, ' website managed by text english '),
(20, 'field', 1, 2, ' harry potter publishing and theatrical rights j k rowling '),
(20, 'slug', 0, 2, ' footer copyright text english '),
(20, 'title', 0, 2, ' footer copyright text english '),
(21, 'field', 1, 2, ' lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 1 aka1 1 lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 2 use of the site2 1 lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 2 2 lorem ipsum is simply dummy text of the printing and typesetting industry a lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum b lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum c lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum d lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 2 3 lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 2 4 lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 2 5 lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum 2 6 lorem ipsum is simply dummy text of the printing and typesetting industry lorem ipsum has been the industry s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has survived not only five centuries but also the leap into electronic typesetting remaining essentially unchanged it was popularised in the 1960s with the release of letraset sheets containing lorem ipsum passages and more recently with desktop publishing software like aldus pagemaker including versions of lorem ipsum '),
(21, 'slug', 0, 2, ' data protection '),
(21, 'title', 0, 2, ' data protection '),
(22, 'field', 1, 2, ' lorem ipsum dolor sit amet consectetur adipiscing elit nunc accumsan ornare euismod vestibulum tincidunt massa at augue faucibus finibus duis iaculis sapien quis orci commodo id commodo tortor volutpat etiam luctus est sapien vel tempor diam cursus et pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas pellentesque ornare a ipsum at posuere maecenas sollicitudin leo id ipsum pellentesque tincidunt sed velit quam gravida at ornare sed gravida a nisl curabitur gravida fringilla erat a venenatis nam posuere sit amet lorem ut blandit praesent ligula lectus imperdiet eu leo id aliquet commodo nisl ut venenatis sem commodo auctor gravida nulla facilisi integer congue at dui nec venenatis suspendisse facilisis enim nec porttitor euismod aliquam ornare ante pretium feugiat elementum dui quam suscipit ipsum a sagittis est libero id diam nunc viverra ante vitae maximus placerat risus dolor luctus elit ac aliquet purus erat vel urna praesent varius urna purus sit amet semper turpis scelerisque ac morbi aliquet nibh mauris vel lobortis quam iaculis ut aliquam rutrum odio ut urna consectetur fringilla suspendisse sit amet urna vitae augue sodales pharetra etiam vel felis elit etiam at finibus velit condimentum fringilla nibh morbi dignissim sollicitudin diam ut elementum nisi auctor vitae etiam egestas dignissim ultricies pellentesque vel arcu sed lectus feugiat iaculis et ac tortor in sit amet maximus arcu et vestibulum neque quisque sollicitudin elit neque lacinia convallis leo lacinia quis sed semper elit orci vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae curabitur at sagittis nibh a egestas ipsum ut tellus diam dapibus nec purus a porttitor iaculis diam sed sagittis mauris eu lacinia ultricies pellentesque volutpat in odio pellentesque facilisis aenean pretium massa ac nisl blandit aliquet imperdiet mi mollis integer erat lacus finibus et diam sed vestibulum lobortis quam sed porttitor magna in malesuada accumsan turpis ipsum placerat lectus nec interdum tortor leo ut dui nunc commodo lorem ut fermentum scelerisque aenean id leo malesuada dapibus diam id convallis libero fusce auctor tortor id risus ultrices tempus integer pretium gravida leo nec iaculis velit commodo ut suspendisse posuere ipsum non magna volutpat sit amet luctus massa scelerisque donec dictum tincidunt massa ut gravida integer fermentum felis sapien nec rhoncus mi consequat in mauris eu dictum risus nulla tincidunt a felis nec rhoncus aliquam placerat nisi augue sit amet tincidunt tortor dictum eu integer hendrerit rutrum dui eget pellentesque risus duis et molestie risus cras lacinia vel dui quis imperdiet integer interdum blandit ligula ac congue donec semper euismod rhoncus '),
(22, 'slug', 0, 2, ' agbs english '),
(22, 'title', 0, 2, ' agbs english '),
(23, 'field', 1, 2, ' lorem ipsum dolor sit amet consectetur adipiscing elit nunc accumsan ornare euismod vestibulum tincidunt massa at augue faucibus finibus duis iaculis sapien quis orci commodo id commodo tortor volutpat etiam luctus est sapien vel tempor diam cursus et pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas pellentesque ornare a ipsum at posuere maecenas sollicitudin leo id ipsum pellentesque tincidunt sed velit quam gravida at ornare sed gravida a nisl curabitur gravida fringilla erat a venenatis nam posuere sit amet lorem ut blandit praesent ligula lectus imperdiet eu leo id aliquet commodo nisl ut venenatis sem commodo auctor gravida nulla facilisi integer congue at dui nec venenatis suspendisse facilisis enim nec porttitor euismod aliquam ornare ante pretium feugiat elementum dui quam suscipit ipsum a sagittis est libero id diam nunc viverra ante vitae maximus placerat risus dolor luctus elit ac aliquet purus erat vel urna praesent varius urna purus sit amet semper turpis scelerisque ac morbi aliquet nibh mauris vel lobortis quam iaculis ut aliquam rutrum odio ut urna consectetur fringilla suspendisse sit amet urna vitae augue sodales pharetra etiam vel felis elit etiam at finibus velit condimentum fringilla nibh morbi dignissim sollicitudin diam ut elementum nisi auctor vitae etiam egestas dignissim ultricies pellentesque vel arcu sed lectus feugiat iaculis et ac tortor in sit amet maximus arcu et vestibulum neque quisque sollicitudin elit neque lacinia convallis leo lacinia quis sed semper elit orci vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae curabitur at sagittis nibh a egestas ipsum ut tellus diam dapibus nec purus a porttitor iaculis diam sed sagittis mauris eu lacinia ultricies pellentesque volutpat in odio pellentesque facilisis aenean pretium massa ac nisl blandit aliquet imperdiet mi mollis integer erat lacus finibus et diam sed vestibulum lobortis quam sed porttitor magna in malesuada accumsan turpis ipsum placerat lectus nec interdum tortor leo ut dui nunc commodo lorem ut fermentum scelerisque aenean id leo malesuada dapibus diam id convallis libero fusce auctor tortor id risus ultrices tempus integer pretium gravida leo nec iaculis velit commodo ut suspendisse posuere ipsum non magna volutpat sit amet luctus massa scelerisque donec dictum tincidunt massa ut gravida integer fermentum felis sapien nec rhoncus mi consequat in mauris eu dictum risus nulla tincidunt a felis nec rhoncus aliquam placerat nisi augue sit amet tincidunt tortor dictum eu integer hendrerit rutrum dui eget pellentesque risus duis et molestie risus cras lacinia vel dui quis imperdiet integer interdum blandit ligula ac congue donec semper euismod rhoncus '),
(23, 'slug', 0, 2, ' cookies english '),
(23, 'title', 0, 2, ' cookies english '),
(27, 'slug', 0, 1, ' presale '),
(27, 'title', 0, 1, ' presale '),
(27, 'slug', 0, 2, ' presale '),
(27, 'title', 0, 2, ' presale '),
(14, 'field', 1, 2, ' vorstellungen harry potter und das venvunschene kind ist ein theaterstuck in zwei teilen wir empfehlen das theatererlebnis am glelchen tag oder an zwei aufeinander folgenden tagen zu sehen mittwoch donnerstag freitag samstag sonntag teil eins 14 30 uhr teil eins 14 30 uhr teil eins 14 30 uhr teil zwei 19 30 uhr teil zwei 19 30 uhr teil zwei 19 30 uhr teil zwei 19 30 uhr teil zwei 19 30 uhr tickets kaufen wahlen sie hier die art der tickets die sie erwerben mochten und sie gelangen direkt zur ticketauswahl empfohlen kombi teil eins und teil zwei aufeinander folgend schauen empfohlen kombi tickets kaufen ﻿﻿individual teil eins und teil zwei unabhangig voneinander schauen individual tickets kaufen single nur teil eins odernur teil zwei schauen single tickets kaufen '),
(24, 'field', 1, 2, ''),
(24, 'slug', 0, 2, ' footer logo block english '),
(24, 'title', 0, 2, ' footer logo block english '),
(25, 'field', 1, 1, ''),
(25, 'slug', 0, 1, ' footer logo block '),
(25, 'title', 0, 1, ' footer logo block '),
(27, 'field', 1, 1, ' vorstellungen harry potter und das venvunschene kind ist ein theaterstueck in zwei teilen wir empfehlen das theatererlebnis am glelchen tag oder an zwei aufeinander folgenden tagen zu sehen mittwoch donnerstag freitag samstag sonntag teil eins 14 30 uhr teil eins 14 30 uhr teil eins 14 30 uhr teil zwei 19 30 uhr teil zwei 19 30 uhr teil zwei 19 30 uhr teil zwei 19 30 uhr teil zwei 19 30 uhr tickets kaufen waehlen sie hier die art der tickets die sie erwerben mochten und sie gelangen direkt zur ticketauswahl empfohlen kombi teil eins und teil zwei aufeinander folgend schauen empfohlen kombi tickets kaufen '),
(27, 'field', 3, 1, ' hp grafik mobile '),
(27, 'field', 2, 1, ' hp grafik '),
(27, 'field', 2, 2, ' hp grafik '),
(27, 'field', 3, 2, ' hp grafik mobile '),
(27, 'field', 1, 2, ' vorstellungen harry potter und das venvunschene kind ist ein theaterstuck in zwei teilen wir empfehlen das theatererlebnis am glelchen tag oder an zwei aufeinander folgenden tagen zu sehen mittwoch donnerstag freitag samstag sonntag teil eins 14 30 uhr teil eins 14 30 uhr teil eins 14 30 uhr teil zwei 19 30 uhr teil zwei 19 30 uhr teil zwei 19 30 uhr teil zwei 19 30 uhr teil zwei 19 30 uhr tickets kaufen wahlen sie hier die art der tickets die sie erwerben mochten und sie gelangen direkt zur ticketauswahl empfohlen kombi teil eins und teil zwei aufeinander folgend schauen empfohlen kombi tickets kaufen '),
(27, 'field', 6, 1, ' https www google com '),
(27, 'field', 5, 1, ' atte fans die sich bis zum 15 marz unter www xyz de fur den newsletter registrieren haben vier tage vor dem offiziellen vorverkaufsstart am 25 marz die meglichkeit hier tickets zu bestehen '),
(27, 'field', 4, 1, ' noch 10 tage bis zum tickets presale '),
(27, 'field', 6, 2, ' https www google com ');
INSERT INTO `searchindex` (`elementId`, `attribute`, `fieldId`, `siteId`, `keywords`) VALUES
(27, 'field', 5, 2, ' atte fans die sich bis zum 15 marz unter www xyz de fur den newsletter registrieren haben vier tage vor dem offiziellen vorverkaufsstart am 25 marz die meglichkeit hier tickets zu bestehen '),
(27, 'field', 4, 2, ' noch 10 tage bis zum tickets presale '),
(27, 'field', 7, 1, ''),
(27, 'field', 7, 2, ''),
(27, 'field', 8, 1, ' wenn du dich bei www xyz de registriert hast hast du einen code per email bekommen den kannst du hier eintragen und exklusiv vor offiziellem verkaufsstart tickets kir harry potter und das verwunschene kind kaufen '),
(27, 'field', 8, 2, ' wenn du dich bei www xyz de registriert hast hast du einen code per email bekommen den kannst du hier eintragen und exklusiv vor offiziellem verkaufsstart tickets kir harry potter und das verwunschene kind kaufen '),
(28, 'filename', 0, 1, ' hp_grafik mobile png '),
(28, 'extension', 0, 1, ' png '),
(28, 'kind', 0, 1, ' image '),
(28, 'slug', 0, 1, ''),
(28, 'title', 0, 1, ' hp_grafik mobile '),
(28, 'filename', 0, 2, ' hp_grafik mobile png '),
(28, 'extension', 0, 2, ' png '),
(28, 'kind', 0, 2, ' image '),
(28, 'slug', 0, 2, ''),
(28, 'title', 0, 2, ' hp_grafik mobile '),
(29, 'filename', 0, 1, ' hp_grafik png '),
(29, 'extension', 0, 1, ' png '),
(29, 'kind', 0, 1, ' image '),
(29, 'slug', 0, 1, ''),
(29, 'title', 0, 1, ' hp_grafik '),
(29, 'filename', 0, 2, ' hp_grafik png '),
(29, 'extension', 0, 2, ' png '),
(29, 'kind', 0, 2, ' image '),
(29, 'slug', 0, 2, ''),
(29, 'title', 0, 2, ' hp_grafik ');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` enum('single','channel','structure') NOT NULL DEFAULT 'channel',
  `enableVersioning` tinyint(1) NOT NULL DEFAULT '0',
  `propagateEntries` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `structureId`, `name`, `handle`, `type`, `enableVersioning`, `propagateEntries`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 1, 'Basic Page', 'basicPage', 'structure', 1, 0, '2019-02-13 14:13:49', '2019-02-18 07:06:19', NULL, 'e1b05292-79dc-42a6-baa4-8d24956e278a'),
(2, NULL, 'Homepage', 'homepage', 'single', 1, 1, '2019-02-13 14:14:34', '2019-02-13 14:14:34', NULL, '9f15624d-d02b-4800-8f08-19a98b8b8118'),
(3, 2, 'Editable Blocks', 'editableBlocks', 'structure', 1, 0, '2019-02-14 10:17:27', '2019-02-18 08:17:47', NULL, '5afdd829-3b74-411b-a718-30f79c756455'),
(4, NULL, 'HomePage English', 'homepageEnglish', 'single', 1, 1, '2019-02-18 06:37:18', '2019-02-20 12:11:04', NULL, 'c641c838-23d8-4911-96cf-76b439df5a95'),
(5, NULL, 'process', 'process', 'single', 1, 1, '2019-02-25 13:41:03', '2019-02-25 13:49:53', '2019-02-25 13:49:53', '2143ef4a-d675-4c58-9e70-3f9505a9e472'),
(6, NULL, 'Presale', 'presale', 'single', 1, 1, '2019-02-26 06:41:43', '2019-02-26 12:51:33', NULL, '30b6bf6f-7f03-4793-bc60-e6ad58c51bcd');

-- --------------------------------------------------------

--
-- Table structure for table `sections_sites`
--

CREATE TABLE `sections_sites` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `enabledByDefault` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sections_sites`
--

INSERT INTO `sections_sites` (`id`, `sectionId`, `siteId`, `hasUrls`, `uriFormat`, `template`, `enabledByDefault`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 1, 1, '{slug}', 'basicPage/_entry', 1, '2019-02-13 14:13:49', '2019-02-18 07:06:19', '6e92b50b-96be-4178-867f-d670fffa3410'),
(2, 2, 1, 1, '__home__', 'index', 1, '2019-02-13 14:14:34', '2019-02-13 14:14:34', 'dd396917-b22e-4eb8-a8ec-86e5e8c43346'),
(3, 3, 1, 0, NULL, NULL, 1, '2019-02-14 10:17:27', '2019-02-18 08:17:47', '8ec9833e-9e50-46f7-9e47-7fbbad92c4c3'),
(4, 4, 2, 1, '__home__', 'en/index', 1, '2019-02-18 06:37:18', '2019-02-20 12:11:04', 'c637544d-a970-4717-991c-577daaa35b46'),
(5, 3, 2, 0, NULL, NULL, 1, '2019-02-18 06:49:34', '2019-02-18 08:17:47', '47a137a7-2a30-4504-a1f1-d0383dbe8a55'),
(6, 1, 2, 1, '{slug}', 'en/basicPage/_entry', 1, '2019-02-18 06:50:10', '2019-02-18 07:06:19', '484fe782-a60f-416e-a29b-2ee9b6930b1c'),
(7, 5, 1, 1, 'process', 'process/index', 1, '2019-02-25 13:41:03', '2019-02-25 13:49:53', '08b3c011-7b49-4891-a394-16085b22583b'),
(8, 5, 2, 1, 'process', 'process/index', 1, '2019-02-25 13:41:03', '2019-02-25 13:49:53', '2fe5dc95-29a0-42ec-b493-8774a4533fd1'),
(9, 6, 1, 1, 'presale', 'presale/index', 1, '2019-02-26 06:41:43', '2019-02-26 12:51:33', 'e6df122f-2758-4de6-8a4c-2130c45ebafc'),
(10, 6, 2, 1, 'presale', 'presale/index', 1, '2019-02-26 06:41:43', '2019-02-26 12:51:33', '25768a51-c2d5-43f0-a06c-e89ffdd30b3c');

-- --------------------------------------------------------

--
-- Table structure for table `sequences`
--

CREATE TABLE `sequences` (
  `name` varchar(255) NOT NULL,
  `next` int(11) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `token` char(100) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `userId`, `token`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 'JrTJUlzoFD0O9K2RRN57lFgVfP-O2RGOWTK6gmB3l3UEtvx68rRlscRuUIR5BA8P09KwmnPSB4_O8MHr3W_W32hM1jQClpfc4aQL', '2019-02-13 14:11:32', '2019-02-13 15:16:12', 'b7ea14f0-946d-4e3d-94bb-4fcb0a49ea5b'),
(2, 1, 'tOjre5tmb3aCchF3qA6OKT3s7tiWBrqTY20mSmEHiOhJAfnPT6DCCdOvXNti-TPhyZryOuzfYpaYoOQJY7ncWE7b0Znu7o4qstIg', '2019-02-13 14:28:22', '2019-02-13 15:16:32', '6426b4e8-86dd-4274-a3d3-4106d1805cdd'),
(6, 1, 'vUG_bzBxE9-SmoOit2SZxJZi7bv1iP36W8Tmkcqzgvh4Yx1z-PRqSsOhaF3Na6yVBusxZGTTZSPZNmKAHYTx-KJ8p_C6ml2bRT6M', '2019-02-14 10:16:17', '2019-02-14 13:33:47', '9704de1e-8c17-49c9-80d2-9deebd45e48f'),
(7, 1, 'R23Ry7PQ6mXY5FijqUT9WXNFyqc6k89Nl6ty52kLDXecyq-xuwTZCkMOmUKe6l5bYSF_PM0gnzOamQB5UzVMCkQqKooYtrSW-U_K', '2019-02-18 06:30:27', '2019-02-18 09:10:57', '2f883dde-cea0-45d6-a201-0bab70f84ba2'),
(9, 1, 'YmoolYDCOX15vfFyTAKkU6dEeWr67kAjV-H5kNJd2LxxNMgNb2IsMLqr_pbY2bo3e26KatO3Zld0oT0I8HfcTvBQlvo1vKPfvLEX', '2019-02-20 09:22:52', '2019-02-20 11:12:24', '3e03cdfd-14e9-473d-9976-12869a65ad7f'),
(10, 1, 'RSiP7G_MHeNo-5utbIHArjuHMOdFqpKjDddwqO6E84AmhRnue4G7lbzOKt3QkrPEgjucjAue5JqBvdlxkFl4yndSLCkjz3uTnM9c', '2019-02-20 11:13:38', '2019-02-20 11:16:24', '5355867b-d45c-483f-9ea3-694dee191cef'),
(11, 1, 'r4i3CcwOPau1Ce20VM4m9bTgjWLu5Y8kDqzydU5BZHt8gOQYwqJq-4WAyU2yqAB2GRjVa7t9_7ybrfyuxe8cVjAGgQponPQx36Uo', '2019-02-20 11:16:31', '2019-02-20 11:21:30', '027aa8a5-46e8-4b66-9952-2b67aacd5df3'),
(12, 1, 'ZdHIOZgJdxZHuNutUnbr5N_jNLR20s8wqlZ_blaxvFFgU7e6QTRG2xE8sV8ld0c8vF3Fol3tvNZzzZSM02d66Cwb1Qnr68_meBx4', '2019-02-20 11:21:31', '2019-02-20 11:21:31', 'b720e21d-03f3-4c7b-afff-728c6ac64dd6'),
(13, 1, 'IMJJV2DJsAcurx4_r4pZF9cKkTrehIcnv4cH-YdZ-94EN4ntuNzd-z_U3w7R5X1qGaCnicLXsg3Jrl5_pKFw_RIgSEX_uYFYkz8U', '2019-02-20 11:21:32', '2019-02-20 11:21:32', 'f8855245-b55c-4fe8-b701-1b2a1e4892fe'),
(14, 1, 'aAZ4TgsTyylfHdrtwayrlfRgshDu-wDU-BLfXWN4zFXjm2Q78WorZexxUTFvICtpm8omLoUS50ofVzCa8qT138mgfhrbiEOFd-ai', '2019-02-20 11:21:32', '2019-02-20 11:21:32', '0552070b-9370-4a87-a42b-e4e7ffd680fd'),
(15, 1, 'SR75JkJKcUSMnadpYUnq45zcQ-k5dVmzUMLnuN0NhSX4XtMqgSvt2_wR-Qawf3VsVp_ARBkHwSCPD6X67uLfcHfmyntM1fmI4B_L', '2019-02-20 11:21:47', '2019-02-20 11:24:51', 'b2ed1483-af99-481c-be07-4551ef5af25c'),
(16, 1, 'zuelFAQ7W4Lsb6DfevIyqtZkQWCLA1qEB-2Wqm-qpfAtIOtWG0_8bGqpN6rOD5bgfQa-kO6NsobZ8F705PxbZW82O2XOndW1395X', '2019-02-20 11:25:00', '2019-02-20 11:25:31', '3aaca012-58ba-4041-83e3-964273c6a0be'),
(17, 1, 'xEgRy3tYJ5e4O-OBcG5UGEbmpMek5lgRB5OHlMpXm__gyi0Fe5huQk2VIAR47tB5k9yp8ApfdwZYBLtDci2GFPDqbI3xZl9Rsxeq', '2019-02-20 11:26:01', '2019-02-20 11:31:09', '47fae4cf-6c1a-4eca-a977-004b8de28c4c'),
(18, 1, '_IEIVi48zTYD4qyvX4UjMlTDPyBOKe7nNMUzo0K5bs9RSdcSzappXS4jbXFc0qZKNhgo1qRA3bYl7YlJCDcvPNZpFHi0m9KzLvPa', '2019-02-20 11:34:11', '2019-02-20 11:34:11', '01d9fecd-8e9e-473a-b6fb-e7a767a9ae7c'),
(19, 1, 'b8ol3nf36-e5dJTL0J1YShBZm8Wcb_-zw17KLpOm-tZzXN7kP-vu2MbmurjIDWSfup2jkAJqhkd_cN3u1yTBm3I7dntxq9rfrqWn', '2019-02-20 11:39:12', '2019-02-20 11:39:12', 'b05b727b-6170-4182-9081-285adf749685'),
(20, 1, 'vkWvWe1oAiRgMY6cNiI7kVGx-Nrh_DV26U8au-2fo1zOnyClGfp2ErUwHUwIjiichYHexq73RsPqYTtsQZDK2DjcUhCUyeDfXB_8', '2019-02-20 11:44:14', '2019-02-20 12:10:49', '0ae83f45-c45d-427d-9f5a-42647925fcd8'),
(21, 1, 'SVSGvGghpjidETYK1kMCJ7WA2jScevT9HYK6yQq0UfYcRxhVdsDBUs4H25F_HKY070slCZJBy4bwLP20gSqkUnvcyJkNvb5U-K_j', '2019-02-20 12:10:52', '2019-02-20 12:31:42', '9bac5a20-acb1-4827-bc79-b2d121d6b7f5'),
(22, 1, 'wfrytdVVkrYIG_acYq3VAAPaLyxKBNWdQQXQbV4l6jY7mQaIGScfnucXZa_pMNzhr1iyHcOthQ31Hmx0hGQ_15ZRA1V04MaZXdvF', '2019-02-21 06:21:47', '2019-02-21 06:27:56', 'afb1807e-ce1b-4699-bc68-4354877266c3'),
(23, 1, 'i7w4RlTSg-O1gY2zjU6y3kfbsVvkWbL3tmS8FVEIhCNN21yKV7JONnEcGQlsXfHyvpS3IGptQazP2ERKiGyxuBCeTSzQnA-Iuawk', '2019-02-21 07:28:00', '2019-02-21 07:28:00', 'a8c3e074-c4f5-4d76-9d0d-a0e49af25403'),
(24, 1, 'M_ng8BOEAcvCzx8QvM0SDmp3-wphR4eQbhubGX7Uw-85yquf-XTZUh1iEQL0t_DLUFY8XYH0P15IsW0KjbZp0JbI3H7j3LxRZzjY', '2019-02-21 08:28:03', '2019-02-21 08:44:19', '701ceea0-139b-4274-8d48-3e7f8f82c60d'),
(25, 1, 'x8BtG1ii1AZd_wf8zEDazM-zg_iQvvVNJHiWBrxaO9Wp9yQPwGtEU18LDqrJTScExE704FmbIogtglQ52Ocwjnmw-ZnvbevIjmIp', '2019-02-21 09:44:22', '2019-02-21 09:44:22', 'e64c1ed6-03df-4cf9-9866-eb9ad8eee1d1'),
(26, 1, 's9a5tbCeqohk3gpbPuhFKxsynQg0m66e3TZpooj87QB6WG607aCES-JvTDMjFSAfpFp7nqaRftnjB2EbhP76SxQy_nmESOtCzQdD', '2019-02-21 10:44:25', '2019-02-21 10:44:25', '821e13f2-6aa3-4e49-8eb2-562fea49f9ef'),
(27, 1, '2q4YC4y_dbGB16xgq8JaCtlB_tz9LuB7m3Nm2LnACRi0sKdOfcE1sAoFqZAJ6PCKxOWBHiAcUZcDm2mtw2ZtrGQV1BVE0EOubhJM', '2019-02-21 11:44:28', '2019-02-21 11:44:28', 'a6a0022c-8f55-4d78-8579-155e38cef985'),
(28, 1, 'UxGYMdGvnA9Gld3kmHpi5TOqkRO648TvdFXmJR_lHoW-OmHUTJFJYdLqTvkxS97bL_3OyAP1pAfElgdsLsB26KWKwIPagbLO7Dz9', '2019-02-21 12:44:31', '2019-02-21 12:44:31', 'ec5cb565-63c2-45e0-982a-37e210eccf43'),
(29, 1, 'c3Fx5Abi8iCpEObNSgI7bwDaxe70ELO6AgTWczTaJziV3k0AYuIrT6myT7W85lM8UicCXJT4Ylj9tog0OSUJii1HB9Wh-IUISsYj', '2019-02-21 13:44:34', '2019-02-21 13:44:34', '987c4318-054f-4fed-bde4-beb322b696fd'),
(30, 1, '0lqs6_2Ff0BQ99RdMqwVIBtgUt3iciwoBj9LQoFcrvXEbMNABTnV7BIUnCzMZyTn40W7BbgTGbTmyn9ApKSrn-DU0TP1CFvNeA9h', '2019-02-22 08:54:36', '2019-02-22 08:55:01', '98a87b31-8ef9-4ae0-b2e2-7ac0b3bad36b'),
(31, 1, 'Rr-QUcCZkUtP8fUBvjaOmfTNC9Xy0bPixukK2A4vETV8Pk2thSn-pek7xNJp_6CdWMklYyf7Xy9bpGHD8cFPtJWW7UXCOfDz817f', '2019-02-22 09:55:04', '2019-02-22 09:55:04', '44505334-e4e2-452f-b37e-5f9777d88af2'),
(32, 1, 'EQqoAipG4IPVNg3M8iAlOqFLXIYJOE3PvpQ36zGdU0AouIt-VERCobZGLlnQorgpCXHUwCaPiCWKsAeej3NiwlAMk74SdpPHU6u0', '2019-02-22 10:08:08', '2019-02-22 10:08:08', '4ff96a08-1c7f-494e-96df-58d49420609b'),
(33, 1, 'wlUMI1qHwZNjwMzYbDk_j13ekQpcI0KnGBCpE-RBgKeKMgHA1X88lpxXctdO87Wm4-u-HbC-0UWN-D-JOipw-TNWC3wOK6Lpqnc_', '2019-02-22 11:06:24', '2019-02-22 11:06:24', '5f4a353a-25be-4f13-9821-fc28cb6686f8'),
(34, 1, 'g8kSO3zKKnF0iJjO-a4tVcTwcStGBLlKzGB764Uce8sL7d3cztfniWHdX7IrTJQVyKusxDZFxid-MJSH4T_8qoH9U60cxZ3bxJ5w', '2019-02-22 11:16:30', '2019-02-22 11:16:30', '8957c2ef-9364-42e7-bf46-46a9d9508ac2'),
(35, 1, 'm-8AX2zPg2iQh-jdySMeAbhx4zLM30HOd4XzVUE7WE_NkmJwpd8QMHl1tpfLOBniC7LfkvzLhsNn0mPe_jkACKNC0nOLqHku0UpC', '2019-02-22 11:18:31', '2019-02-22 11:18:31', '1693ac3d-fe5e-47de-b4b4-a80066d6fa1a'),
(36, 1, 'Awdxm2gRZcssjhAZinJhOIm4lZt9uqr7BILkulyLLxja47aOsF7gO_HPr731LsavGZ8VNomSIA8NZ0rL0FJ8utbG3FodK15rjAAM', '2019-02-22 11:24:33', '2019-02-22 11:52:58', 'cf94a239-c517-4de9-a743-93adfd40e8f7'),
(37, 1, '12nEI1pmjNWD8yt201wpqnSKEdjJzvpNrz8VsNlr7z7-CClx8wKbjd7MRTzGtI1_MQIUNrQ13Y3xc5k8_UkPMEsPZivq_1lKYRGM', '2019-02-22 12:53:01', '2019-02-22 12:53:01', '41caf80c-1e98-4624-ac1f-efcd7fd7723e'),
(38, 1, '-FY05v4zn9rYHoRuBQvvAMjXNJwGe3eka8sqYuxcMN7Ec8LC1-wJWPey7I4_bLQSfdr1YpLdKpHIqu34JAMij-w90UOkPMZmIRIc', '2019-02-25 10:38:48', '2019-02-25 10:38:59', '39ef234a-5cb2-4a6b-99fd-386a7f995e7d'),
(39, 1, 'vE7NfaMwxdXwbTwqmBEYEUZ776e6011cZ7ZqauxdbCkzladQK5CHVPwGBn-WV6Fxw8XEfJwxcd13GoQcuStagL3MvX7XG_svCPFi', '2019-02-25 12:53:03', '2019-02-25 12:53:09', '87c56c84-d01c-4234-bac8-c7e5cc9fdd97'),
(40, 1, '6higQu3R4zQZ1U45EOe5kgMOwpht_t8R2FUwA3Nt7KXzpDCQxGwGtUk8_eMGPhJXWexEgJEbPF7KJT7YV9GORK4DiTKxxUGevReD', '2019-02-25 12:57:08', '2019-02-25 12:57:08', 'e241e8d2-7974-4c78-ab81-e39d5c2cde07'),
(41, 1, 'jgKVFGiF4zIacxIZEtYx_QaMH8bDW-l5EvFx5wrX47rBml3bdF1EdSi2BTDIyZMhPtNNeEufxvn9VL7B9V1edrgJQSmauwkNbL2C', '2019-02-25 12:59:09', '2019-02-25 12:59:09', 'd030b0aa-fc9a-47f1-8e7f-1f8ca7822ffc'),
(42, 1, 'UzNB4EgznlBuGNPag-ppYQIXktPeODCRTpGf1HF47719Puq4LuTTHj7rxIy2x3Uc8jB62-deEjGdtSfMtl3gMa6RPFOP05d3_aOu', '2019-02-25 13:01:10', '2019-02-25 14:43:43', 'a01cbbb8-1337-4b76-8069-7e0c864228c6'),
(43, 1, 'A0Wm0bz0AqhRTLOiyQFNOREwOWLTuhGWF-uDs8yfMQsRuAhvfgFpXTEUTMvNMjG7DqjRRU3i9AO6bNRkZNAP_qEIEbqgy-I3H-gN', '2019-02-26 06:00:47', '2019-02-26 07:02:15', '699a99e1-6c5e-474a-976d-d00224be8e09'),
(44, 1, 'faZfqY3rHpJhTbNMNK-e36LT71J-73Bwr2yQz0YLtDihGepfblufIx62xtuwYjwJcLfMf2TXmJ-vx6IPtxNcJfz1lHV4T6BR_Rsd', '2019-02-26 08:02:18', '2019-02-26 14:16:30', 'd78d57f9-70a2-4317-8875-28b95ad89140'),
(45, 1, '4CwkY9HUZI0CuYBvYTqufj_EQsPsDT1AR-wN9zBKrJ1VcFX4WJ6gZNtf5G375dQZE_oXtgHz8enQDn4j0gkCqHSIRKvsSLiVBi0X', '2019-02-26 14:26:09', '2019-02-26 14:42:08', '5ba40b12-68a9-4d08-8002-bc80ca221957'),
(46, 1, 'vGDGg9pGCMHQXdyWh0aCpOPWSRc5bEq5Z4ubQE6VyHNQfsRhzfX_EUYXFrdBpKIEuMuv5-Tugn9RFT6U67yRVTdGtYuMhGYvCT1k', '2019-02-27 09:09:34', '2019-02-27 09:34:35', '7382c53a-d4e0-4584-89c1-43ba7d125ebf'),
(47, 1, 'MtWwFy5NKGqY175VamtgodNoZ2J5YYqEV2lq_6AeNaJ98m2hn5E858bngLITnMynqQ3pPoytxeLDCcgNt4_H40Q9uCA7FFlkJNYT', '2019-02-27 11:09:31', '2019-02-27 11:34:33', 'e8800df8-a309-4897-9dc9-d66f13636e73'),
(48, 1, '9U1vCD3fEQda7vJ2StejZoo-XXOXTdJyrz7nQtgc4-G-pCPtiQHvcmAF6Ifa_KAdr143nl-E1GB47cOkUcT69oeDE_ONaPDHk6pW', '2019-02-27 12:34:34', '2019-02-27 12:34:34', 'ba89179e-5164-4dd3-9210-cedd50f2ba8f'),
(49, 1, 'Ksu7GI9vJTKikDl4xBIoEOCBOeuMX9fbNuspicfaUeiibMPO5R7gYAJPQhEbaTJ-ldWw95AMbfliplmBGZjsxIxghyqusxDX58Lw', '2019-02-27 13:34:36', '2019-02-27 13:34:36', '9a2f9913-4565-4ab6-a826-f4c3e0cc82df'),
(50, 1, 'JRr9wQny5pcYn_dLQ1S9snVPsz-fYM8bDNeJxRpMtrCDBUgEPcqScKN6sORmh4zE1qihTEyDHoSJwj963Fprw0zXVr8vnD5nosaD', '2019-02-27 14:34:37', '2019-02-27 15:04:07', 'f735f2ad-f1a9-4505-8e14-8227769819b3'),
(51, 1, '3_HhpWsYvOdsKamyX3Br7vAnqjl1wnGYXHLwCGSrkdQxDnZI4JZYEaxRW2-cKNAq78Yw4YcEQ9KaEG0Wn9t658sR6i2xGdrx33-o', '2019-02-27 16:04:08', '2019-02-27 16:04:08', '036e616d-6066-4aaa-880c-bdb3e6789739'),
(52, 1, 'x9zMWLtLxt65DKFkpVw5gLwuiUxm3QXYyRyxnqzv4bq2EOouOYCWuFMZzrvYSorfBF0hJjhXP7tB52RHQXt8v8Vbuju4VWYn91G8', '2019-02-27 17:04:10', '2019-02-27 17:04:10', '4fc1f5fa-0430-4bc5-9e87-5facb29a74e2'),
(53, 1, 'wKzIKSoKoSzJrKveu2Vi40kkZstW1mWSyioe5_LIzcfe1BKH_kJGpL0KyWNdLYs0l2q-hUN8yTjXgmRo1totarGx6Q-i7PoV2486', '2019-02-27 18:04:11', '2019-02-27 18:04:11', 'd8bd05f4-7984-4386-afb0-8e97ef2f3380'),
(54, 1, 'JbNVIGnHQZ-klWrcx927Jtg7Z0hQj3GsKOFG_OswHs-iZQek4CqfOPOKniwl9GPw4MnVWx1Uu4Ddmd9v3Zl3Egtj2a81MU90-gk7', '2019-02-27 19:04:12', '2019-02-27 19:04:12', '9c0dad1b-d5d2-44ac-bba7-d8c6be20871d'),
(55, 1, '9aGOL-nWLZwuH7ckHA9EHlgV7g5qbJw54h5BwzHMrF_4P5SCWmnQFh7vPvKPUUZQdy7hnmyMFgWEV5zpZf3iO5e8E_g3n9bDY1MU', '2019-02-27 20:04:13', '2019-02-27 20:04:13', '694be0b0-c522-4ac9-9b98-f9bddd668d08'),
(56, 1, '6GqeW62qjgp3JFRK7BxAy6cy5uemUGluDA7bCA4p-dj0UlzUIIR7T7OmaRbNXCoCVHSS6Vd1x3HAlbl8yEzVpo7Jo4I-yXGy2TRw', '2019-02-27 21:04:14', '2019-02-27 21:04:14', '5c44cab2-dd74-4181-b390-d2078684cefd'),
(57, 1, '57TWBJX2oFzSGKtGXta23ru3xMsXo35sS-i33ZICpcueJIkja3Sh8tE__eFrD3uDSKNjjR1DoqNUG03QAeVKvahAZnNZjrUTc0L-', '2019-02-27 22:04:15', '2019-02-27 22:04:15', '09a3dd17-eda4-4dec-a662-0dde1d9bb6ba'),
(58, 1, 'x0dzLXzU10aYNhQwyW3kwMTIUEPTCAu6bKejaX40XcgYuFlLrzdbxp1Zedb2lgPmsZejhVLpZIsQHZB4Wi_nVWfyTFCMvlH1GXhb', '2019-02-27 23:04:16', '2019-02-27 23:04:16', '63fe0141-61e4-47fd-8c52-e3707a1fcf4a'),
(59, 1, 'PCX6u2Dr10WMmRQaXKrS4mHil1IL3MclxAJjnv3fhGdI0vUSlueUAyAJmul-WjLcZfD6QUEN4ZhrzCqP7XxQw6AWHiXvDLgulttx', '2019-02-28 00:04:17', '2019-02-28 00:04:17', 'd25f814d-2d21-448a-80fc-2b70f2dc1812'),
(60, 1, 'bU4Gmy62tqDJf9BAkwRbFpmRkCPRfAlvPZPYbo76MUtPzucu_LtcHP26mCpgQORvEToDFDQht0QTXntk0XnNTCyq703KfgnRukqH', '2019-02-28 01:04:19', '2019-02-28 01:04:19', 'cc13bbdc-1b81-4083-b030-9160c732dcc0'),
(61, 1, 'fmZvKic1trUu_NEY_cfF_pR9wLN_pBPBtE3SpTZO-kzn4uA4x9_0pdnSAwwQudZmIc3c4nRg7pCx1th9kYaf9L3vAeC-xQHsGIOS', '2019-02-28 02:04:21', '2019-02-28 02:04:21', '391ccc6c-99ee-49f4-a530-1482c4e342c5'),
(62, 1, 'ikA6JQNTmS5QzZAnFcK8OAW69cD4GXm-4NPPIeF8Px3MwEtPO3yI4e7DxDKi035a2Bh8IvuX-K7Q4MwoydwgqjYG60pwxdgRpLun', '2019-02-28 03:04:22', '2019-02-28 03:04:22', 'bc48562b-f1b7-4c7f-b9d6-d57ccd860835'),
(63, 1, '8PPl-C_-rK7eUbLYrsCusCCWtluP-py7TZLWXvobNOT5u_KqvLBdw6uiHIs7a1xUbyhH8UUe7JXWAE03IiBUyhSMNzfYRMkhHYT3', '2019-02-28 04:04:23', '2019-02-28 04:04:23', 'cdb964a2-7a25-4ff2-ace8-fb6e19d78905'),
(64, 1, 'jBlsrdXKJqWKNH-D1XeWYA3Ozo5ux9f60aabwIAyyVW7sOfzHXiAU8IhFkLoOAQDkpXyvpluPHZWiroNPNCqnQx1gEf5Bc5p0s3_', '2019-02-28 05:04:24', '2019-02-28 05:04:24', '53f2f487-b436-4b8d-b75a-6affea49b013'),
(65, 1, '6TaZQNNoNaBENT159lUKnSpf_9MDBzWiYMXwDf4hwUA_IMv1wi6ghZ8pJukHrZJGsUObznTmUG-1pFC-w-4VqhqtkBM-pYwXQBPI', '2019-02-28 06:04:25', '2019-02-28 06:04:25', '73056a50-96f7-42e5-b47c-9a9722ec2687');

-- --------------------------------------------------------

--
-- Table structure for table `shunnedmessages`
--

CREATE TABLE `shunnedmessages` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sitegroups`
--

CREATE TABLE `sitegroups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sitegroups`
--

INSERT INTO `sitegroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 'Harrypotter Ticketmaster', '2019-02-12 06:57:57', '2019-02-12 06:57:57', NULL, '874ec50f-856d-46fb-867c-d9b4af8688bf');

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE `sites` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `primary` tinyint(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `language` varchar(12) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '0',
  `baseUrl` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`id`, `groupId`, `primary`, `name`, `handle`, `language`, `hasUrls`, `baseUrl`, `sortOrder`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 1, 1, 'Harrypotter Ticketmaster', 'default', 'de', 1, '@web/', 1, '2019-02-12 06:57:57', '2019-02-13 14:45:09', NULL, '729f3241-22db-46b8-bb9c-d71237b64b1b'),
(2, 1, 0, 'Harrypotter Ticketmaster English', 'harrypotterTicketmasterEnglish', 'en', 1, '@web/en/', 2, '2019-02-15 06:56:49', '2019-02-15 06:56:49', NULL, '68cc0d46-d25a-403b-bac6-43627f738486');

-- --------------------------------------------------------

--
-- Table structure for table `structureelements`
--

CREATE TABLE `structureelements` (
  `id` int(11) NOT NULL,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) UNSIGNED DEFAULT NULL,
  `lft` int(11) UNSIGNED NOT NULL,
  `rgt` int(11) UNSIGNED NOT NULL,
  `level` smallint(6) UNSIGNED NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `structureelements`
--

INSERT INTO `structureelements` (`id`, `structureId`, `elementId`, `root`, `lft`, `rgt`, `level`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, NULL, 1, 1, 18, 0, '2019-02-13 14:37:12', '2019-02-18 08:24:19', 'bbb5d521-b143-4089-8b03-a3102a8d7e38'),
(2, 1, 5, 1, 2, 3, 1, '2019-02-13 14:37:12', '2019-02-13 14:37:12', '344eece1-9fed-4e2e-b3e7-bbbdf5f78fe7'),
(3, 1, 6, 1, 4, 5, 1, '2019-02-13 14:37:35', '2019-02-13 14:37:35', 'e9ddc240-a2cf-424d-b21c-dd6f9ecc97ea'),
(4, 1, 7, 1, 6, 7, 1, '2019-02-13 14:37:55', '2019-02-13 14:37:55', '73a2eec6-7d18-46b6-91a9-ff2ce5be32ab'),
(5, 1, 8, 1, 8, 9, 1, '2019-02-13 14:38:09', '2019-02-13 14:38:09', '321481d6-b06d-4a9f-a6bd-fa8f08cc049b'),
(6, 2, NULL, 6, 1, 26, 0, '2019-02-14 10:18:18', '2019-02-20 12:26:24', '7fc3ce8e-8ecb-4445-aa3e-194021221812'),
(7, 2, 9, 6, 2, 3, 1, '2019-02-14 10:18:18', '2019-02-14 10:18:18', '4a152af1-17e2-4ada-9d73-e5c1d4d99655'),
(8, 2, 10, 6, 4, 5, 1, '2019-02-14 10:29:20', '2019-02-14 10:29:20', '8467cde6-1046-499a-95ca-663b7ca5596d'),
(9, 2, 11, 6, 6, 7, 1, '2019-02-14 10:35:54', '2019-02-14 10:35:54', '4c671e86-e406-44fd-899c-35203bbd1564'),
(10, 2, 12, 6, 8, 9, 1, '2019-02-14 11:18:50', '2019-02-14 11:18:50', '56308472-21c4-421d-a7be-0f4cb8498342'),
(11, 2, 13, 6, 10, 11, 1, '2019-02-14 11:51:41', '2019-02-14 11:51:41', 'de6bc764-85d1-4b4e-b5db-ac70b2732f9e'),
(12, 1, 15, 1, 10, 11, 1, '2019-02-18 07:07:45', '2019-02-18 07:07:45', 'c170a1f1-e8d6-4cb2-8dbb-f3ec82893ab0'),
(13, 2, 16, 6, 12, 13, 1, '2019-02-18 08:18:44', '2019-02-18 08:18:44', '0f6ff503-4ce2-4347-a974-b338904ca7a2'),
(14, 2, 17, 6, 14, 15, 1, '2019-02-18 08:19:31', '2019-02-18 08:19:31', '73120fb4-5d9f-4995-b54a-314aea28b7ba'),
(15, 2, 18, 6, 16, 17, 1, '2019-02-18 08:20:11', '2019-02-18 08:20:11', '240520df-d5b4-43d9-909e-d7a049bee187'),
(16, 2, 19, 6, 18, 19, 1, '2019-02-18 08:21:26', '2019-02-18 08:21:26', '5f625690-630d-4a1d-b9e5-f81b657dc52a'),
(17, 2, 20, 6, 20, 21, 1, '2019-02-18 08:22:12', '2019-02-18 08:22:12', 'c9cbab12-cdd9-427a-8efe-8369304195ad'),
(18, 1, 21, 1, 12, 13, 1, '2019-02-18 08:23:03', '2019-02-18 08:23:03', 'bd7d7165-de2d-4593-8614-459f0e04d9af'),
(19, 1, 22, 1, 14, 15, 1, '2019-02-18 08:23:58', '2019-02-18 08:23:58', '2101f93b-4c62-45c2-b52d-6522935e0dd5'),
(20, 1, 23, 1, 16, 17, 1, '2019-02-18 08:24:19', '2019-02-18 08:24:19', '27d8df52-fae3-4992-a2ec-36c7de94b568'),
(21, 2, 24, 6, 22, 23, 1, '2019-02-20 12:22:05', '2019-02-20 12:22:05', '1a1494f2-15e0-4032-9d39-a6e69b111557'),
(22, 2, 25, 6, 24, 25, 1, '2019-02-20 12:26:24', '2019-02-20 12:26:24', '0a53b787-c074-4a08-8fce-9458e1870e6f');

-- --------------------------------------------------------

--
-- Table structure for table `structures`
--

CREATE TABLE `structures` (
  `id` int(11) NOT NULL,
  `maxLevels` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `structures`
--

INSERT INTO `structures` (`id`, `maxLevels`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, NULL, '2019-02-13 14:13:49', '2019-02-18 07:06:19', NULL, '6c5e095e-48ec-40b1-880a-41bd87a28529'),
(2, NULL, '2019-02-14 10:17:27', '2019-02-18 08:17:47', NULL, 'e521864e-772a-4828-be80-c15f248d77d4');

-- --------------------------------------------------------

--
-- Table structure for table `systemmessages`
--

CREATE TABLE `systemmessages` (
  `id` int(11) NOT NULL,
  `language` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `subject` text NOT NULL,
  `body` text NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `taggroups`
--

CREATE TABLE `taggroups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `deletedWithGroup` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `templatecacheelements`
--

CREATE TABLE `templatecacheelements` (
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `templatecachequeries`
--

CREATE TABLE `templatecachequeries` (
  `id` int(11) NOT NULL,
  `cacheId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `query` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `templatecaches`
--

CREATE TABLE `templatecaches` (
  `id` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `cacheKey` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `id` int(11) NOT NULL,
  `token` char(32) NOT NULL,
  `route` text,
  `usageLimit` tinyint(3) UNSIGNED DEFAULT NULL,
  `usageCount` tinyint(3) UNSIGNED DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usergroups`
--

CREATE TABLE `usergroups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usergroups_users`
--

CREATE TABLE `usergroups_users` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userpermissions`
--

CREATE TABLE `userpermissions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userpermissions_usergroups`
--

CREATE TABLE `userpermissions_usergroups` (
  `id` int(11) NOT NULL,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userpermissions_users`
--

CREATE TABLE `userpermissions_users` (
  `id` int(11) NOT NULL,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userpreferences`
--

CREATE TABLE `userpreferences` (
  `userId` int(11) NOT NULL,
  `preferences` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userpreferences`
--

INSERT INTO `userpreferences` (`userId`, `preferences`) VALUES
(1, '{"language":"en-US","weekStartDay":"0","enableDebugToolbarForSite":false,"enableDebugToolbarForCp":false}');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `photoId` int(11) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `pending` tinyint(1) NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIp` varchar(45) DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(3) UNSIGNED DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `hasDashboard` tinyint(1) NOT NULL DEFAULT '0',
  `verificationCode` varchar(255) DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) DEFAULT NULL,
  `passwordResetRequired` tinyint(1) NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `photoId`, `firstName`, `lastName`, `email`, `password`, `admin`, `locked`, `suspended`, `pending`, `lastLoginDate`, `lastLoginAttemptIp`, `invalidLoginWindowStart`, `invalidLoginCount`, `lastInvalidLoginDate`, `lockoutDate`, `hasDashboard`, `verificationCode`, `verificationCodeIssuedDate`, `unverifiedEmail`, `passwordResetRequired`, `lastPasswordChangeDate`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'admin', NULL, '', '', 'abhinglajia@cygnet-infotech.com', '$2y$13$tlKDyEJqNi8VWf7wR2a2duL3LJtXtCz7qpQuKArNi67G8GjZu2l9.', 1, 0, 0, 0, '2019-02-28 06:04:25', '::1', NULL, NULL, '2019-02-25 12:52:57', NULL, 1, NULL, NULL, NULL, 0, '2019-02-12 06:57:58', '2019-02-12 06:57:58', '2019-02-28 06:04:25', 'c34c1c89-265a-4288-ac23-a0ef7828d843');

-- --------------------------------------------------------

--
-- Table structure for table `volumefolders`
--

CREATE TABLE `volumefolders` (
  `id` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `volumefolders`
--

INSERT INTO `volumefolders` (`id`, `parentId`, `volumeId`, `name`, `path`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, NULL, 1, 'uploads', '', '2019-02-13 14:19:34', '2019-02-13 14:19:34', '017f77dc-6574-4626-b906-c63f842f5b35'),
(2, NULL, NULL, 'Temporary source', NULL, '2019-02-13 14:24:05', '2019-02-13 14:24:05', '0fff4c36-f54f-429a-8257-f9a52df62f7e'),
(3, 2, NULL, 'user_1', 'user_1/', '2019-02-13 14:24:05', '2019-02-13 14:24:05', '528f17c8-6e1b-4f88-ab71-c36e8a8957dd'),
(4, 1, 1, 'public', 'public/', '2019-02-13 14:32:56', '2019-02-13 14:32:56', 'fcf012fa-a482-4ac2-a492-b4d3c44d159a'),
(5, 4, 1, 'assets', 'public/assets/', '2019-02-13 14:32:56', '2019-02-13 14:32:56', '2441f669-47db-4d6f-99ff-80788a19399e'),
(6, 5, 1, 'images', 'public/assets/images/', '2019-02-13 14:32:56', '2019-02-13 14:32:56', 'd36583ce-46ce-4b12-9edc-0cfc888d58fb'),
(7, NULL, 2, 'Object Storage Assets', '', '2019-02-26 14:31:31', '2019-02-26 14:31:31', 'd8a46b06-f061-4da0-87cb-95ec391d8231'),
(8, 7, 2, 'public', 'public/', '2019-02-27 09:21:13', '2019-02-27 09:21:13', 'b492dbc1-d0c6-4b64-a320-9806a283ffa5'),
(9, 8, 2, 'assets', 'public/assets/', '2019-02-27 09:21:13', '2019-02-27 09:21:13', 'd10e861a-2332-443e-a63d-21f58abd8b85'),
(10, 9, 2, 'images', 'public/assets/images/', '2019-02-27 09:21:13', '2019-02-27 09:21:13', '112cfcdb-c4d7-4f39-9b03-d3ef10371859');

-- --------------------------------------------------------

--
-- Table structure for table `volumes`
--

CREATE TABLE `volumes` (
  `id` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(255) DEFAULT NULL,
  `settings` text,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `volumes`
--

INSERT INTO `volumes` (`id`, `fieldLayoutId`, `name`, `handle`, `type`, `hasUrls`, `url`, `settings`, `sortOrder`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 3, 'uploads', 'uploads', 'craft\\volumes\\Local', 1, '@web/public/assets/images/', '{"path":"@webroot/public/assets/images/"}', 1, '2019-02-13 14:19:34', '2019-02-13 14:32:01', NULL, 'fe6fbf03-dfa3-47c3-b97e-e4af5c3181b9'),
(2, NULL, 'Object Storage Assets', 'objectStorageAssets', 'fortrabbit\\ObjectStorage\\Volume', 1, NULL, '{"subfolder":"assets","keyId":"tmharrypotter","secret":"(OBJECT_STORAGE_SECRET)","bucket":"tmharrypotter","region":"(us-east-1|eu-west-1)","expires":"","endpoint":"https://objects.(us1|eu2).frbit.com"}', NULL, '2019-02-26 14:31:31', '2019-02-26 14:31:31', NULL, 'bfe274c4-e8ae-4fdc-9067-b773ff0fe455');

-- --------------------------------------------------------

--
-- Table structure for table `widgets`
--

CREATE TABLE `widgets` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `colspan` tinyint(3) DEFAULT NULL,
  `settings` text,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `widgets`
--

INSERT INTO `widgets` (`id`, `userId`, `type`, `sortOrder`, `colspan`, `settings`, `enabled`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 'craft\\widgets\\RecentEntries', 1, 0, '{"section":"*","siteId":"1","limit":10}', 1, '2019-02-12 06:58:01', '2019-02-12 06:58:01', '113fe56c-4817-4710-8964-5f5f396a1437'),
(2, 1, 'craft\\widgets\\CraftSupport', 2, 0, '[]', 1, '2019-02-12 06:58:01', '2019-02-12 06:58:01', '658842e0-4fa8-42c1-9ddd-c6b8e6464561'),
(3, 1, 'craft\\widgets\\Updates', 3, 0, '[]', 1, '2019-02-12 06:58:01', '2019-02-12 06:58:01', 'acab9db5-6c77-46d0-a069-0e12073cb2c6'),
(4, 1, 'craft\\widgets\\Feed', 4, 0, '{"url":"https://craftcms.com/news.rss","title":"Craft News","limit":5}', 1, '2019-02-12 06:58:01', '2019-02-12 06:58:01', '332971f5-6294-4794-ab96-b2599739bd82');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assetindexdata`
--
ALTER TABLE `assetindexdata`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assetindexdata_sessionId_volumeId_idx` (`sessionId`,`volumeId`),
  ADD KEY `assetindexdata_volumeId_idx` (`volumeId`);

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assets_folderId_idx` (`folderId`),
  ADD KEY `assets_volumeId_idx` (`volumeId`),
  ADD KEY `assets_volumeId_keptFile_idx` (`volumeId`,`keptFile`),
  ADD KEY `assets_filename_folderId_idx` (`filename`,`folderId`);

--
-- Indexes for table `assettransformindex`
--
ALTER TABLE `assettransformindex`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assettransformindex_volumeId_assetId_location_idx` (`volumeId`,`assetId`,`location`);

--
-- Indexes for table `assettransforms`
--
ALTER TABLE `assettransforms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `assettransforms_name_unq_idx` (`name`),
  ADD UNIQUE KEY `assettransforms_handle_unq_idx` (`handle`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_groupId_idx` (`groupId`),
  ADD KEY `categories_parentId_fk` (`parentId`);

--
-- Indexes for table `categorygroups`
--
ALTER TABLE `categorygroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categorygroups_structureId_idx` (`structureId`),
  ADD KEY `categorygroups_fieldLayoutId_idx` (`fieldLayoutId`),
  ADD KEY `categorygroups_dateDeleted_idx` (`dateDeleted`),
  ADD KEY `categorygroups_name_idx` (`name`),
  ADD KEY `categorygroups_handle_idx` (`handle`);

--
-- Indexes for table `categorygroups_sites`
--
ALTER TABLE `categorygroups_sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categorygroups_sites_groupId_siteId_unq_idx` (`groupId`,`siteId`),
  ADD KEY `categorygroups_sites_siteId_idx` (`siteId`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `content_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  ADD KEY `content_siteId_idx` (`siteId`),
  ADD KEY `content_title_idx` (`title`);

--
-- Indexes for table `craftidtokens`
--
ALTER TABLE `craftidtokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craftidtokens_userId_fk` (`userId`);

--
-- Indexes for table `deprecationerrors`
--
ALTER TABLE `deprecationerrors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `deprecationerrors_key_fingerprint_unq_idx` (`key`,`fingerprint`);

--
-- Indexes for table `elementindexsettings`
--
ALTER TABLE `elementindexsettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `elementindexsettings_type_unq_idx` (`type`);

--
-- Indexes for table `elements`
--
ALTER TABLE `elements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `elements_fieldLayoutId_idx` (`fieldLayoutId`),
  ADD KEY `elements_type_idx` (`type`),
  ADD KEY `elements_enabled_idx` (`enabled`),
  ADD KEY `elements_archived_dateCreated_idx` (`archived`,`dateCreated`),
  ADD KEY `elements_dateDeleted_idx` (`dateDeleted`);

--
-- Indexes for table `elements_sites`
--
ALTER TABLE `elements_sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `elements_sites_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  ADD KEY `elements_sites_siteId_idx` (`siteId`),
  ADD KEY `elements_sites_slug_siteId_idx` (`slug`,`siteId`),
  ADD KEY `elements_sites_enabled_idx` (`enabled`),
  ADD KEY `elements_sites_uri_siteId_idx` (`uri`,`siteId`);

--
-- Indexes for table `entries`
--
ALTER TABLE `entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entries_postDate_idx` (`postDate`),
  ADD KEY `entries_expiryDate_idx` (`expiryDate`),
  ADD KEY `entries_authorId_idx` (`authorId`),
  ADD KEY `entries_sectionId_idx` (`sectionId`),
  ADD KEY `entries_typeId_idx` (`typeId`),
  ADD KEY `entries_parentId_fk` (`parentId`);

--
-- Indexes for table `entrydrafts`
--
ALTER TABLE `entrydrafts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entrydrafts_sectionId_idx` (`sectionId`),
  ADD KEY `entrydrafts_entryId_siteId_idx` (`entryId`,`siteId`),
  ADD KEY `entrydrafts_siteId_idx` (`siteId`),
  ADD KEY `entrydrafts_creatorId_idx` (`creatorId`);

--
-- Indexes for table `entrytypes`
--
ALTER TABLE `entrytypes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entrytypes_sectionId_idx` (`sectionId`),
  ADD KEY `entrytypes_fieldLayoutId_idx` (`fieldLayoutId`),
  ADD KEY `entrytypes_dateDeleted_idx` (`dateDeleted`),
  ADD KEY `entrytypes_name_sectionId_idx` (`name`,`sectionId`),
  ADD KEY `entrytypes_handle_sectionId_idx` (`handle`,`sectionId`);

--
-- Indexes for table `entryversions`
--
ALTER TABLE `entryversions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entryversions_sectionId_idx` (`sectionId`),
  ADD KEY `entryversions_entryId_siteId_idx` (`entryId`,`siteId`),
  ADD KEY `entryversions_siteId_idx` (`siteId`),
  ADD KEY `entryversions_creatorId_idx` (`creatorId`);

--
-- Indexes for table `fieldgroups`
--
ALTER TABLE `fieldgroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fieldgroups_name_unq_idx` (`name`);

--
-- Indexes for table `fieldlayoutfields`
--
ALTER TABLE `fieldlayoutfields`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fieldlayoutfields_layoutId_fieldId_unq_idx` (`layoutId`,`fieldId`),
  ADD KEY `fieldlayoutfields_sortOrder_idx` (`sortOrder`),
  ADD KEY `fieldlayoutfields_tabId_idx` (`tabId`),
  ADD KEY `fieldlayoutfields_fieldId_idx` (`fieldId`);

--
-- Indexes for table `fieldlayouts`
--
ALTER TABLE `fieldlayouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fieldlayouts_type_idx` (`type`),
  ADD KEY `fieldlayouts_dateDeleted_idx` (`dateDeleted`);

--
-- Indexes for table `fieldlayouttabs`
--
ALTER TABLE `fieldlayouttabs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fieldlayouttabs_sortOrder_idx` (`sortOrder`),
  ADD KEY `fieldlayouttabs_layoutId_idx` (`layoutId`);

--
-- Indexes for table `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fields_handle_context_unq_idx` (`handle`,`context`),
  ADD KEY `fields_groupId_idx` (`groupId`),
  ADD KEY `fields_context_idx` (`context`);

--
-- Indexes for table `globalsets`
--
ALTER TABLE `globalsets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `globalsets_name_unq_idx` (`name`),
  ADD UNIQUE KEY `globalsets_handle_unq_idx` (`handle`),
  ADD KEY `globalsets_fieldLayoutId_idx` (`fieldLayoutId`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matrixblocks`
--
ALTER TABLE `matrixblocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `matrixblocks_ownerId_idx` (`ownerId`),
  ADD KEY `matrixblocks_fieldId_idx` (`fieldId`),
  ADD KEY `matrixblocks_typeId_idx` (`typeId`),
  ADD KEY `matrixblocks_sortOrder_idx` (`sortOrder`),
  ADD KEY `matrixblocks_ownerSiteId_idx` (`ownerSiteId`);

--
-- Indexes for table `matrixblocktypes`
--
ALTER TABLE `matrixblocktypes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `matrixblocktypes_name_fieldId_unq_idx` (`name`,`fieldId`),
  ADD UNIQUE KEY `matrixblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  ADD KEY `matrixblocktypes_fieldId_idx` (`fieldId`),
  ADD KEY `matrixblocktypes_fieldLayoutId_idx` (`fieldLayoutId`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `migrations_pluginId_idx` (`pluginId`),
  ADD KEY `migrations_type_pluginId_idx` (`type`,`pluginId`);

--
-- Indexes for table `olivemenus`
--
ALTER TABLE `olivemenus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `olivemenus_name_unq_idx` (`name`),
  ADD UNIQUE KEY `olivemenus_handle_unq_idx` (`handle`);

--
-- Indexes for table `olivemenus_items`
--
ALTER TABLE `olivemenus_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `olivemenus_items_menu_id_fk` (`menu_id`);

--
-- Indexes for table `plugins`
--
ALTER TABLE `plugins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plugins_handle_unq_idx` (`handle`);

--
-- Indexes for table `presale_code`
--
ALTER TABLE `presale_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `queue`
--
ALTER TABLE `queue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `queue_fail_timeUpdated_timePushed_idx` (`fail`,`timeUpdated`,`timePushed`),
  ADD KEY `queue_fail_timeUpdated_delay_idx` (`fail`,`timeUpdated`,`delay`);

--
-- Indexes for table `relations`
--
ALTER TABLE `relations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `relations_fieldId_sourceId_sourceSiteId_targetId_unq_idx` (`fieldId`,`sourceId`,`sourceSiteId`,`targetId`),
  ADD KEY `relations_sourceId_idx` (`sourceId`),
  ADD KEY `relations_targetId_idx` (`targetId`),
  ADD KEY `relations_sourceSiteId_idx` (`sourceSiteId`);

--
-- Indexes for table `resourcepaths`
--
ALTER TABLE `resourcepaths`
  ADD PRIMARY KEY (`hash`);

--
-- Indexes for table `searchindex`
--
ALTER TABLE `searchindex`
  ADD PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`siteId`);
ALTER TABLE `searchindex` ADD FULLTEXT KEY `searchindex_keywords_idx` (`keywords`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sections_structureId_idx` (`structureId`),
  ADD KEY `sections_dateDeleted_idx` (`dateDeleted`),
  ADD KEY `sections_name_idx` (`name`),
  ADD KEY `sections_handle_idx` (`handle`);

--
-- Indexes for table `sections_sites`
--
ALTER TABLE `sections_sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sections_sites_sectionId_siteId_unq_idx` (`sectionId`,`siteId`),
  ADD KEY `sections_sites_siteId_idx` (`siteId`);

--
-- Indexes for table `sequences`
--
ALTER TABLE `sequences`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_uid_idx` (`uid`),
  ADD KEY `sessions_token_idx` (`token`),
  ADD KEY `sessions_dateUpdated_idx` (`dateUpdated`),
  ADD KEY `sessions_userId_idx` (`userId`);

--
-- Indexes for table `shunnedmessages`
--
ALTER TABLE `shunnedmessages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `shunnedmessages_userId_message_unq_idx` (`userId`,`message`);

--
-- Indexes for table `sitegroups`
--
ALTER TABLE `sitegroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sitegroups_dateDeleted_idx` (`dateDeleted`),
  ADD KEY `sitegroups_name_idx` (`name`);

--
-- Indexes for table `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sites_sortOrder_idx` (`sortOrder`),
  ADD KEY `sites_groupId_fk` (`groupId`),
  ADD KEY `sites_dateDeleted_idx` (`dateDeleted`),
  ADD KEY `sites_handle_idx` (`handle`);

--
-- Indexes for table `structureelements`
--
ALTER TABLE `structureelements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `structureelements_structureId_elementId_unq_idx` (`structureId`,`elementId`),
  ADD KEY `structureelements_root_idx` (`root`),
  ADD KEY `structureelements_lft_idx` (`lft`),
  ADD KEY `structureelements_rgt_idx` (`rgt`),
  ADD KEY `structureelements_level_idx` (`level`),
  ADD KEY `structureelements_elementId_idx` (`elementId`);

--
-- Indexes for table `structures`
--
ALTER TABLE `structures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `structures_dateDeleted_idx` (`dateDeleted`);

--
-- Indexes for table `systemmessages`
--
ALTER TABLE `systemmessages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `systemmessages_key_language_unq_idx` (`key`,`language`),
  ADD KEY `systemmessages_language_idx` (`language`);

--
-- Indexes for table `taggroups`
--
ALTER TABLE `taggroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `taggroups_fieldLayoutId_fk` (`fieldLayoutId`),
  ADD KEY `taggroups_dateDeleted_idx` (`dateDeleted`),
  ADD KEY `taggroups_name_idx` (`name`),
  ADD KEY `taggroups_handle_idx` (`handle`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tags_groupId_idx` (`groupId`);

--
-- Indexes for table `templatecacheelements`
--
ALTER TABLE `templatecacheelements`
  ADD KEY `templatecacheelements_cacheId_idx` (`cacheId`),
  ADD KEY `templatecacheelements_elementId_idx` (`elementId`);

--
-- Indexes for table `templatecachequeries`
--
ALTER TABLE `templatecachequeries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `templatecachequeries_cacheId_idx` (`cacheId`),
  ADD KEY `templatecachequeries_type_idx` (`type`);

--
-- Indexes for table `templatecaches`
--
ALTER TABLE `templatecaches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `templatecaches_cacheKey_siteId_expiryDate_path_idx` (`cacheKey`,`siteId`,`expiryDate`,`path`),
  ADD KEY `templatecaches_cacheKey_siteId_expiryDate_idx` (`cacheKey`,`siteId`,`expiryDate`),
  ADD KEY `templatecaches_siteId_idx` (`siteId`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tokens_token_unq_idx` (`token`),
  ADD KEY `tokens_expiryDate_idx` (`expiryDate`);

--
-- Indexes for table `usergroups`
--
ALTER TABLE `usergroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usergroups_handle_unq_idx` (`handle`),
  ADD UNIQUE KEY `usergroups_name_unq_idx` (`name`);

--
-- Indexes for table `usergroups_users`
--
ALTER TABLE `usergroups_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usergroups_users_groupId_userId_unq_idx` (`groupId`,`userId`),
  ADD KEY `usergroups_users_userId_idx` (`userId`);

--
-- Indexes for table `userpermissions`
--
ALTER TABLE `userpermissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userpermissions_name_unq_idx` (`name`);

--
-- Indexes for table `userpermissions_usergroups`
--
ALTER TABLE `userpermissions_usergroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userpermissions_usergroups_permissionId_groupId_unq_idx` (`permissionId`,`groupId`),
  ADD KEY `userpermissions_usergroups_groupId_idx` (`groupId`);

--
-- Indexes for table `userpermissions_users`
--
ALTER TABLE `userpermissions_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userpermissions_users_permissionId_userId_unq_idx` (`permissionId`,`userId`),
  ADD KEY `userpermissions_users_userId_idx` (`userId`);

--
-- Indexes for table `userpreferences`
--
ALTER TABLE `userpreferences`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_uid_idx` (`uid`),
  ADD KEY `users_verificationCode_idx` (`verificationCode`),
  ADD KEY `users_email_idx` (`email`),
  ADD KEY `users_username_idx` (`username`),
  ADD KEY `users_photoId_fk` (`photoId`);

--
-- Indexes for table `volumefolders`
--
ALTER TABLE `volumefolders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `volumefolders_name_parentId_volumeId_unq_idx` (`name`,`parentId`,`volumeId`),
  ADD KEY `volumefolders_parentId_idx` (`parentId`),
  ADD KEY `volumefolders_volumeId_idx` (`volumeId`);

--
-- Indexes for table `volumes`
--
ALTER TABLE `volumes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `volumes_fieldLayoutId_idx` (`fieldLayoutId`),
  ADD KEY `volumes_dateDeleted_idx` (`dateDeleted`),
  ADD KEY `volumes_name_idx` (`name`),
  ADD KEY `volumes_handle_idx` (`handle`);

--
-- Indexes for table `widgets`
--
ALTER TABLE `widgets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `widgets_userId_idx` (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assetindexdata`
--
ALTER TABLE `assetindexdata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `assettransformindex`
--
ALTER TABLE `assettransformindex`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `assettransforms`
--
ALTER TABLE `assettransforms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categorygroups`
--
ALTER TABLE `categorygroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categorygroups_sites`
--
ALTER TABLE `categorygroups_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `craftidtokens`
--
ALTER TABLE `craftidtokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `deprecationerrors`
--
ALTER TABLE `deprecationerrors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `elementindexsettings`
--
ALTER TABLE `elementindexsettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `elements`
--
ALTER TABLE `elements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `elements_sites`
--
ALTER TABLE `elements_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `entrydrafts`
--
ALTER TABLE `entrydrafts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `entrytypes`
--
ALTER TABLE `entrytypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `entryversions`
--
ALTER TABLE `entryversions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `fieldgroups`
--
ALTER TABLE `fieldgroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `fieldlayoutfields`
--
ALTER TABLE `fieldlayoutfields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `fieldlayouts`
--
ALTER TABLE `fieldlayouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `fieldlayouttabs`
--
ALTER TABLE `fieldlayouttabs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `globalsets`
--
ALTER TABLE `globalsets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `matrixblocktypes`
--
ALTER TABLE `matrixblocktypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;
--
-- AUTO_INCREMENT for table `olivemenus`
--
ALTER TABLE `olivemenus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `olivemenus_items`
--
ALTER TABLE `olivemenus_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `plugins`
--
ALTER TABLE `plugins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `presale_code`
--
ALTER TABLE `presale_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `queue`
--
ALTER TABLE `queue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `relations`
--
ALTER TABLE `relations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;
--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sections_sites`
--
ALTER TABLE `sections_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `shunnedmessages`
--
ALTER TABLE `shunnedmessages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sitegroups`
--
ALTER TABLE `sitegroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sites`
--
ALTER TABLE `sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `structureelements`
--
ALTER TABLE `structureelements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `structures`
--
ALTER TABLE `structures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `systemmessages`
--
ALTER TABLE `systemmessages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `taggroups`
--
ALTER TABLE `taggroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `templatecachequeries`
--
ALTER TABLE `templatecachequeries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `templatecaches`
--
ALTER TABLE `templatecaches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usergroups`
--
ALTER TABLE `usergroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usergroups_users`
--
ALTER TABLE `usergroups_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `userpermissions`
--
ALTER TABLE `userpermissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `userpermissions_usergroups`
--
ALTER TABLE `userpermissions_usergroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `userpermissions_users`
--
ALTER TABLE `userpermissions_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `userpreferences`
--
ALTER TABLE `userpreferences`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `volumefolders`
--
ALTER TABLE `volumefolders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `volumes`
--
ALTER TABLE `volumes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `widgets`
--
ALTER TABLE `widgets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `assetindexdata`
--
ALTER TABLE `assetindexdata`
  ADD CONSTRAINT `assetindexdata_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `assets`
--
ALTER TABLE `assets`
  ADD CONSTRAINT `assets_folderId_fk` FOREIGN KEY (`folderId`) REFERENCES `volumefolders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assets_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `categories_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `categories_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `categories` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `categorygroups`
--
ALTER TABLE `categorygroups`
  ADD CONSTRAINT `categorygroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `categorygroups_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categorygroups_sites`
--
ALTER TABLE `categorygroups_sites`
  ADD CONSTRAINT `categorygroups_sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `categorygroups_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `content`
--
ALTER TABLE `content`
  ADD CONSTRAINT `content_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `content_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `craftidtokens`
--
ALTER TABLE `craftidtokens`
  ADD CONSTRAINT `craftidtokens_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `elements`
--
ALTER TABLE `elements`
  ADD CONSTRAINT `elements_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `elements_sites`
--
ALTER TABLE `elements_sites`
  ADD CONSTRAINT `elements_sites_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `elements_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `entries`
--
ALTER TABLE `entries`
  ADD CONSTRAINT `entries_authorId_fk` FOREIGN KEY (`authorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `entries_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `entries_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `entries` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `entries_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `entries_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `entrytypes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `entrydrafts`
--
ALTER TABLE `entrydrafts`
  ADD CONSTRAINT `entrydrafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `entrydrafts_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `entries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `entrydrafts_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `entrydrafts_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `entrytypes`
--
ALTER TABLE `entrytypes`
  ADD CONSTRAINT `entrytypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `entrytypes_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `entryversions`
--
ALTER TABLE `entryversions`
  ADD CONSTRAINT `entryversions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `entryversions_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `entries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `entryversions_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `entryversions_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fieldlayoutfields`
--
ALTER TABLE `fieldlayoutfields`
  ADD CONSTRAINT `fieldlayoutfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fieldlayoutfields_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fieldlayoutfields_tabId_fk` FOREIGN KEY (`tabId`) REFERENCES `fieldlayouttabs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fieldlayouttabs`
--
ALTER TABLE `fieldlayouttabs`
  ADD CONSTRAINT `fieldlayouttabs_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fields`
--
ALTER TABLE `fields`
  ADD CONSTRAINT `fields_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `fieldgroups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `globalsets`
--
ALTER TABLE `globalsets`
  ADD CONSTRAINT `globalsets_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `globalsets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `matrixblocks`
--
ALTER TABLE `matrixblocks`
  ADD CONSTRAINT `matrixblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matrixblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matrixblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matrixblocks_ownerSiteId_fk` FOREIGN KEY (`ownerSiteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `matrixblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `matrixblocktypes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `matrixblocktypes`
--
ALTER TABLE `matrixblocktypes`
  ADD CONSTRAINT `matrixblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matrixblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `migrations`
--
ALTER TABLE `migrations`
  ADD CONSTRAINT `migrations_pluginId_fk` FOREIGN KEY (`pluginId`) REFERENCES `plugins` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `olivemenus_items`
--
ALTER TABLE `olivemenus_items`
  ADD CONSTRAINT `olivemenus_items_menu_id_fk` FOREIGN KEY (`menu_id`) REFERENCES `olivemenus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `relations`
--
ALTER TABLE `relations`
  ADD CONSTRAINT `relations_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `relations_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `relations_sourceSiteId_fk` FOREIGN KEY (`sourceSiteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `relations_targetId_fk` FOREIGN KEY (`targetId`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sections`
--
ALTER TABLE `sections`
  ADD CONSTRAINT `sections_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `sections_sites`
--
ALTER TABLE `sections_sites`
  ADD CONSTRAINT `sections_sites_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sections_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sessions`
--
ALTER TABLE `sessions`
  ADD CONSTRAINT `sessions_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `shunnedmessages`
--
ALTER TABLE `shunnedmessages`
  ADD CONSTRAINT `shunnedmessages_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sites`
--
ALTER TABLE `sites`
  ADD CONSTRAINT `sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `sitegroups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `structureelements`
--
ALTER TABLE `structureelements`
  ADD CONSTRAINT `structureelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `structureelements_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `taggroups`
--
ALTER TABLE `taggroups`
  ADD CONSTRAINT `taggroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `tags_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `taggroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tags_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `templatecacheelements`
--
ALTER TABLE `templatecacheelements`
  ADD CONSTRAINT `templatecacheelements_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `templatecacheelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `templatecachequeries`
--
ALTER TABLE `templatecachequeries`
  ADD CONSTRAINT `templatecachequeries_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `templatecaches`
--
ALTER TABLE `templatecaches`
  ADD CONSTRAINT `templatecaches_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `usergroups_users`
--
ALTER TABLE `usergroups_users`
  ADD CONSTRAINT `usergroups_users_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `usergroups_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `userpermissions_usergroups`
--
ALTER TABLE `userpermissions_usergroups`
  ADD CONSTRAINT `userpermissions_usergroups_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `userpermissions_usergroups_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `userpermissions_users`
--
ALTER TABLE `userpermissions_users`
  ADD CONSTRAINT `userpermissions_users_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `userpermissions_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `userpreferences`
--
ALTER TABLE `userpreferences`
  ADD CONSTRAINT `userpreferences_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_photoId_fk` FOREIGN KEY (`photoId`) REFERENCES `assets` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `volumefolders`
--
ALTER TABLE `volumefolders`
  ADD CONSTRAINT `volumefolders_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `volumefolders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `volumefolders_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `volumes`
--
ALTER TABLE `volumes`
  ADD CONSTRAINT `volumes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `widgets`
--
ALTER TABLE `widgets`
  ADD CONSTRAINT `widgets_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
