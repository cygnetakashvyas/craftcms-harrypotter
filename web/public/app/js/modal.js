$(document).ready(function () {
      var availableMonths = [ ["04","2019","April"],["05","2019","Mai"],["06","2019","Juni"],["07","2019","Juli"] ];
      renderMonthSwitch();
      $('.ajax-data').on('click', function () {
        $('#myModal').modal('show');

//     $('#myModal .modal-right-block').bind('scroll', function()
// 	  {

// 	  	$('#heading-txt').parent().children().hide();
//         $('#modal-para-txt').hide();

//         // $('#close-button img').addClass('modal-closebtn');

// /*	    if($(this).scrollTop() + $(this).innerHeight()>=$(this)[0].scrollHeight)
// 	    {
// 	      alert('end reached');
// 	    }*/
// 	    if($(this).scrollTop() == 0){
// 	   	$('#heading-txt').show();
//         $('#modal-para-txt').show();
//         // $('#close-button img').removeClass('modal-closebtn');
// 	    }
// 	  })
        var tagType = $(this).attr('data-id');
        $(".class-drop-down").attr("data-ticket", tagType);

        if(tagType == "HPKOMBI")
        {
          var modaltitle_content = $("#kombimodaltitle").html();
          var modaltitle_desc = $("#kombimodaldescription").html();
          $(".modal-title").html("");
          $("#modal-para-txt").html("");
          $(".modal-title").append(modaltitle_content);
          $("#modal-para-txt").append(modaltitle_desc);
        }
        if(tagType == "HPFLEX")
        {
          var modaltitle_content = $("#individualmodaltitle").html();
          var modaltitle_desc = $("#individualmodaldescription").html();
          $(".modal-title").html("");
          $("#modal-para-txt").html("");
          $(".modal-title").append(modaltitle_content);
          $("#modal-para-txt").append(modaltitle_desc);
        }
        if(tagType == "HPEINZEL")
        {
          var modaltitle_content = $("#singlemodaltitle").html();
          var modaltitle_desc = $("#singlemodaldescription").html();
          $(".modal-title").html("");
          $("#modal-para-txt").html("");
          $(".modal-title").append(modaltitle_content);
          $("#modal-para-txt").append(modaltitle_desc);
        }

        if($(".footer").hasClass("0")){
          var url = "/public/sample_json/"+tagType+".json"; //$(this).attr('data-local-json');
        } else {
          var url = "https://app.ticketmaster.eu/mfxapi/v2/events?apikey=3f2vu5BiyL4S96uvhhuRqDDdClwERqrB&domain=germany&tag=" + tagType + "&attraction_ids=987404&rows=250" ; //$(this).attr('data-live-json');
        }

        //var url = "https://app.ticketmaster.eu/mfxapi/v2/events?apikey=3f2vu5BiyL4S96uvhhuRqDDdClwERqrB&domain=germany&tag=" + tagType + "&attraction_ids=987404&rows=250";

        if (tagType == 'HPEINZEL') {
          var ModalTitle = $('.modal_tag_title').html('Single Ticket<span class="d-none d-sm-block">Lorem Ipsum is simply dummy text of the printing</span>');
        } else if(tagType == 'HPKOMBI') {
          var ModalTitle = $('.modal_tag_title').html('Kombi Ticket<span class="d-none d-sm-block">Lorem Ipsum is simply dummy text of the printing</span>');
        } else {
          var ModalTitle = $('.modal_tag_title').html('Individual Ticket<span class="d-none d-sm-block">Lorem Ipsum is simply dummy text of the printing</span>');
        }

        getData(url, tagType);
      });

      $("#close-button").on('click',function(){
          $("#month-value").html(availableMonths[0][2]+" "+availableMonths[0][1]+' <div class="arrow-down">'); //$("#month-value").html(`April 2019<div class="arrow-down">`);
          $('.cal-table .cal-data').addClass('stripe-4');
      });

      $('.class-drop-down').on('click', function () {
        $('.cal-table .cal-data').addClass('stripe-4');
        $('#mobile-render').html('');
        var tagType = $(this).attr('data-ticket');
        var month = $(this).attr('data-date-value');
        var showMonth = month.split('-');
        showMonthName = monthNames[showMonth[0] - 1];

        $("#month-value").html(`${showMonthName} ${showMonth[1]} <div class="arrow-down">`);

        //var url = "https://app.ticketmaster.eu/mfxapi/v2/events?apikey=3f2vu5BiyL4S96uvhhuRqDDdClwERqrB&domain=germany&tag=" + tagType + "&attraction_ids=987404&rows=250";
        //var url = $(this).attr('data-local-json');

        if($(".footer").hasClass("0")){
          var url = "/public/sample_json/"+tagType+".json"; //$(this).attr('data-local-json');
        } else {
          var url = "https://app.ticketmaster.eu/mfxapi/v2/events?apikey=3f2vu5BiyL4S96uvhhuRqDDdClwERqrB&domain=germany&tag=" + tagType + "&attraction_ids=987404&rows=250" ; //$(this).attr('data-live-json');
        }

        getData(url, tagType, month);
      });

      const monthNames = ["Januar", "Februar", "Marz", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"];

      function getData(url, tagType, month) {
        if (month == undefined) {
          month = availableMonths[0][0]+"-"+availableMonths[0][1]; // '05-2019'; Set default month on calender load
        }
        var premiereDate = '5Mai'; // Current premier date for testing
        function getWeekNumber(date) {
          var monthStartDate = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
          monthStartDate = new Date(monthStartDate);
          var day = monthStartDate.getDay();
          date = new Date(date);
          var date = date.getDate();

          let weekNumber = Math.floor((date + (day)) / 7);
          return (weekNumber == 0) ? 1 : weekNumber;
        }

        //remove custome classes on load calendar
        removeCustomClasses();

        $.ajax({
          url: url,
          type: "GET",
          success: function (response) {
            $('body .cal-data').html('');

            if (response.events) {
              for (var i = 0; i < response.events.length; i++) {
                if (response.events[i]['event_date']) {

                  // Availability calculation start
                   var priceTag = `<i class="circle-icon green-crcl"></i>`;
                    var isAvailable = 1;
                  // var priceUrl = "https://app.ticketmaster.eu/mfxapi/v2/events/" + response.events[i]['id'] + "/prices?price_level_ids&apikey=3f2vu5BiyL4S96uvhhuRqDDdClwERqrB&domain=germany";
                  // var priceDataResponse = '';
                  // $.ajax({
                  //   url: priceUrl,
                  //   type: "GET",
                  //   async:false,
                  //   success: function (priceResponse) {
                  //     if (priceResponse && priceResponse.event && priceResponse.event.price_types[0] && priceResponse.event.price_types[0].price_levels) {
                  //       priceDataResponse = priceResponse.event.price_types[0].price_levels;
                  //     }
                  //   }
                  // });

                  // //var priceTag = `<i class="circle-icon green-crcl"></i>`;
                  // if (priceDataResponse) {
                  //   var pricrCount = {
                  //     'high' : 0,
                  //     'limited' : 0,
                  //     'none' : 0
                  //   };
                  //   var high = 0;
                  //   var limited = 0;
                  //   var none = 0;

                  //   $.each(priceDataResponse, function(i, item) {
                  //     if (item.availability && item.availability == 'high') {
                  //       pricrCount['high'] = ++high;
                  //     } else if (item.availability && item.availability == 'limited') {
                  //       pricrCount['limited'] = ++limited;
                  //     } else {
                  //       pricrCount['none'] = ++none;
                  //     }
                  //   });

                  //   var highCount = pricrCount.high;
                  //   var limitedCount = pricrCount.limited;
                  //   var noneCount = pricrCount.none;

                  //   if(highCount >= noneCount && highCount >= limitedCount){
                  //       priceTag = `<i class="circle-icon green-crcl"></i>`;
                  //   }

                  //   if(limitedCount >= highCount && limitedCount >= noneCount){
                  //       priceTag = `<i class="circle-icon yellow-crcl"></i>`;
                  //   }

                  //   if(noneCount >= highCount && noneCount >= limitedCount){
                  //       priceTag = `<i class="circle-icon red-crcl"></i>`;
                  //       isAvailable = 0;
                  //   }
                  // }
                  // Availability calculation end


                  // Show events details in calender start
                  var eventDates = response.events[i]['event_date']['value'];
                  eventDates = eventDates.replace("T", " ");
                  eventDates = eventDates.replace("Z", "");

                  var monthYearDate = moment(eventDates, "YYYY-MM-DD");
                  var yearNumber = monthYearDate.format('YYYY');
                  yearNumber = yearNumber.toString();
                  var monthNumber = monthYearDate.format('MM');
                  monthNumber = monthNumber.toString();
                  var monthYearCheck = monthNumber + '-' + yearNumber;

                  if (month && (month != monthYearCheck)) {
                    continue;
                  }

                  var day = moment(eventDates).day();
                  var week = getWeekNumber(eventDates);

                  // week = Math.ceil(week.date() / 7);

                  var weekOfDay = response.events[i]['day_of_week'];

                  var className = '';
                  var combine = 'cal-text';
                  var nextCell ='';
                  if (weekOfDay == 'Donnerstag'  && tagType === "HPKOMBI") {
                    combine = 'cal-text cell-combine';
                  }
                  if (week == 1) {
                    className = `#r1_${weekOfDay}`;
                    if (weekOfDay == 'Donnerstag') {
                      nextCell = '#r1_Freitag';
                    }
                  } else if (week == 2) {
                    className = `#r2_${weekOfDay}`;
                    if (weekOfDay == 'Donnerstag') {
                      nextCell = '#r2_Freitag';
                    }
                  } else if (week == 3) {
                    className = `#r3_${weekOfDay}`;
                    if (weekOfDay == 'Donnerstag') {
                      nextCell = '#r3_Freitag';
                    }
                  } else if (week == 4) {
                    className = `#r4_${weekOfDay}`;
                    if (weekOfDay == 'Donnerstag') {
                      nextCell = '#r4_Freitag';
                    }
                  } else if (week == 5) {
                    className = `#r5_${weekOfDay}`;
                    if (weekOfDay == 'Donnerstag') {
                      nextCell = '#r5_Freitag';
                    }
                  } else if (week == 6) {
                    className = `#r6_${weekOfDay}`;
                    if (weekOfDay == 'Donnerstag') {
                      nextCell = '#r6_Freitag';
                    }
                  } else if (week == 7) {
                    className = `#r7_${weekOfDay}`;
                    if (weekOfDay == 'Donnerstag') {
                      nextCell = '#r7_Freitag';
                    }
                  }
                  var url = response.events[i]['url'];

                  var eventDate = response.events[i]['event_date']['value'];
                  eventDate = eventDate.replace("T", " ");
                  eventDate = eventDate.replace("Z", "");

                  var dateConverted = new Date(eventDate);

                  dayOfDate = dateConverted.getDate();

                  var monthOfDate = dateConverted.getMonth();

                  monthOfDate = monthNames[monthOfDate];
                  var name = '';

                  var mobileClassFirst = '';
                  if (i == 0) {
                    mobileClassFirst = 'cal-bookticket';
                  }
                  var mobileDayName = weekOfDay.substring(0, 2);

                  var mobileAdd = '';

                  if (weekOfDay == 'Donnerstag' && tagType === "HPKOMBI") {
                    name = `Teil Eins + Teil Zwei <br/>
                      <span>${dayOfDate}. ${monthOfDate} 19:30 + <br/>${dayOfDate + 1}. ${monthOfDate} 19:30 Uhr </span>`;
                    mobileAdd = `<div class="mob-cal-row">
                      <div class="mob-cal-cell" style="width: 55%;">
                        <p class="cal-text">${priceTag}
                          <span>${mobileDayName} ${dayOfDate}.${monthOfDate}.19:30 +<br />
                            Fr ${dayOfDate + 1}.${monthOfDate}.19:30 Uhr
                          </span>
                        </p>
                      </div>
                      <div class="mob-cal-cell" style="width: 43%;">
                        <p class="cal-text text-right mobileIframe">`;
                        if (isAvailable == 0) {
                          mobileAdd += `Teil Eins <br /> Teil Zwei`;
                        } else {
                          mobileAdd += `<a class="nameDetails" data-iframe-url="${url}"> Teil Eins <br /> Teil Zwei </a>`;
                        }
                      mobileAdd += `</p>
                      </div>
                      <div class="mob-cal-cell" style="width: 2%;">
                        <img src="/public/app/images/back.png">
                      </div>
                    </div>`;
                  } else {
                    if (weekOfDay == 'Freitag' && tagType === "HPKOMBI") {
                      continue; // skip friday events as we are showing thursday & friday events in combined view
                    }

                    var part1Time = '14:30';
                    var part2Time = '19:30';
                    if (weekOfDay == 'Sonntag') {
                      part1Time = '14:00';
                      part2Time = '19:00';
                    }

                    if (tagType === "HPKOMBI") {
                      //kombi
                      // name = response.events[i]['name'];
                      name = `Teil Eins + Teil Zwei<br/>
                              <span>${dayOfDate}. ${monthOfDate}<br/> ${part1Time} + ${part2Time} Uhr</span>`;
                      mobileAdd = `<div class="mob-cal-row ${mobileClassFirst}">
                          <div class="mob-cal-cell" style="width: 55%;">
                            <p class="cal-text">${priceTag}
                              <span>${mobileDayName} ${dayOfDate}.${monthOfDate}<br />${part1Time} + ${part2Time} Uhr
                              </span>
                            </p>
                          </div>
                          <div class="mob-cal-cell" style="width: 43%;">
                            <p class="cal-text text-right mobileIframe">`;
                            if (isAvailable == 0) {
                              mobileAdd += `Teil Eins + Teil Zwei`;
                            } else {
                              mobileAdd += `<a class="nameDetails" data-iframe-url="${url}"> Teil Eins + Teil Zwei </a>`;
                            }
                            mobileAdd += `</p>
                          </div>
                          <div class="mob-cal-cell" style="width: 2%;">
                            <img src="/public/app/images/back.png">
                          </div>
                        </div>`;
                    } else if (tagType === "HPEINZEL") {
                      //single event
                      event_name = response.events[i]['name'];
                      eName = 'Teil Eins';
                      eTime = (weekOfDay == 'Sonntag') ? '14:00' : '14:30';
                      if(event_name.indexOf('Teil Zwei') != -1){
                        eName = 'Teil Zwei';
                        eTime = (weekOfDay == 'Sonntag') ? '19:00' : '19:30';
                      }

                      name = `${eName}<br/>
                            <span>${dayOfDate}. ${monthOfDate}<br/> ${eTime} Uhr</span>`;
                      mobileAdd = `<div class="mob-cal-row ${mobileClassFirst}">
                          <div class="mob-cal-cell" style="width: 55%;">
                            <p class="cal-text">${priceTag}
                              <span>${mobileDayName} ${dayOfDate}.${monthOfDate}<br />${eTime} Uhr
                              </span>
                            </p>
                          </div>
                          <div class="mob-cal-cell" style="width: 43%;">
                            <p class="cal-text text-right mobileIframe">`;
                            if (isAvailable == 0) {
                              mobileAdd += `${eName}`;
                            } else {
                              mobileAdd += `<a class="nameDetails" data-iframe-url="${url}"> ${eName} </a>`;
                            }

                        mobileAdd += `</p>
                          </div>
                          <div class="mob-cal-cell" style="width: 2%;">
                            <img src="/public/app/images/back.png">
                          </div>
                        </div>`;
                    } else {
                      //individual event
                      event_name = response.events[i]['name'];
                      eName = 'Teil Eins';
                      eTime = (weekOfDay == 'Sonntag') ? '14:00' : '14:30';
                      if(event_name.indexOf('Teil Zwei') != -1){
                        eName = 'Teil Zwei';
                        eTime = (weekOfDay == 'Sonntag') ? '19:00' : '19:30';
                      }

                      name = `${eName}<br/>
                            <span>${dayOfDate}. ${monthOfDate}<br/> ${eTime} Uhr</span>`;
                      mobileAdd = `<div class="mob-cal-row ${mobileClassFirst}">
                          <div class="mob-cal-cell" style="width: 55%;">
                            <p class="cal-text">${priceTag}
                              <span>${mobileDayName} ${dayOfDate}.${monthOfDate}<br />${eTime} Uhr
                              </span>
                            </p>
                          </div>
                          <div class="mob-cal-cell" style="width: 43%;">
                            <p class="cal-text text-right mobileIframe">`;
                            if (isAvailable == 0) {
                              mobileAdd += `${eName}`;
                            } else {
                              mobileAdd += `<a class="nameDetails" data-iframe-url="${url}"> ${eName} </a>`;
                            }

                            mobileAdd += `</p>
                          </div>
                          <div class="mob-cal-cell" style="width: 2%;">
                            <img src="/public/app/images/back.png">
                          </div>
                        </div>`;
                    }
                  }

                  // Show premiere event
                  var dateMonthCombination = `${dayOfDate}${monthOfDate}`;
                  if (premiereDate == dateMonthCombination) {
                    $(className).addClass('premiere');
                  }

                  /*For mobile view : START */
                  $('#mobile-render').append(mobileAdd);
                  /*For mobile view : END */
                  var appendData = '';
                  if (isAvailable == 0) {
                    appendData = `<div class="schedule-col schedule-data" data-event-date="${dayOfDate}"> <p class="${combine}">${priceTag} ${name}</p></div>`;
                  } else {
                    appendData = `<a class="nameDetails" data-iframe-url="${url}"> <div class="schedule-col schedule-data" data-event-date="${dayOfDate}"> <p class="${combine}">${priceTag} ${name}</p></div> </a>`;
                  }
                  if (tagType == 'HPEINZEL') {
                    $(className).append(appendData);
                    $(className).removeClass('stripe-4');
                    if (weekOfDay == 'Donnerstag') {
                      $(className).removeClass('stripe-4');
                      $(nextCell).removeClass('stripe-4');
                    }
                  } else {
                    $(className).append(appendData);
                    $(className).removeClass('stripe-4');
                    $(nextCell).removeClass('stripe-4');
                  }
                  // Show events details in calender end
                }
              }
              //add before-premiere class to all before event from premiere event
              var premiereDiv = $("div").hasClass("premiere");
              if (premiereDiv) {
                var premiereEventDate = $(".premiere").find(".schedule-data").attr("data-event-date");
                $(".cal-data").map(function() {
                  if ($(this).find(".nameDetails").length > 0){
                    var currentEventDate = $(this).find(".nameDetails").find(".schedule-data").attr("data-event-date");
                    if (currentEventDate < premiereEventDate) {
                      var currentEventClass = $(this).attr('id');
                      var splitCurrentEventClass = currentEventClass.split('_');
                      if (splitCurrentEventClass[1] == 'Donnerstag') {
                        $('#'+splitCurrentEventClass[0]+'_Freitag').addClass('before-premiere');
                      } else {
                        $(this).addClass('before-premiere');
                      }
                    }
                  }
                });

              }
            }
          }
        });
      }

      // $('.cal-data').click(function () {

      //   if (!$(this).hasClass('stripe-4')) {

      //     var iframeUrl = $(this).find(".nameDetails").first().attr("data-iframe-url"),
      //       dateTime = $(this).children().eq(1).find(".nameDetails").first().text();
      //     console.log(dateTime);
      //     $("#iframe-ticketmaster").attr("src", iframeUrl + "&brand=de_harry_potter");
      //     $('#cal-block').hide();
      //     $('#ticket-iframe .ticet-iframe-head .ticket-time-first').html(dateTime);
      //     $('#ticket-iframe').show();
      //   } else {
      //     $('#cal-block').show();
      //     $('#ticket-iframe').hide();
      //   }

      // });

      // $(document).on('click', '.mobileIframe', function () {
      //   var iframeUrl = $(this).find(".nameDetails").first().attr("data-iframe-url"),
      //     dateTime = $(this).children().eq(1).find(".nameDetails").first().text();

      //   $("#iframe-ticketmaster").attr("src", iframeUrl + "&brand=de_harry_potter");
      //   $('#cal-block').hide();
      //   $('#ticket-iframe .ticet-iframe-head .ticket-time-first').html(dateTime);
      //   $('#ticket-iframe').show();
      // });

      $(document).on('click', '.nameDetails', function () {
        var iframeUrl = $(this).attr("data-iframe-url"),
          dateTime = $(this).children().eq(1).find(".nameDetails").first().text();

        $("#iframe-ticketmaster").attr("src", iframeUrl + "&brand=de_harry_potter");
        $('#cal-block').hide();
        $('#ticket-iframe .ticet-iframe-head .ticket-time-first').html(dateTime);
        $('#ticket-iframe').show();
      });


      $('#myModal').on('hide.bs.modal', function () {
        $('#cal-block').show();
        $('#ticket-iframe').hide();

      })
      $('#scroll-down-btn').click(function () {
        $('#heading-txt').parent().children().hide();
        $('#modal-para-txt').hide();
        $('#scroll-down-btn').hide();
        $('#scroll-top-btn').show();
        $('#close-button img').addClass('modal-closebtn');
      });
      $('#scroll-top-btn').click(function () {
        $('#heading-txt').show();
        $('#modal-para-txt').show();
        $('#scroll-top-btn').hide();
        $('#scroll-down-btn').show();
        $('#close-button img').removeClass('modal-closebtn');
      });

      // Method to genrate month selector based on available months
      function renderMonthSwitch(){
        var htmlContent = '';
        for (i = 0; i < availableMonths.length; i++) {
          var dateValue = availableMonths[i][0]+"-"+availableMonths[i][1];
          var dateText = availableMonths[i][2]+" "+availableMonths[i][1];

          htmlContent += '<a href="javascript:void(0);" class="class-drop-down" data-ticket="" data-date-value="'+dateValue+'">'+dateText+'</a>';

        }
        $("#month-value").html(availableMonths[0][2]+" "+availableMonths[0][1]+' <div class="arrow-down">');
        $("#parent-dropdown").html(htmlContent);
      }

      // Method to remove premiere, before-premiere classes
      function removeCustomClasses(){
        $('.cal-data').removeClass('premiere');
        $('.cal-data').removeClass('before-premiere');
      }
    });