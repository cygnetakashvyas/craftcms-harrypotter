$(document).ready(function () {

  const second = 1000,
    minute = second * 60,
    hour = minute * 60
  day = hour * 24;

  let countDown = new Date('March 19, 2019 10:00:00 GMT-01:00').getTime(),
    x = setInterval(function () {

      let now = new Date().getTime(),
        distance = countDown - now;

      document.getElementById('days').innerText = Math.floor(distance / (day)),
      document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
        document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
        document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);

        //do something later when date is reached
        if (distance < 0) {
          clearInterval(x);
          $('#timercountdown').hide();
          $('#presale-ticket').hide();
          $('#presale-ticket-code').show();
        }


    }, second)

    $('#timer-block').click(function () {
      $('#timercountdown').hide();
      $('#presale-ticket').hide();
      $('#presale-ticket-code').show();
    });
    });