'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var order = require("gulp-order");
var cssbeautify = require('gulp-cssbeautify');
 var sassFiles = 'app/scss/style.scss';
var cssDest = 'app/css/';
var jsFiles = 'app/js/*.js';
var jsDest = 'app/js/';

 gulp.task('sass', function(){
     gulp.src(sassFiles)
         .pipe(sass().on('error', sass.logError))
         .pipe(cssbeautify())
         .pipe(concat('style.css'))
         .pipe(gulp.dest(cssDest));
 });

 gulp.task('js', function(){
  gulp.src(jsFiles)
  .pipe(order([
    'app/js/jquery-3.2.1.slim.min.js', 
    'app/js/popper.min.js', 
    'app/js/bootstrap.min.js', 
    'app/js/main.js',
    'app/js/*.js']))
      .pipe(concat('all.js'))
      .pipe(gulp.dest(jsDest));
});
